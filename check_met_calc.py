import helper_functions as hf



def check_met_calc(fpga_dat, RUN_num, source):
    a = 1003
    b = 409

    FPGA_str = fpga_dat[0].fpga_id

    l = len(fpga_dat)

    fx = open(f"Comparison_dbg/dbg_MET_Comp/Run{RUN_num}_{source}_FPGA_{FPGA_str}_METX_Calc_check.txt", "w")
    fy = open(f"Comparison_dbg/dbg_MET_Comp/Run{RUN_num}_{source}_FPGA_{FPGA_str}_METY_Calc_check.txt", "w")

    fx.write(f"*Run{RUN_num} {source} FPGA {FPGA_str} METX Check a={a} b={b}\n")
    fy.write(f"*Run{RUN_num} {source} FPGA {FPGA_str} METY Check a={a} b={b}\n")

    fx.write(f"METX TOB Value      METX Calc Value (a*MHTX + b*MSTX)       TOB - Calc\n")
    fy.write(f"METY TOB Value      METY Calc Value (a*MHTX + b*MSTX)       TOB - Calc\n")

    fx.write("-------------------------------------------------------------------------\n")
    fy.write("-------------------------------------------------------------------------\n")



    for i in range(l):
        fx.write(f"{str(fpga_dat[i].jwj_metx):<20}{str(hf.calc_met_val(fpga_dat[i].jwj_mhtx, fpga_dat[i].jwj_mstx, a, b)):<40}"
                 f"{str(fpga_dat[i].jwj_metx - hf.calc_met_val(fpga_dat[i].jwj_mhtx, fpga_dat[i].jwj_mstx, a, b)):<10}\n")
        fy.write(f"{str(fpga_dat[i].jwj_mety):<20}{str(hf.calc_met_val(fpga_dat[i].jwj_mhty, fpga_dat[i].jwj_msty, a, b)):<40}"
                 f"{str(fpga_dat[i].jwj_mety - hf.calc_met_val(fpga_dat[i].jwj_mhty, fpga_dat[i].jwj_msty, a, b)):<10}\n")


    fx.close()
    fy.close()



    return


def make_check_met_calc(ev_list, RUN_num, source):

    fpga_a_list = []
    fpga_b_list = []
    fpga_c_list = []

    l = len(ev_list)

    for i in range(0,l):
        fpga_a_list.append(ev_list[i].FPGA_A)
        fpga_b_list.append(ev_list[i].FPGA_B)
        fpga_c_list.append(ev_list[i].FPGA_C)

    check_met_calc(fpga_a_list, RUN_num, source)
    check_met_calc(fpga_b_list, RUN_num, source)
    check_met_calc(fpga_c_list, RUN_num, source)


    return