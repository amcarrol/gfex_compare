# Copy of run_compare for use in testing only
# File to run all the comparison functions in a separate file from gfex_compare for easier editing
# Please take a look at gfex_compare.py to understand all the functions being called

# import the other scripts and libraries
import gfex_compare as comp
import argparse
import ROOT as r

# set batch mode for plots to prevent all plots opening
r.gROOT.SetBatch(r.kTRUE)

ap = argparse.ArgumentParser()
ap.add_argument("-r", "--run", required=True, default=None, type=int, help="Run Number of TOB files to be compared")

args = vars(ap.parse_args())


RUN_num = args["run"]

#print statement
print("reading in files")

# files to be called
dump_ajtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_ajtobs.txt"
dump_bjtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_bjtobs.txt"
dump_cjtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_cjtobs.txt"

dump_agtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_agtobs.txt"
dump_bgtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_bgtobs.txt"
dump_cgtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_cgtobs.txt"


sim_ajtobs = f"SimTOBfiles/file{RUN_num}_atobs.txt"
sim_bjtobs = f"SimTOBfiles/file{RUN_num}_btobs.txt"
sim_cjtobs = f"SimTOBfiles/file{RUN_num}_ctobs.txt"

sim_agtobs = f"SimTOBfiles/file{RUN_num}_agtobs.txt"
sim_bgtobs = f"SimTOBfiles/file{RUN_num}_bgtobs.txt"
sim_cgtobs = f"SimTOBfiles/file{RUN_num}_cgtobs.txt"


#print statement
print("making comparisons for jet TOBs")

#compare functions
#print statement
print("FPGA A")
comp.compare_jtobs(dump_ajtobs, sim_ajtobs, RUN_num, 0)
#print statement
print("FPGA B")
comp.compare_jtobs(dump_bjtobs, sim_bjtobs, RUN_num, 1)
#print statement
print("FPGA C")
comp.compare_jtobs(dump_cjtobs, sim_cjtobs, RUN_num, 2)

#print statement
print("Making Some Comparison Plots")
comp.comp_half_fpga(dump_ajtobs, sim_ajtobs, RUN_num, 0)
comp.comp_half_fpga(dump_bjtobs, sim_bjtobs, RUN_num, 1)
comp.comp_half_fpga(dump_cjtobs, sim_cjtobs, RUN_num, 2)


#print statement
print("done!")