#Debugging functions for gFEX Comparison framework

import event_builder as eb
import plotting_functions as pf
import jet_tob_parsing as jtp
import helper_functions as hf
import argparse
import ROOT as r

#function for returning lists of only mismatching large r jet events, takes in lists of event objects and returns lists of sim/fw eta and phi as well as L1ID

def find_mismatches(sim, fw):
    
    l = len(fw)

    L1ID = []
    sim_eta = []
    sim_phi = []
    fw_eta = []
    fw_phi = []
    

    for i in range(0,l):
        if fw[i].FPGA_A.TOB1_lgLJ_et != sim[i].FPGA_A.TOB1_lgLJ_et:
            L1ID.append(fw[i].FPGA_A.j_l1id)
            sim_eta.append(sim[i].FPGA_A.TOB1_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_A.TOB1_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_A.TOB1_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_A.TOB1_lgLJ_phi)
        if fw[i].FPGA_A.TOB2_lgLJ_et != sim[i].FPGA_A.TOB2_lgLJ_et:
            L1ID.append(fw[i].FPGA_A.j_l1id)
            sim_eta.append(sim[i].FPGA_A.TOB2_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_A.TOB2_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_A.TOB2_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_A.TOB2_lgLJ_phi)
        if fw[i].FPGA_B.TOB1_lgLJ_et != sim[i].FPGA_B.TOB1_lgLJ_et:
            L1ID.append(fw[i].FPGA_B.j_l1id)
            sim_eta.append(sim[i].FPGA_B.TOB1_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_B.TOB1_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_B.TOB1_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_B.TOB1_lgLJ_phi)
        if fw[i].FPGA_B.TOB2_lgLJ_et != sim[i].FPGA_B.TOB2_lgLJ_et:
            L1ID.append(fw[i].FPGA_B.j_l1id)
            sim_eta.append(sim[i].FPGA_B.TOB2_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_B.TOB2_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_B.TOB2_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_B.TOB2_lgLJ_phi)
        if fw[i].FPGA_C.TOB1_lgLJ_et != sim[i].FPGA_C.TOB1_lgLJ_et:
            L1ID.append(fw[i].FPGA_C.j_l1id)
            sim_eta.append(sim[i].FPGA_C.TOB1_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_C.TOB1_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_C.TOB1_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_C.TOB1_lgLJ_phi)
        if fw[i].FPGA_C.TOB2_lgLJ_et != sim[i].FPGA_C.TOB2_lgLJ_et:
            L1ID.append(fw[i].FPGA_C.j_l1id)
            sim_eta.append(sim[i].FPGA_C.TOB2_lgLJ_eta)
            sim_phi.append(sim[i].FPGA_C.TOB2_lgLJ_phi)
            fw_eta.append(fw[i].FPGA_C.TOB2_lgLJ_eta)
            fw_phi.append(fw[i].FPGA_C.TOB2_lgLJ_phi)

    return(L1ID, sim_eta, sim_phi, fw_eta, fw_phi)



def mismatch_etaphi_plots(fw_event_list, sim_event_list, RUN_num):

    #find mismatches
    mis_L1ID, mis_sim_eta, mis_sim_phi, mis_fw_eta, mis_fw_phi = find_mismatches(sim_event_list, fw_event_list)



    folder = "Comparison_dbg/dbg_hitmaps/"

    pf.eta_phi_map(mis_L1ID, mis_sim_phi, mis_sim_eta, "Mismatching_Events_CSim_Values", folder, RUN_num)
    pf.eta_phi_map(mis_L1ID, mis_fw_phi, mis_fw_eta, "Mismatching_Events_FW_Values", folder, RUN_num)

    return