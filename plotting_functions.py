#module to contain all the plotting functions needed for comparisons/distributions of TOB data
import numpy as np
from array import *
from ROOT import TCanvas, TGraph, TMultiGraph, TH2F, TProfile, TH1F, gStyle, TLatex, THStack, EColor, gPad


# helper function to plot measured vs simulated values
# min,max, sets bounds of plot, integers
def scat_plot(fw_dat, sim_dat, xstr, ystr, plotstr, folder, RUN_num, FPGA_str):

    gStyle.SetImageScaling(3.)
    minv = 0
    maxv = 0

    if min(fw_dat) <= min(sim_dat):
        minv =  int(min(fw_dat)) - 1
    else:
        minv = int(min(sim_dat)) - 1

    if max(fw_dat) >= max(sim_dat):
        maxv =  int(max(fw_dat)) + 1
    else:
        maxv = int(max(sim_dat)) + 1

    # determine size and bounds of scatter plot
    size = range(minv, maxv+1, 1)
    n = len(size)

    # make a "truth" line of data points that agree
    x = array('d')
    y = array('d')

    for i in size:
        x.append(float(i))
        y.append(float(i))

    truth = TGraph(n, x, y)
    truth.SetMinimum(minv)
    truth.SetMaximum(maxv)
    truth.SetLineColor(4)
    truth.SetTitle("Agreement")

    n_dat = int(len(fw_dat))
    dat = TGraph(n_dat, fw_dat, sim_dat)
    dat.SetMinimum(minv)
    dat.SetMaximum(maxv)
    dat.SetMarkerStyle(25)
    dat.SetMarkerColor(2)
    dat.SetMarkerSize(0.25)
    dat.SetTitle("Data")

    c = TCanvas('c', f'{plotstr}', 200, 10, 700, 500)

    inc_count = 0
    for i in range(n_dat):
        if fw_dat[i] != sim_dat[i]:
            inc_count += 1

    match_frac = float(n_dat - inc_count) / float(n_dat) * 100.

    mf_str = f"Matching Percentage: {match_frac}%"
    te_str = f"Number of events: {n_dat}"
    text_str = f"{mf_str}\n{te_str}"

    mg = TMultiGraph()
    mg.Add(truth, "L")
    mg.Add(dat, "P")

    mg.SetTitle(f'Run {RUN_num} FPGA {FPGA_str} {plotstr}')
    mg.GetXaxis().SetTitle(xstr)
    mg.GetYaxis().SetTitle(ystr)

    mg.Draw("A")

    c.BuildLegend(0.1, 0.9, 0.3, 0.8)


    yrange = maxv - minv
    yplace = 0.8*maxv
    if maxv > 4500:
        yplace = 0.6*maxv
    if maxv < 20:
        yplace = 0.9*maxv


    text = TLatex()
    text.SetTextSize(0.025)
    text.DrawLatex(minv, yplace, text_str)

    c.Print(f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.png")

    return

def hist_1d(arr, xstr, plotstr, folder, RUN_num, FPGA_str):
    gStyle.SetImageScaling(3.)

    #define bounds of plot automatically
    max_val = max(arr)
    min_val = min(arr)
    b = 0.0

    #set b based on different conditions, we want it to be the largest abs() of min or max 
    if max_val >= 0.0 and min_val >= 0.0:
        b = max_val

    if max_val < 0.0 and min_val < 0.0:
        b = -1.0 * min_val

    if max_val > 0.0 and min_val < 0.0:
        if max_val >= -1.0 * min_val:
            b = max_val
        if max_val < -1.0 * min_val:
            b =  -1.0 * min_val
    
    lowb = -(b+1.0)
    if min_val >= 0.0:
        lowb = 0.0
            
    c = TCanvas('c', 'Histograms', 200, 10, 700, 500)
    c.SetRightMargin(0.18)

    #log plot
    c.SetLogy()

    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.png"
    # add 1 to the bound so the max is included
    hist = TH1F('hist', f'Run{RUN_num} FPGA {FPGA_str} {plotstr}; {xstr}', 100, lowb, b + 1.0)
    for i in range(len(arr)):
        hist.Fill(arr[i])


    hist.Draw()

    c.Print(name)

    return


def eta_phi_map(data, data_phi, data_eta, plotstr, folder, RUN_num):

    c = TCanvas('c', 'Eta Phi Space', 200, 10, 700, 500)
    c.SetRightMargin(0.18)
    
    name = f"{folder}Run{RUN_num}_{plotstr}.png"
    hist = TH2F('hist', f"Run {RUN_num} {plotstr} Eta Phi Hit Map; eta [bin number]; phi [row number]; count", 36, 2.0, 38.0, 32, 0.0, 32.0)

    for i in range(len(data)):
        for irow in range(32):
            for icolumn in range(40):
                if (data_phi[i] == irow and data_eta[i] == icolumn):
                    hist.Fill(float(icolumn)+0.5, float(irow)+0.5, 1.0)

    hist.SetStats(0)
    hist.Draw("colz")

    c.Print(name)

    return

#Overlay 1d histograms of two sets of data for one variable for better comparisons, by convention, arr1 is CSim data,
#arr2 is FW Data
def comp_hist(arr1, arr2, plabel, flabel, xlabel, folder, RUN_num, FPGA_str):
    gStyle.SetImageScaling(3.)

    # define bounds of plot automatically
    if max(arr1) >= max(arr2):
        max_val = max(arr1)
    else:
        max_val = max(arr2)

    if min(arr1) <= min(arr2):
        min_val = min(arr1)
    else:
        min_val = min(arr2)

    b = 0.0

    # set b based on different conditions, we want it to be the largest abs() of min or max
    if max_val >= 0.0 and min_val >= 0.0:
        b = max_val

    if max_val < 0.0 and min_val < 0.0:
        b = -1.0 * min_val

    if max_val > 0.0 and min_val < 0.0:
        if max_val >= -1.0 * min_val:
            b = max_val
        if max_val < -1.0 * min_val:
            b = -1.0 * min_val

    lowb = -(b + 1.0)
    if min_val >= 0.0:
        lowb = 0.0

    c = TCanvas('c', 'Histograms', 200, 10, 700, 500)
    c.SetRightMargin(0.18)
    c.SetLogy()

    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{flabel}.png"

    hist1 = TH1F('hist1', 'Bitwise C Simulation', 100, lowb, b + 1.0)
    hist1.SetLineColor(EColor.kBlue)
    hist1.SetLineWidth(3)
    hist1.SetFillColor(EColor.kCyan-7)
    hist1.SetDirectory(0)
    for i in range(len(arr1)):
        hist1.Fill(arr1[i])

    hist2 = TH1F('hist2', 'FW Readout Path', 100, lowb, b + 1.0)
    hist2.SetLineColor(EColor.kRed)
    hist2.SetLineWidth(3)
    hist2.SetLineStyle(10)
    hist2.SetFillStyle(3244)
    hist2.SetFillColor(EColor.kRed)
    hist2.SetDirectory(0)
    for i in range(len(arr2)):
        hist2.Fill(arr2[i])

    n_dat = len(arr1)

    inc_count = 0
    for i in range(n_dat):
        if arr1[i] != arr2[i]:
            inc_count += 1

    match_frac = float(n_dat - inc_count) / float(n_dat) * 100.

    #mf_str = f"Matching Percentage: {match_frac}%"
    #te_str = f"Number of events: {n_dat}"

    hstack = THStack('hstack', f'Run{RUN_num} FPGA {FPGA_str} {plabel}; {xlabel}; Counts')

    hstack.Add(hist1)
    hstack.Add(hist2)

    hstack.Draw("nostack")

    gPad.SetGrid(1,0)
    gPad.BuildLegend(0.85,0.85,0.99,0.99)

    text = TLatex()
    text.SetNDC()
    text.SetTextSize(0.019)
    text.DrawLatex(0.82, 0.70, "#splitline{Matching Percentage: %.2f%%}{Number of events: %d}" %(match_frac, n_dat))

    c.Print(name)


    return

'''
ANYTHING BELOW THIS STILL NEEDS WORK

'''


# helper function for creating eta/phi space plots of difference counts
# d is the energy array being passed in, d phi/eta are the eta and phi arrays of the energy array
# thresh is the threshold for plotting counts, map_type specifies between difference plots (0) or hit maps (1), w is weight type mode, 0 is counts, 1 is value of d[i]
'''
DEPRECATED
def etaphi_map(d, dphi, deta, thresh, map_type, w, plotstr, folder, RUN_num, FPGA_str):
    map_str = ''
    map_str_file = ''
    weight_str = ''
    if map_type == 0:
        if w == 0:
            map_str = 'E_T Difference Counts'
            map_str_file = 'DiffCounts'
            weight_str = 'Count'
        if w == 1:
            map_str = 'E_T Difference Values'
            map_str_file = 'DiffVal'
            weight_str = 'Difference'
    if map_type == 1:
        map_str = 'E_T'
        map_str_file = 'HitMap'
        weight_str = 'Count'

    if thresh < 0:
        c = TCanvas('c', 'Eta Phi Space', 200, 10, 700, 500)
        c.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_LessThan{thresh}.pdf"
        hist = TH2F('hist',
                    f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} < {thresh};eta [bin number];phi [row number]; {weight_str}',
                    36, 2.0, 38.0, 32, 0.0, 32.0)
        # loop through array of the energy values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
            # loop through columns and rows, match by measured eta/phi
            for irow in range(32):
                for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if (d[i] < thresh):
                            hist.Fill(float(icolumn) + 0.5, float(irow) + 0.5, count)

        hist.SetStats(0)
        hist.Draw("colz")

        c.Print(name)

    elif thresh > 0:
        c = TCanvas('c', 'Eta Phi Space', 200, 10, 700, 500)
        c.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_GreaterThan{thresh}.pdf"
        hist = TH2F('hist',
                    f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} > {thresh};eta [bin number];phi [row number]; {weight_str}',
                    36, 2.0, 38.0, 32, 0.0, 32.0)
        # loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
            # loop through columns and rows, match by measured eta/phi
            for irow in range(32):
                for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if (d[i] > thresh):
                            hist.Fill(float(icolumn) + 0.5, float(irow) + 0.5, count)

        hist.SetStats(0)
        hist.Draw("colz")

        c.Print(name)

        # need to do both for 0
    elif thresh == 0:
        ca = TCanvas('ca', 'Eta Phi Space', 200, 10, 700, 500)
        ca.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_LessThan{thresh}.pdf"
        hist1 = TH2F('hist1',
                     f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} < {thresh};eta [bin number];phi [row number]; {weight_str}',
                     36, 2.0, 38.0, 32, 0.0, 32.0)
        # loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
            # loop through columns and rows, match by measured eta/phi
            for irow in range(32):
                for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if (d[i] < thresh):
                            hist1.Fill(float(icolumn) + 0.5, float(irow) + 0.5, count)

        hist1.SetStats(0)
        hist1.Draw("colz")

        ca.Print(name)

        cb = TCanvas('cb', 'Eta Phi Space', 200, 10, 700, 500)
        cb.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_GreaterThan{thresh}.pdf"
        hist2 = TH2F('hist2',
                     f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} > {thresh};eta [bin number];phi [row number]; {weight_str}',
                     36, 2.0, 38.0, 32, 0.0, 32.0)
        # loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
            # loop through columns and rows, match by measured eta/phi
            for irow in range(32):
                for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if (d[i] > thresh):
                            hist2.Fill(float(icolumn) + 0.5, float(irow) + 0.5, count)

        hist2.SetStats(0)
        hist2.Draw("colz")

        cb.Print(name)

        cc = TCanvas('cc', 'Eta Phi Space', 200, 10, 700, 500)
        cc.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_Equal{thresh}.pdf"
        hist3 = TH2F('hist3',
                     f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} = {thresh};eta [bin number];phi [row number]; {weight_str}',
                     36, 2.0, 38.0, 32, 0.0, 32.0)
        # loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
            # loop through columns and rows, match by measured eta/phi
            for irow in range(32):
                for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if (d[i] == thresh):
                            hist3.Fill(float(icolumn) + 0.5, float(irow) + 0.5, count)

        hist3.SetStats(0)
        hist3.Draw("colz")

        cc.Print(name)

    return
'''

# TProfile function for plotting
def profile_plot(x, y, xstr, ystr, plotstr, folder, RUN_num, FPGA_str):
    c = TCanvas('c', 'Profile Plots', 200, 10, 700, 500)
    c.SetRightMargin(0.18)

    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.pdf"
    prof = TProfile('prof', f"Run{RUN_num} FPGA {FPGA_str} {plotstr}; {xstr}; {ystr}", 1000, 0.0, float(max(x)))

    for i in range(len(x)):
        prof.Fill(float(x[i]), float(y[i]))

    prof.Draw("AP")

    c.Print(name)

    return


