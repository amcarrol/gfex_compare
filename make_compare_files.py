#module for actually running comparisons and generating text files

import jet_tob_parsing as jtp
import global_tob_parsing as gtp
import helper_functions as hf

#fw_dat and sim_dat are lists of fpga_ev objects
#creates 4 files, a detailed comparison output, a summary file, a separate list of L1IDs that had a mismatch, and a
#Saturation Bit comparison
def jet_TOB_Compare(fw_dat, sim_dat, RUN_num):


    #DQ checks for variable assignments

    #FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    #Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    #open four files: detail, summary, l1id_list of events which have a mismatch, and a satbit check file
    f_detail = open(f"Comparison_dbg/dbg_Jet_Comp/Run{RUN_num}_FPGA_{FPGA_str}_JetTOB_Comp_Detail.txt", "w+")
    f_summ = open(f"Comparison_dbg/dbg_Jet_Comp/Run{RUN_num}_FPGA_{FPGA_str}_JetTOB_Comp_Summary.txt", "w+")
    f_l1id = open(f"Comparison_dbg/dbg_Jet_Comp/Run{RUN_num}_FPGA_{FPGA_str}_JetTOB_Mismatch_L1IDs.txt", "w+")
    f_satbit = open(f"Comparison_dbg/dbg_Jet_Comp/Run{RUN_num}_FPGA_{FPGA_str}_JetTOB_Satbit_Check.txt", "w+")


    #write preamble stuff for detail files
    f_detail.write(f'Mismatches found in Jet TOBs for Run {RUN_num} FPGA {FPGA_str} \n')
    f_detail.write('Type                             L1ID         FW Bin          FW Int  CSim Bin        CSim Int\n')
    f_detail.write('------------------------------------------------------------------------------------------------\n')
    clines = set(f_detail)

    f_l1id.write(f"List of L1IDs that correspond to a mismatch in TOB2 Large Jets`\n")

    f_satbit.write(f"List of L1IDs with a satbit turned on\n")
    f_satbit.write('Type                             L1ID             FW SatBit    CSim SatBit\n')
    f_satbit.write('------------------------------------------------------------------------------------------------\n')

    # set counters for the number of errors so its easy to keep track
    lgJ_phi_left_err = 0
    sgJ_phi_left_err = 0
    lgLJ_phi_left_err = 0
    lgJ_eta_left_err = 0
    sgJ_eta_left_err = 0
    lgLJ_eta_left_err = 0
    lgJ_en_left_err = 0
    sgJ_en_left_err = 0
    lgLJ_en_left_err = 0

    lgJ_phi_right_err = 0
    sgJ_phi_right_err = 0
    lgLJ_phi_right_err = 0
    lgJ_eta_right_err = 0
    sgJ_eta_right_err = 0
    lgLJ_eta_right_err = 0
    lgJ_en_right_err = 0
    sgJ_en_right_err = 0
    lgLJ_en_right_err = 0

    puc_err = 0

    phi_err = 0
    eta_err = 0
    nrg_err = 0

    l1id_mis = 0

    for i in range(0,l):
        if fw_dat[i].j_l1id == sim_dat[i].j_l1id:
            '''
            Left half of FPGA
            '''

            if fw_dat[i].TOB1_lgJ_phi != sim_dat[i].TOB1_lgJ_phi:
                line = f"{FPGA_str}TOB_1 Leading Small Jet Phi     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB1_lgJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB1_lgJ_phi):<6}\n"
                phi_err += 1
                lgJ_phi_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB1_lgJ_eta != sim_dat[i].TOB1_lgJ_eta:
                line = f"{FPGA_str}TOB_1 Leading Small Jet Eta     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB1_lgJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB1_lgJ_eta):<6}\n"
                eta_err += 1
                lgJ_eta_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB1_lgJ_et != sim_dat[i].TOB1_lgJ_et:
                line = f"{FPGA_str}TOB_1 Leading Small Jet ET      {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB1_lgJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB1_lgJ_et):<6}\n"
                nrg_err += 1
                lgJ_en_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)

            if fw_dat[i].TOB1_lgJ_satbit != sim_dat[i].TOB1_lgJ_satbit:
                line = f"{FPGA_str}TOB_1 Leading Small Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB1_lgJ_satbit}   {sim_dat[i].TOB1_lgJ_satbit}\n"
                f_satbit.write(line)

            if fw_dat[i].TOB1_sgJ_phi != sim_dat[i].TOB1_sgJ_phi:
                line = f"{FPGA_str}TOB_1 Subleading Small Jet Phi  {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_sgJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB1_sgJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB1_sgJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB1_sgJ_phi):<6}\n"
                phi_err += 1
                sgJ_phi_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB1_sgJ_eta != sim_dat[i].TOB1_sgJ_eta:
                line = f"{FPGA_str}TOB_1 Subleading Small Jet Eta  {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_sgJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB1_sgJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB1_sgJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB1_sgJ_eta):<6}\n"
                eta_err += 1
                sgJ_eta_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)

            if fw_dat[i].TOB1_sgJ_satbit != sim_dat[i].TOB1_sgJ_satbit:
                line = f"{FPGA_str}TOB_1 Subleading Small Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB1_sgJ_satbit}   {sim_dat[i].TOB1_sgJ_satbit}\n"
                f_satbit.write(line)

            if fw_dat[i].TOB1_sgJ_et != sim_dat[i].TOB1_sgJ_et:
                line = f"{FPGA_str}TOB_1 Subleading Small Jet ET   {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_sgJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB1_sgJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB1_sgJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB1_sgJ_et):<6}\n"
                nrg_err += 1
                sgJ_en_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)

                   # f_l1id.write(f'{fw_dat[i].j_l1id}\n')

            if fw_dat[i].TOB1_lgLJ_phi != sim_dat[i].TOB1_lgLJ_phi:
                line = f"{FPGA_str}TOB_1 Leading Large Jet Phi     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgLJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB1_lgLJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgLJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB1_lgLJ_phi):<6}\n"
                phi_err += 1
                lgLJ_phi_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB1_lgLJ_eta != sim_dat[i].TOB1_lgLJ_eta:
                line = f"{FPGA_str}TOB_1 Leading Large Jet Eta     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgLJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB1_lgLJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgLJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB1_lgLJ_eta):<6}\n"
                eta_err += 1
                lgLJ_eta_left_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB1_lgLJ_et != sim_dat[i].TOB1_lgLJ_et:
                line = f"{FPGA_str}TOB_1 Leading Large Jet ET      {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB1_lgLJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB1_lgLJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB1_lgLJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB1_lgLJ_et):<6}\n"
                nrg_err += 1
                lgLJ_en_left_err += 1
                #special check
                #if ((fw_dat[i].TOB1_lgLJ_et == 0) and (sim_dat[i].TOB1_lgLJ_et > 400)):
                #    f_l1id.write(f'{fw_dat[i].j_l1id}\n')
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)

            if fw_dat[i].TOB1_lgLJ_satbit != sim_dat[i].TOB1_lgLJ_satbit:
                line = f"{FPGA_str}TOB_1 Leading Large Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB1_lgLJ_satbit}   {sim_dat[i].TOB1_lgLJ_satbit}\n"
                f_satbit.write(line)

            '''
            Right Half of FPGA
            '''
            if fw_dat[i].TOB2_lgJ_phi != sim_dat[i].TOB2_lgJ_phi:
                line = f"{FPGA_str}TOB_2 Leading Small Jet Phi     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB2_lgJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB2_lgJ_phi):<6}\n"
                phi_err += 1
                lgJ_phi_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB2_lgJ_eta != sim_dat[i].TOB2_lgJ_eta:
                line = f"{FPGA_str}TOB_2 Leading Small Jet Eta     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB2_lgJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB2_lgJ_eta):<6}\n"
                eta_err += 1
                lgJ_eta_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB2_lgJ_et != sim_dat[i].TOB2_lgJ_et:
                line = f"{FPGA_str}TOB_2 Leading Small Jet ET      {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB2_lgJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB2_lgJ_et):<6}\n"
                nrg_err += 1
                lgJ_en_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    #f_l1id.write(f'{fw_dat[i].j_l1id}\n')

            if fw_dat[i].TOB2_lgJ_satbit != sim_dat[i].TOB2_lgJ_satbit:
                line = f"{FPGA_str}TOB_2 Leading Small Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB2_lgJ_satbit}   {sim_dat[i].TOB2_lgJ_satbit}\n"
                f_satbit.write(line)


            if fw_dat[i].TOB2_sgJ_phi != sim_dat[i].TOB2_sgJ_phi:
                line = f"{FPGA_str}TOB_2 Subleading Small Jet Phi  {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_sgJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB2_sgJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB2_sgJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB2_sgJ_phi):<6}\n"
                phi_err += 1
                sgJ_phi_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB2_sgJ_eta != sim_dat[i].TOB2_sgJ_eta:
                line = f"{FPGA_str}TOB_2 Subleading Small Jet Eta  {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_sgJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB2_sgJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB2_sgJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB2_sgJ_eta):<6}\n"
                eta_err += 1
                sgJ_eta_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB2_sgJ_et != sim_dat[i].TOB2_sgJ_et:
                line = f"{FPGA_str}TOB_2 Subleading Small Jet ET   {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_sgJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB2_sgJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB2_sgJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB2_sgJ_et):<6}\n"
                nrg_err += 1
                sgJ_en_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    #f_l1id.write(f'{fw_dat[i].j_l1id}\n')

            if fw_dat[i].TOB2_sgJ_satbit != sim_dat[i].TOB2_sgJ_satbit:
                line = f"{FPGA_str}TOB_2 Subleading Small Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB2_sgJ_satbit}    {sim_dat[i].TOB2_sgJ_satbit}\n"
                f_satbit.write(line)

            if fw_dat[i].TOB2_lgLJ_phi != sim_dat[i].TOB2_lgLJ_phi:
                line = f"{FPGA_str}TOB_2 Leading Large Jet Phi     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgLJ_phi)[2:].zfill(5):<14}  {str(fw_dat[i].TOB2_lgLJ_phi):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgLJ_phi)[2:].zfill(5):<14}  {str(sim_dat[i].TOB2_lgLJ_phi):<6}\n"
                phi_err += 1
                lgLJ_phi_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)


            if fw_dat[i].TOB2_lgLJ_eta != sim_dat[i].TOB2_lgLJ_eta:
                line = f"{FPGA_str}TOB_2 Leading Large Jet Eta     {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgLJ_eta)[2:].zfill(6):<14}  {str(fw_dat[i].TOB2_lgLJ_eta):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgLJ_eta)[2:].zfill(6):<14}  {str(sim_dat[i].TOB2_lgLJ_eta):<6}\n"
                lgLJ_eta_right_err += 1
                eta_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)



            if fw_dat[i].TOB2_lgLJ_et != sim_dat[i].TOB2_lgLJ_et:
                line = f"{FPGA_str}TOB_2 Leading Large Jet ET      {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].TOB2_lgLJ_et)[2:].zfill(12):<14}  {str(fw_dat[i].TOB2_lgLJ_et):<6}  " \
                       f"{bin(sim_dat[i].TOB2_lgLJ_et)[2:].zfill(12):<14}  {str(sim_dat[i].TOB2_lgLJ_et):<6}\n"
                nrg_err += 1
                lgLJ_en_right_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    #for run 448816, checking vertical line at 504
                    #if fw_dat[i].TOB2_lgLJ_et == 504 and sim_dat[i].TOB2_lgLJ_et != 0:
                    f_l1id.write(f'{fw_dat[i].j_l1id}\n')

            if fw_dat[i].TOB2_lgLJ_satbit != sim_dat[i].TOB2_lgLJ_satbit:
                line = f"{FPGA_str}TOB_2 Leading Large Jet      {fw_dat[i].j_l1id:<11}  "\
                       f"{fw_dat[i].TOB2_lgLJ_satbit}    {sim_dat[i].TOB2_lgLJ_satbit}\n"
                f_satbit.write(line)

            #PUC
            if fw_dat[i].puc != sim_dat[i].puc:
                line = f"{FPGA_str} PUC      {fw_dat[i].j_l1id:<11}  " \
                       f"{bin(fw_dat[i].puc)[2:].zfill(12):<14}  {str(fw_dat[i].puc):<6}  " \
                       f"{bin(sim_dat[i].puc)[2:].zfill(12):<14}  {str(sim_dat[i].puc):<6}\n"
                puc_err += 1
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    f_l1id.write(f'{fw_dat[i].j_l1id}\n')
        else:
            f_detail.write(f"Error: L1ID mismatch: {fw_dat[i].j_l1id}    {sim_dat[i].j_l1id}\n")
            l1id_mis += 1


    f_summ.write(f"Total Number of Events: {l}\n")
    f_summ.write(f"Events with mismatched L1IDs: {l1id_mis}\n")
    f_summ.write(f"Errors in phi: {phi_err}\n")
    f_summ.write(f"Errors in eta: {eta_err}\n")
    f_summ.write(f"Errors in energy: {nrg_err}\n")
    f_summ.write(f"Errors in LG1 phi: {lgJ_phi_left_err}\n")
    f_summ.write(f"Errors in LG1 eta: {lgJ_eta_left_err}\n")
    f_summ.write(f"Errors in LG1 energy: {lgJ_en_left_err}\n")
    f_summ.write(f"Errors in SG1 phi: {sgJ_phi_left_err}\n")
    f_summ.write(f"Errors in SG1 eta: {sgJ_eta_left_err}\n")
    f_summ.write(f"Errors in SG1 energy: {sgJ_en_left_err}\n")
    f_summ.write(f"Errors in LJ1 phi: {lgLJ_phi_left_err}\n")
    f_summ.write(f"Errors in LJ1 eta: {lgLJ_eta_left_err}\n")
    f_summ.write(f"Errors in LJ1 energy: {lgLJ_en_left_err}\n")
    f_summ.write(f"Errors in LG2 phi: {lgJ_phi_right_err}\n")
    f_summ.write(f"Errors in LG2 eta: {lgJ_eta_right_err}\n")
    f_summ.write(f"Errors in LG2 energy: {lgJ_en_right_err}\n")
    f_summ.write(f"Errors in SG2 phi: {sgJ_phi_right_err}\n")
    f_summ.write(f"Errors in SG2 eta: {sgJ_eta_right_err}\n")
    f_summ.write(f"Errors in SG2 energy: {sgJ_en_right_err}\n")
    f_summ.write(f"Errors in LJ2 phi: {lgLJ_phi_right_err}\n")
    f_summ.write(f"Errors in LJ2 eta: {lgLJ_eta_right_err}\n")
    f_summ.write(f"Errors in LJ2 energy: {lgLJ_en_right_err}\n")
    f_summ.write(f"Errors in PUC value: {puc_err}\n")

    #close files
    f_detail.close()
    f_summ.close()
    f_l1id.close()
    f_satbit.close()

    return

#fw_dat and sim_dat are lists of fpga_ev objects
def global_TOB_compare(fw_dat, sim_dat, RUN_num):

    #DQ checks for variable assignments

    #FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    #Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    # set error counters
    l1id_mis = 0
    jwj_mhtx_err = 0
    jwj_mhty_err = 0
    jwj_mstx_err = 0
    jwj_msty_err = 0
    jwj_metx_err = 0
    jwj_mety_err = 0
    jwj_tot_sumEt_err = 0
    jwj_hard_sumEt_err = 0
    jwj_soft_sumEt_err = 0
    nc_metx_err = 0
    nc_mety_err = 0
    rms_metx_err = 0
    rms_mety_err = 0
    nc_sumEt_err = 0
    rms_sumEt_err = 0


    # open output file
    f_detail = open(f"Comparison_dbg/dbg_MET_Comp/Run{RUN_num}_FPGA{FPGA_str}_compare_METTOBs.txt", 'w+')
    f_detail.write('Mismatches found in Global(MET) TOBs for FPGA-%s run %d \n' % (FPGA_str, RUN_num))
    f_detail.write('Type       L1ID         FW Bin             FW Int    CSim Bin             CSim Int   \n')
    f_detail.write('-------------------------------------------------------------------------------------------\n')
    clines = set(f_detail)

    for i in range(0,l):
        if fw_dat[i].g_l1id == sim_dat[i].g_l1id:

            if fw_dat[i].jwj_mhtx != sim_dat[i].jwj_mhtx:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_mhtx,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_mhtx,16)
                line = f"JWJ_MHTX   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_mhtx):<7}    " \
                       f"{sim_bin_str:<17}   {str(sim_dat[i].jwj_mhtx):<7}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_mhtx_err += 1

            if fw_dat[i].jwj_mhty != sim_dat[i].jwj_mhty:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_mhty,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_mhty,16)
                line = f"JWJ_MHTY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_mhty):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_mhty):<7}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_mhty_err += 1

            if fw_dat[i].jwj_metx != sim_dat[i].jwj_metx:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_metx,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_metx,16)
                line = f"JWJ_METX   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_metx):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_metx):<7}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_metx_err += 1

            if fw_dat[i].jwj_mety != sim_dat[i].jwj_mety:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_mety,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_mety,16)
                line = f"JWJ_METY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_mety):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_mety):<7}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_mety_err += 1

            if fw_dat[i].jwj_mstx != sim_dat[i].jwj_mstx:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_mstx,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_mstx,16)
                line = f"JWJ_MSTX   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_mstx):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_mstx):<7}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_mstx_err += 1

            if fw_dat[i].jwj_msty != sim_dat[i].jwj_msty:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_msty,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_msty,16)
                line = f"JWJ_MSTY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_msty):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_msty):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_msty_err += 1


            if fw_dat[i].nc_metx != sim_dat[i].nc_metx:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].nc_metx,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].nc_metx,16)
                line = f"NC_METX   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].nc_metx):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].nc_metx):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    nc_metx_err += 1

            if fw_dat[i].nc_mety != sim_dat[i].nc_mety:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].nc_mety,16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].nc_mety,16)
                line = f"NC_METY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].nc_mety):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].nc_mety):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    nc_mety_err += 1

            if fw_dat[i].rms_metx != sim_dat[i].rms_metx:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].rms_metx, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].rms_metx, 16)
                line = f"RMS_METX   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].rms_metx):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].rms_metx):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    rms_metx_err += 1

            if fw_dat[i].rms_mety != sim_dat[i].rms_mety:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].rms_mety, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].rms_mety, 16)
                line = f"RMS_METY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].rms_mety):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].rms_mety):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    rms_mety_err += 1

            if fw_dat[i].nc_sumEt != sim_dat[i].nc_sumEt:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].nc_sumEt, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].nc_sumEt, 16)
                line = f"NC_SumEt   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].nc_sumEt):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].nc_sumEt):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    nc_sumEt_err += 1

            if fw_dat[i].rms_sumEt != sim_dat[i].rms_sumEt:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].rms_sumEt, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].rms_sumEt, 16)
                line = f"NC_METY   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].rms_sumEt):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].rms_sumEt):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    rms_sumEt_err += 1

            if fw_dat[i].jwj_tot_sumEt != sim_dat[i].jwj_tot_sumEt:
                fw_bin_str = bin(fw_dat[i].jwj_tot_sumEt)[2:].zfill(16)
                sim_bin_str = bin(sim_dat[i].jwj_tot_sumEt)[2:].zfill(16)
                line = f"JWJ sumET Tot   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_tot_sumEt):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_tot_sumEt):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_tot_sumEt_err += 1

            if fw_dat[i].jwj_hard_sumEt != sim_dat[i].jwj_hard_sumEt:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_hard_sumEt, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_hard_sumEt, 16)
                line = f"JWJ sumET Hard   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_hard_sumEt):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_hard_sumEt):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_hard_sumEt_err += 1

            if fw_dat[i].jwj_soft_sumEt != sim_dat[i].jwj_soft_sumEt:
                fw_bin_str = hf.signed_int_to_bin(fw_dat[i].jwj_soft_sumEt, 16)
                sim_bin_str = hf.signed_int_to_bin(sim_dat[i].jwj_soft_sumEt, 16)
                line = f"sumET Soft   {fw_dat[i].g_l1id:<11}  {fw_bin_str:<17}  " \
                       f"{str(fw_dat[i].jwj_soft_sumEt):<7}   " \
                       f"{sim_bin_str:<17}    {str(sim_dat[i].jwj_soft_sumEt):<7s}\n"
                if not line in clines:
                    f_detail.write(line)
                    clines.add(line)
                    jwj_soft_sumEt_err += 1

        else:
            f_detail.write(f"Error: L1ID mismatch: {fw_dat[i].j_l1id}    {sim_dat[i].j_l1id}\n")
            l1id_mis += 1


    f_detail.close()


    f_summ = open(f"Comparison_dbg/dbg_MET_Comp/Run{RUN_num}_FPGA{FPGA_str}_METTOB_Error_Summary.txt", "w+")

    f_summ.write(f"Total Number of Events: {l}\n")
    f_summ.write(f"Events with mismatched L1IDs: {l1id_mis}\n")
    f_summ.write(f"errors in JWJ hard term x: {jwj_mhtx_err}\n")
    f_summ.write(f"errors in JWJ hard term y: {jwj_mhty_err}\n")
    f_summ.write(f"errors in JWJ soft term x: {jwj_mstx_err}\n")
    f_summ.write(f"errors in JWJ soft term y: {jwj_msty_err}\n")
    f_summ.write(f"errors in JWJ combined term x: {jwj_metx_err}\n")
    f_summ.write(f"errors in JWJ combined term y: {jwj_mety_err}\n")
    f_summ.write(f"errors in NC x term: {nc_metx_err}\n")
    f_summ.write(f"errors in NC y term: {nc_mety_err}\n")
    f_summ.write(f"errors in Rho+RMS x term: {rms_metx_err}\n")
    f_summ.write(f"errors in Rho+RMS y term: {rms_mety_err}\n")
    f_summ.write(f"errors in NC Sum Et term: {nc_sumEt_err}\n")
    f_summ.write(f"errors in Rho+RMS Sum Et term: {rms_sumEt_err}\n")
    f_summ.write(f"errors in JWJ Total Sum Et term: {jwj_tot_sumEt_err}\n")
    f_summ.write(f"errors in JWJ Hard Sum Et term: {jwj_hard_sumEt_err}\n")
    f_summ.write(f"errors in JWJ Soft Sum Et term: {jwj_soft_sumEt_err}\n")
    f_summ.close()

    return


# inputs are the list of events from event builder
def make_compare_files(fw_ev_list, sim_ev_list, RUN_num):

    #define flags, these can be turned on or off 
    aj_flag = 1
    bj_flag = 1
    cj_flag = 1
    ag_flag = 1
    bg_flag = 1
    cg_flag = 1

    #organize events into FPGAs
    fw_fpga_a_list = []
    fw_fpga_b_list = []
    fw_fpga_c_list = []

    sim_fpga_a_list = []
    sim_fpga_b_list = []
    sim_fpga_c_list = []

    l = len(fw_ev_list)

    for i in range(0,l):
        fw_fpga_a_list.append(fw_ev_list[i].FPGA_A)
        fw_fpga_b_list.append(fw_ev_list[i].FPGA_B)
        fw_fpga_c_list.append(fw_ev_list[i].FPGA_C)
        sim_fpga_a_list.append(sim_ev_list[i].FPGA_A)
        sim_fpga_b_list.append(sim_ev_list[i].FPGA_B)
        sim_fpga_c_list.append(sim_ev_list[i].FPGA_C)


    #if any flag is set to 0, we omit the comparisons for that object
    if aj_flag == 1:
        jet_TOB_Compare(fw_fpga_a_list, sim_fpga_a_list, RUN_num)

    if bj_flag == 1:
        jet_TOB_Compare(fw_fpga_b_list, sim_fpga_b_list, RUN_num)

    if cj_flag == 1:
        jet_TOB_Compare(fw_fpga_c_list, sim_fpga_c_list, RUN_num)

    if ag_flag == 1:
        global_TOB_compare(fw_fpga_a_list, sim_fpga_a_list, RUN_num)

    if bg_flag == 1:
        global_TOB_compare(fw_fpga_b_list, sim_fpga_b_list, RUN_num)

    if cg_flag == 1:
        global_TOB_compare(fw_fpga_c_list, sim_fpga_c_list, RUN_num)
        
    return


