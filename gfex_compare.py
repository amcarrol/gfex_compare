"""
Created on Thu Jun 24 12:35:54 2021

author: Anthony Carroll (acarrol4@uoregon.edu)
"""

'''
THIS CODE IS DEPRECATED AND KEPT ONLY FOR REFERENCE
'''


#Robust Code package for comparing different simulated and real outputs of various gFEX simulations and real data

#useful libraries
import numpy as np 
from array import *
from ROOT import TCanvas, TGraph, TMultiGraph, TH2F, TProfile, TH1F

#global strings
#define strings for plot names/axes
larger_str = "Leading_LargeR_Jet"
lsmallr_str = "Leading_SmallR_Jet"
ssmallr_str = "Subleading_SmallR_Jet"

et_str = "E_T"
eta_str = "Eta"
phi_str = "Phi"

#global variables
lsb = 0.2  # GeV

#debug flag
dbg_mode = 0
temp_check = 0

'''
Base Changing Helper Functions
------------------------------------------------------------------------------------------------------------------------------------
'''

#function that will take in the 8 digit hexadecimal string and return the 32 digit binary number for parsing
#hstr is the string of 8 characters in hexadecimal
#n is the integer length of the binary string
#returns a string of the binary characters
def hex_to_binary(hstr, n):
    
    return(str(bin(int(hstr, 16))))[2:].zfill(n)
    
#returns hexadecimal given binary string
def binary_to_hex(bstr, n):
    
    return(str(hex(int(bstr, 2))))[2:].zfill(n)

def bin_to_signed_int(bstr):
    num = 0
    if bstr[0] == '1':
        #to calculate signed integer from binary, subtract 1, [2:] cuts off the '0b' from bin()
        calc = bin(int(bstr,2) - 1)[2:]
        #flip bits so that 0->1 and 1->0
        calc_list = list(calc)
        for i in range(len(calc_list)):
            if calc_list[i] == '0':
                calc_list[i] = '1'
            else:
                calc_list[i] = '0'
        calc = ''.join(calc_list)
        num = -1* int(calc,2)
    if bstr[0] == '0':
        num = int(bstr,2)

    return(num)

'''
Parsing Helper Functions
-----------------------------------------------------------------------------------------------------------------------------------
'''

#given file, returns nicely parsed lines
def get_lines(f):
    dat = open(f, 'r+')
    lines = dat.readlines()
    lines = [s.strip() for s in lines]
    dat.close()
    return lines

#assigns FPGA_string for file writing based on FPGA_num
def get_fpgastr(fpga_num):
    fpga_str = ''
    if fpga_num == 0:
        fpga_str = 'A'
    if fpga_num == 1:
        fpga_str = 'B'
    if fpga_num== 2:
        fpga_str = 'C'
    return fpga_str


#helper function to parse data for jet tobs
#takes an array of lines from a jet tob data file and parses it into leading/subleading gblock, gJet, and respective eta, phi, energy
def parse_jtobs(lines):

    #get the header for each event
    l1id = lines[1::17]
    #reverse list to make it easier to index  
    l1id = [l[::-1] for l in l1id]
    #take the first element of the split list (separates at space)
    l1id = [l.split()[0] for l in l1id]
    #flip back
    l1id = [l[::-1] for l in l1id]


    #l is TOB_1, r is TOB_2
    lead_g_l = lines[4::17]
    sub_g_l = lines[5::17]
    lead_j_l = lines[6::17]
    trlr_l = lines[9::17]
    lead_g_r = lines[12::17]
    sub_g_r = lines[13::17]
    lead_j_r = lines[14::17]
    trlr_r = lines[17::17]

    #get rid of the first two characters in each row (just word numbers and a space)
    lead_g_l = [l[2:] for l in lead_g_l]
    sub_g_l = [l[2:] for l in sub_g_l]
    lead_j_l = [l[2:] for l in lead_j_l] 
    trlr_l = [l[2:] for l in trlr_l]
    lead_g_r = [l[2:] for l in lead_g_r]
    sub_g_r = [l[2:] for l in sub_g_r]
    lead_j_r = [l[2:] for l in lead_j_r]
    trlr_r = [l[2:] for l in trlr_r] 

    return(l1id, lead_g_l,sub_g_l,lead_j_l,trlr_l,lead_g_r,sub_g_r,lead_j_r,trlr_r)

#parses array of words from a jet TOB into phi, eta, and energy
#assumes 8 bit hexadecimal words have been converted into full 32 bit binary word 
def parse_jwords(words):

    phi = [word[1:6] for word in words]
    eta = [word[6:12] for word in words]
    energy = [word[12:24] for word in words]
    sat_bit = [word[0] for word in words]

    return(phi, eta, energy, sat_bit)
'''
TOB Trailer format
0-8   : CRC
9-17  : gFEX Trailer
18-19 : FEX ID
20-23 : Bunch Crossing Number (BCN)
23-31 : K28.5 -- don't know what this is
'''

#right now just takes the 4 bcid bits, can add other fields of the trailer later for testing if needed
def parse_jtrlr(trlr):
    bcn = [word[20:24] for word in trlr]
    return bcn

# helper function to parse global tob words from tob output file -- right now only use on dump outputs, sim outputs have different format
def parse_gtobs(lines):
    #get the header for each event
    l1id = lines[1::17]
    #reverse list to make it easier to index  
    l1id = [l[::-1] for l in l1id]
    #take the first element of the split list (separates at space)
    l1id = [l.split()[0] for l in l1id]
    #flip back
    l1id = [l[::-1] for l in l1id]

    jwj_mht_comp = lines[3::17]
    jwj_mst_comp = lines[4::17]
    jwj_met_comp = lines[5::17]
    jwj_trlr = lines[9::17]

    #these are currently wrong and ignored but may be used eventually when NC and RMS are fully implemented?
    nc_met_comp = lines[11::17]
    rms_met_comp = lines[12::17]
    alt_met_sums = lines[13::17]
    altmet_trlr = lines[17::17]

    #strip off first two characters (word number and space)
    jwj_mht_comp = [l[2:] for l in jwj_mht_comp]
    jwj_met_comp = [l[2:] for l in jwj_met_comp]
    jwj_mst_comp = [l[2:] for l in jwj_mst_comp]
    jwj_trlr = [l[2:] for l in jwj_trlr]

    nc_met_comp = [l[2:] for l in nc_met_comp]
    rms_met_comp = [l[2:] for l in rms_met_comp]
    alt_met_sums = [l[2:] for l in alt_met_sums]
    altmet_trlr = [l[2:] for l in altmet_trlr]

    return(l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr, nc_met_comp, rms_met_comp, alt_met_sums, altmet_trlr)

#helper function to parse global tobs for Sim JWOJ only (no second link for alt met algorithms included yet)
def parse_sim_gtobs(lines):
    #get the header for each event
    l1id = lines[1::9]
    #reverse list to make it easier to index  
    l1id = [l[::-1] for l in l1id]
    #take the first element of the split list (separates at space)
    l1id = [l.split()[0] for l in l1id]
    #flip back
    l1id = [l[::-1] for l in l1id]

    #for simulated file, when both links are used, need to change the 9 to 17 like above
    jwj_mht_comp = lines[3::9]
    jwj_mst_comp = lines[4::9]
    jwj_met_comp = lines[5::9]
    jwj_trlr = lines[9::9]



    jwj_mht_comp = [l[2:] for l in jwj_mht_comp]
    jwj_met_comp = [l[2:] for l in jwj_met_comp]
    jwj_mst_comp = [l[2:] for l in jwj_mst_comp]
    jwj_trlr = [l[2:] for l in jwj_trlr]

    return(l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr)

#helper function to parse relevant TOB items from list of parsed TOB words
def parse_gwords(words):

    words = [hex_to_binary(i,32) for i in words]

    #check length -keep or comment out eventually
    for i in range(len(words)):
        if len(words[i]) != 32:
            print(f"Too long! {len(words[i])} bits at index {i}")

    #parse out the x,y, and bcid terms 
    word1 = [l[0:16] for l in words]
    word2 = [l[16:] for l in words]
    return(word1, word2)

#runs the above helper functions to prep the data file
#base is what base you want the eta,phi, and energy information returned in, only binary (2) or decimal (10) allowed right now
def jTOB_full_parse(lines, base):

    l1id,lead_g_l,sub_g_l,lead_j_l,trlr_l,lead_g_r,sub_g_r,lead_j_r,trlr_r = parse_jtobs(lines)

    lead_g_l = [hex_to_binary(l, 32) for l in lead_g_l]
    sub_g_l = [hex_to_binary(l, 32) for l in sub_g_l]
    lead_j_l = [hex_to_binary(l, 32) for l in lead_j_l]
    trlr_l = [hex_to_binary(l, 32) for l in trlr_l]
    lead_g_r = [hex_to_binary(l, 32) for l in lead_g_r]
    sub_g_r = [hex_to_binary(l, 32) for l in sub_g_r]
    lead_j_r = [hex_to_binary(l, 32) for l in lead_j_r]
    trlr_r = [hex_to_binary(l, 32) for l in trlr_r]

    lead_g_l_phi, lead_g_l_eta, lead_g_l_en, lead_g_l_satbit = parse_jwords(lead_g_l)
    sub_g_l_phi, sub_g_l_eta, sub_g_l_en, sub_g_l_satbit = parse_jwords(sub_g_l)
    lead_j_l_phi, lead_j_l_eta, lead_j_l_en, lead_j_l_satbit = parse_jwords(lead_j_l)
    lead_g_r_phi, lead_g_r_eta, lead_g_r_en, lead_g_r_satbit = parse_jwords(lead_g_r)
    sub_g_r_phi, sub_g_r_eta, sub_g_r_en, sub_g_r_satbit = parse_jwords(sub_g_r)
    lead_j_r_phi, lead_j_r_eta, lead_j_r_en, lead_j_r_satbit = parse_jwords(lead_j_r)

    bcid_l = parse_jtrlr(trlr_l)
    bcid_r = parse_jtrlr(trlr_r)
    
    if base == 10:
        lead_g_l_phi = [int(l,2) for l in lead_g_l_phi]
        lead_g_l_eta = [int(l,2) for l in lead_g_l_eta]
        lead_g_l_en = [int(l,2) for l in lead_g_l_en]
        sub_g_l_phi = [int(l,2) for l in sub_g_l_phi]
        sub_g_l_eta = [int(l,2) for l in sub_g_l_eta]
        sub_g_l_en = [int(l,2) for l in sub_g_l_en]
        lead_j_l_phi = [int(l,2) for l in lead_j_l_phi]
        lead_j_l_eta = [int(l,2) for l in lead_j_l_eta]
        lead_j_l_en = [int(l,2) for l in lead_j_l_en]
        bcid_l = [int(l,2) for l in bcid_l]

        lead_g_r_phi = [int(l,2) for l in lead_g_r_phi]
        lead_g_r_eta = [int(l,2) for l in lead_g_r_eta]
        lead_g_r_en = [int(l,2) for l in lead_g_r_en]
        sub_g_r_phi = [int(l,2) for l in sub_g_r_phi]
        sub_g_r_eta = [int(l,2) for l in sub_g_r_eta]
        sub_g_r_en = [int(l,2) for l in sub_g_r_en]
        lead_j_r_phi = [int(l,2) for l in lead_j_r_phi]
        lead_j_r_eta = [int(l,2) for l in lead_j_r_eta]
        lead_j_r_en = [int(l,2) for l in lead_j_r_en]
        bcid_r = [int(l,2) for l in bcid_r]

        return (lead_g_l_phi, lead_g_l_eta, lead_g_l_en, lead_g_l_satbit, sub_g_l_phi, sub_g_l_eta, sub_g_l_en, sub_g_l_satbit, lead_j_l_phi, lead_j_l_eta, lead_j_l_en, lead_j_l_satbit, bcid_l, lead_g_r_phi, lead_g_r_eta, lead_g_r_en, lead_g_r_satbit, sub_g_r_phi, sub_g_r_eta, sub_g_r_en, sub_g_r_satbit, lead_j_r_phi, lead_j_r_eta, lead_j_r_en, lead_j_r_satbit, bcid_r)
    
    #default is binary
    else:
        return (lead_g_l_phi, lead_g_l_eta, lead_g_l_en, lead_g_l_satbit, sub_g_l_phi, sub_g_l_eta, sub_g_l_en, sub_g_l_satbit, lead_j_l_phi, lead_j_l_eta, lead_j_l_en, lead_j_l_satbit, bcid_l, lead_g_r_phi, lead_g_r_eta, lead_g_r_en, lead_g_r_satbit, sub_g_r_phi, sub_g_r_eta, sub_g_r_en, sub_g_r_satbit, lead_j_r_phi, lead_j_r_eta, lead_j_r_en, lead_j_r_satbit, bcid_r)

#helper function to get difference of values between two arrays
def get_diff(arr1,arr2):
    diff = array('d')

    for i in range(len(arr1)):
        diff.append(arr1[i] - arr2[i])
    
    return diff

#helper function to append left and right arrays of FPGAs together, if arrays are not same size, returns an empty array
def append_arr(arr1, arr2):
    combined = array('d')

    if len(arr1) == len(arr2):
        for i in range(len(arr1)):
            combined.append(arr1[i])
            combined.append(arr2[i])
    
    return combined

'''
Comparisons Functions to generate text files
---------------------------------------------------------------------------------------------------------------------------------------------
'''

#f1, f2 are strings corresponding to names of files to be compared
#RUN_num is run number, FPGA_num is 0 for A, 1 for B, 2 for C
def compare_jtobs(f1, f2, RUN_num, FPGA_num):
    #TODO: add comparison for BCID, since we now index through L1ID

    FPGA_str = get_fpgastr(FPGA_num)

    '''
    for jet TOBs, format is
    Link 0: Left half of pFPGA
    Word 0: unused
    Word 1: Leading gBlock (small R jet)
    Word 2: Subleading gBlock
    Word 3: Leading gJet
    Word 4-5: unused
    Word 6: Trailer

    Link 0: Right half of pFPGA
    Word 0: Leading gBlock (small R jet)
    Word 1: Subleading gBlock
    Word 2: Leading gJet
    Word 3-5: unused
    Word 6: Trailer

    Word Structure (32 bits):
    31: Energy Saturation
    30-26: Phi
    25-20: Eta
    19-8: energy
    7: Status
    6-5: Reserved
    4-0: TOB ID
    '''
    lines1 = get_lines(f1)
    lines2 = get_lines(f2)


    #parsing out the different rows
    l1id_1,lead_g_l1,sub_g_l1,lead_j_l1,trlr_l1,lead_g_r1,sub_g_r1,lead_j_r1,trlr_r1 = parse_jtobs(lines1)

    l1id_2,lead_g_l2,sub_g_l2,lead_j_l2,trlr_l2,lead_g_r2,sub_g_r2,lead_j_r2,trlr_r2 = parse_jtobs(lines2)
  
    #get phi,eta, energy
    lead_g_l1_phi, lead_g_l1_eta, lead_g_l1_en, lead_g_l1_satbit, sub_g_l1_phi, sub_g_l1_eta, sub_g_l1_en, sub_g_l1_satbit, lead_j_l1_phi, lead_j_l1_eta, lead_j_l1_en, lead_j_l1_satbit, bcid_l1, lead_g_r1_phi, lead_g_r1_eta, lead_g_r1_en, lead_g_r1_satbit, sub_g_r1_phi, sub_g_r1_eta, sub_g_r1_en, sub_g_r1_satbit, lead_j_r1_phi, lead_j_r1_eta, lead_j_r1_en, lead_j_r1_satbit, bcid_r1 = jTOB_full_parse(lines1, 2)

    lead_g_l2_phi, lead_g_l2_eta, lead_g_l2_en, lead_g_l2_satbit, sub_g_l2_phi, sub_g_l2_eta, sub_g_l2_en, sub_g_l2_satbit, lead_j_l2_phi, lead_j_l2_eta, lead_j_l2_en, lead_j_l2_satbit, bcid_l2, lead_g_r2_phi, lead_g_r2_eta, lead_g_r2_en, lead_g_r2_satbit, sub_g_r2_phi, sub_g_r2_eta, sub_g_r2_en, sub_g_r2_satbit, lead_j_r2_phi, lead_j_r2_eta, lead_j_r2_en, lead_j_r2_satbit, bcid_r2 = jTOB_full_parse(lines2, 2)    

    
    #output file
    f = open("ComparisonOutputs/ComparisonTextFiles/compare_JetTOBs_FPGA%s_Run%d.txt" %(FPGA_str,RUN_num),'w+')
        
    f.write('Mismatches found in Jet TOBs for FPGA-%s run %d \n'%(FPGA_str, RUN_num))
    f.write('Type                                             L1ID                FW (Binary)        FW (Integer)    CSim (Binary)      CSim (Integer)\n')
    clines = set(f)

    if dbg_mode == 1:
        l1id_list = open("ComparisonOutputs/ComparisonTextFiles/Run%d_FPGA%s_mismatchingL1IDs.txt" %(RUN_num,FPGA_str),'w+')
        l1id_list.write(f"List of L1IDs that contain mismatching events\n")

    #set counters for the number of errors so its easy to keep track
    lg_phi_l_err = 0
    sg_phi_l_err = 0
    lj_phi_l_err = 0
    lg_eta_l_err = 0
    sg_eta_l_err = 0
    lj_eta_l_err = 0
    lg_en_l_err = 0
    sg_en_l_err = 0
    lj_en_l_err = 0

    lg_phi_r_err = 0
    sg_phi_r_err = 0
    lj_phi_r_err = 0
    lg_eta_r_err = 0
    sg_eta_r_err = 0
    lj_eta_r_err = 0
    lg_en_r_err = 0
    sg_en_r_err = 0
    lj_en_r_err = 0    

    phi_err = 0
    eta_err = 0
    nrg_err = 0

    l1id_mis = 0

    for i in range(int(len(l1id_1))):

        if l1id_1[i] == l1id_2[i]:
            #left leading gblock

            if lead_g_l1_phi[i] != lead_g_l2_phi[i]:
                    line = f"{FPGA_str}TOB_1 Leading gblock phi        {l1id_1[i]}     {lead_g_l1_phi[i]}  {int(lead_g_l1_phi[i],2)}      {lead_g_l2_phi[i]} {int(lead_g_l2_phi[i],2)}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err += 1
                        lg_phi_l_err += 1


            if lead_g_l1_eta[i] != lead_g_l2_eta[i]:
                    line = f"{FPGA_str}TOB_1 Leading gblock eta        {l1id_1[i]}     {lead_g_l1_eta[i]}       {lead_g_l2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        lg_eta_l_err += 1

            if lead_g_l1_en[i] != lead_g_l2_en[i]:
                    line = f"{FPGA_str}TOB_1 Leading gblock energy     {l1id_1[i]}     {lead_g_l1_en[i]} {lead_g_l2_en[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        lg_en_l_err += 1
                        #if dbg_mode == 1:
                        #    l1id_list.write(f"{l1id_1[i]}\n")

            #left subleading gblock
            
            if sub_g_l1_phi[i] != sub_g_l2_phi[i]:
                    line = f"{FPGA_str}TOB_1 Subleading gblock phi     {l1id_1[i]}     {sub_g_l1_phi[i]}        {sub_g_l2_phi[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err += 1
                        sg_phi_l_err += 1


            if sub_g_l1_eta[i] != sub_g_l2_eta[i]:
                    line = f"{FPGA_str}TOB_1 Subleading gblock eta     {l1id_1[i]}     {sub_g_l1_eta[i]}       {sub_g_l2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        sg_eta_l_err += 1

            if sub_g_l1_en[i] != sub_g_l2_en[i]:
                    line = f"{FPGA_str}TOB_1 Subleading gblock energy  {l1id_1[i]}     {sub_g_l1_en[i]} {sub_g_l2_en[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        sg_en_l_err += 1
                        #if dbg_mode == 1:
                            #l1id_list.write(f"{l1id_1[i]}\n")

            #left leading gjet
            
            if lead_j_l1_phi[i] != lead_j_l2_phi[i]:
                    line = f"{FPGA_str}TOB_1 Leading gjet phi          {l1id_1[i]}     {lead_j_l1_phi[i]}        {lead_j_l2_phi[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err += 1
                        lj_phi_l_err += 1

            if lead_j_l1_eta[i] != lead_j_l2_eta[i]:
                    line = f"{FPGA_str}TOB_1 Leading gjet eta          {l1id_1[i]}     {lead_j_l1_eta[i]}       {lead_j_l2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        lj_eta_l_err += 1

            if lead_j_l1_en[i] != lead_j_l2_en[i]:
                    line = f"{FPGA_str}TOB_1 Leading gjet energy      {l1id_1[i]}     {lead_j_l1_en[i]}  {int(lead_j_l1_en[i],2)}                   {lead_j_l2_en[i]}   {int(lead_j_l2_en[i],2)}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        lj_en_l_err += 1
                        if (dbg_mode == 1):
                            l1id_list.write(f"{l1id_1[i]}\n")

            #right leading gblock

            if lead_g_r1_phi[i] != lead_g_r2_phi[i]:
                    line = f"{FPGA_str}TOB_2 Leading gblock phi        {l1id_1[i]}     {lead_g_r1_phi[i]}        {lead_g_r2_phi[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err += 1
                        lg_phi_r_err += 1

            if lead_g_r1_eta[i] != lead_g_r2_eta[i]:
                    line = f"{FPGA_str}TOB_2 Leading gblock eta        {l1id_1[i]}     {lead_g_r1_eta[i]}       {lead_g_r2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        lg_eta_r_err += 1

            if lead_g_r1_en[i] != lead_g_r2_en[i]:
                    line = f"{FPGA_str}TOB_2 Leading gblock energy     {l1id_1[i]}     {lead_g_r1_en[i]} {lead_g_r2_en[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        lg_en_r_err += 1
                        #if dbg_mode == 1:
                        #    l1id_list.write(f"{l1id_1[i]}\n")

            #right subleading gblock
            
            if sub_g_r1_phi[i] != sub_g_r2_phi[i]:
                    line = f"{FPGA_str}TOB_2 Subleading gblock phi     {l1id_1[i]}     {sub_g_r1_phi[i]}        {sub_g_r2_phi[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err = phi_err + 1
                        sg_phi_r_err += 1

            if sub_g_r1_eta[i] != sub_g_r2_eta[i]:
                    line = f"{FPGA_str}TOB_2 Subleading gblock eta     {l1id_1[i]}     {sub_g_r1_eta[i]}       {sub_g_r2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        sg_eta_r_err += 1

            if sub_g_r1_en[i] != sub_g_r2_en[i]:
                    line = f"{FPGA_str}TOB_2 Subleading gblock energy  {l1id_1[i]}     {sub_g_r1_en[i]} {sub_g_r2_en[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        sg_en_r_err += 1
                        #if dbg_mode == 1:
                        #    l1id_list.write(f"{l1id_1[i]}\n")

            #right leading gjet
            
            if lead_j_r1_phi[i] != lead_j_r2_phi[i]:
                    line = f"{FPGA_str}TOB_2 Leading gjet phi           {l1id_1[i]}    {lead_j_r1_phi[i]}        {lead_j_r2_phi[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        phi_err += 1
                        lj_phi_r_err += 1

            if lead_j_r1_eta[i] != lead_j_r2_eta[i]:
                    line = f"{FPGA_str}TOB_2 Leading gjet eta           {l1id_1[i]}    {lead_j_r1_eta[i]}       {lead_j_r2_eta[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        eta_err += 1
                        lj_eta_r_err += 1

            if lead_j_r1_en[i] != lead_j_r2_en[i]:
                    line = f"{FPGA_str}TOB_2 Leading gjet energy      {l1id_1[i]}    {lead_j_r1_en[i]} {lead_j_r2_en[i]}\n"
                    if not line in clines:
                        f.write(line)
                        clines.add(line)
                        nrg_err += 1
                        lj_en_r_err += 1
                        if (dbg_mode == 1):
                            l1id_list.write(f"{l1id_1[i]}\n")

        else:
            f.write(f"Error: L1ID mismatch: {l1id_1[i]}    {l1id_2[i]}\n")
            l1id_mis += 1



        #error summary file
    summ = open(f"ComparisonOutputs/ComparisonTextFiles/Run{RUN_num}_FPGA{FPGA_str}_JetTOB_Error_Summary.txt","w+")

    summ.write(f"Events with mismatched L1IDs: {l1id_mis}\n")
    summ.write(f"Errors in phi: {phi_err}\n")
    summ.write(f"Errors in eta: {eta_err}\n")
    summ.write(f"Errors in energy: {nrg_err}\n")
    summ.write(f"Errors is LG1 phi: {lg_phi_l_err}\n")
    summ.write(f"Errors in LG1 eta: {lg_eta_l_err}\n")
    summ.write(f"Errors in LG1 energy: {lg_en_l_err}\n")
    summ.write(f"Errors in SG1 phi: {sg_phi_l_err}\n")
    summ.write(f"Errors in SG1 eta: {sg_eta_l_err}\n")
    summ.write(f"Errors in SG1 energy: {sg_en_l_err}\n")
    summ.write(f"Errors in LJ1 phi: {lj_phi_l_err}\n")
    summ.write(f"Errors in LJ1 eta: {lj_eta_l_err}\n")
    summ.write(f"Errors in LJ1 energy: {lj_en_l_err}\n")
    summ.write(f"Errors in LG2 phi: {lg_phi_r_err}\n")
    summ.write(f"Errors in LG2 eta: {lg_eta_r_err}\n")
    summ.write(f"Errors in LG2 energy: {lg_en_r_err}\n")
    summ.write(f"Errors in SG2 phi: {sg_phi_r_err}\n")
    summ.write(f"Errors in SG2 eta: {sg_eta_r_err}\n")
    summ.write(f"Errors in SG2 energy: {sg_en_r_err}\n")
    summ.write(f"Errors in LJ2 phi: {lj_phi_r_err}\n")
    summ.write(f"Errors in LJ2 eta: {lj_eta_r_err}\n")
    summ.write(f"Errors in LJ2 energy: {lj_en_r_err}\n")

    f.close()
    summ.close()
    if dbg_mode == 1:
        l1id_list.close()

    #check to see if Saturation Bit is ever on in either simulation or in the firmware
    satbit_check_sim = open(f"SatBitCheck/Run{RUN_num}_FPGA{FPGA_str}_SatBitCheck_Sim.txt", 'w+')
    satbit_check_readout = open(f"SatBitCheck/Run{RUN_num}_FPGA{FPGA_str}_SatBitCheck_Readout.txt", "w+")

    satbit_check_sim.write(f"Run {RUN_num} FPGA {FPGA_str} Events with Saturation Bit = 1 in C simulation\n")
    satbit_check_readout.write(f"Run {RUN_num} FPGA {FPGA_str} Events with Saturation Bit = 1 in gFEX Readout Path\n")

    satbit_check_sim.write(f"------------------------------------------------------------------------------------------\n")
    satbit_check_sim.write(f"TOB Type             Energy (Gev)         Eta Index      Phi Index\n")
    satbit_check_sim.write(f"------------------------------------------------------------------------------------------\n")

    satbit_check_readout.write(f"------------------------------------------------------------------------------------------\n")
    satbit_check_readout.write(f"TOB Type             Energy (GeV)         Eta Index      Phi Index\n")
    satbit_check_readout.write(f"------------------------------------------------------------------------------------------\n")
    #loop through events
    for i in range(int(len(l1id_1))):
	    if lead_g_l1_satbit[i] == "1":
	        satbit_check_readout.write(f"Leading Small-R Jet        {int(lead_g_l1_en[i])*0.2}      {int(lead_g_l1_eta[i])}      {int(lead_g_l1_phi[i])}\n")
	    if sub_g_l1_satbit[i] == "1":
	        satbit_check_readout.write(f"Subleading Small-R Jet        {int(sub_g_l1_en[i])*0.2}      {int(sub_g_l1_eta[i])}      {int(sub_g_l1_phi[i])}\n")        
	    if lead_j_l1_satbit[i] == "1":
	        satbit_check_readout.write(f"Leading Large-R Jet        {int(lead_j_l1_en[i])*0.2}      {int(lead_j_l1_eta[i])}      {int(lead_j_l1_phi[i])}\n")
	    if lead_g_r1_satbit[i] == "1":
	        satbit_check_readout.write(f"Leading Small-R Jet        {int(lead_g_r1_en[i])*0.2}      {int(lead_g_r1_eta[i])}      {int(lead_g_r1_phi[i])}\n")
	    if sub_g_r1_satbit[i] == "1":
	        satbit_check_readout.write(f"Subleading Small-R Jet        {int(sub_g_r1_en[i])*0.2}      {int(sub_g_r1_eta[i])}      {int(sub_g_r1_phi[i])}\n")        
	    if lead_j_r1_satbit[i] == "1":
	        satbit_check_readout.write(f"Leading Large-R Jet        {int(lead_j_r1_en[i])*0.2}      {int(lead_j_r1_eta[i])}      {int(lead_j_r1_phi[i])}\n")

	    if lead_g_l2_satbit[i] == "1":
	        satbit_check_sim.write(f"Leading Small-R Jet        {int(lead_g_l2_en[i])*0.2}      {int(lead_g_l2_eta[i])}      {int(lead_g_l2_phi[i])}\n")
	    if sub_g_l2_satbit[i] == "1":
	        satbit_check_sim.write(f"Subleading Small-R Jet        {int(sub_g_l2_en[i])*0.2}      {int(sub_g_l2_eta[i])}      {int(sub_g_l2_phi[i])}\n")        
	    if lead_j_l2_satbit[i] == "1":
	        satbit_check_sim.write(f"Leading Large-R Jet        {int(lead_j_l2_en[i])*0.2}      {int(lead_j_l2_eta[i])}      {int(lead_j_l2_phi[i])}\n")
	    if lead_g_r2_satbit[i] == "1":
	        satbit_check_sim.write(f"Leading Small-R Jet        {int(lead_g_r2_en[i])*0.2}      {int(lead_g_r2_eta[i])}      {int(lead_g_r2_phi[i])}\n")
	    if sub_g_r2_satbit[i] == "1":
	        satbit_check_sim.write(f"Subleading Small-R Jet        {int(sub_g_r2_en[i])*0.2}      {int(sub_g_r2_eta[i])}      {int(sub_g_r2_phi[i])}\n")        
	    if lead_j_r2_satbit[i] == "1":
	        satbit_check_sim.write(f"Leading Large-R Jet        {int(lead_j_r2_en[i])*0.2}      {int(lead_j_r2_eta[i])}      {int(lead_j_r2_phi[i])}\n")

    satbit_check_sim.close()
    satbit_check_readout.close()

    return
    
    

#TODO: redesign to match jtob and clean up
def compare_gtobs(f1, f2, RUN_num, FPGA_num):
    #IMPORTANT: f1 must be dumped data, and f2 must be simulated output, right now they have different format (simulated data does not have the second link yet)

    #setup string based on FPGA_num
    FPGA_str = get_fpgastr(FPGA_num)

    lines1 = get_lines(f1)
    lines2 = get_lines(f2)

    '''
    for global TOBs, format of links is:
    Link 0:
    Word 0: JWJ Missing Hard Term
    Word 1: JWJ Missing Soft Term
    Word 2: JWJ Missing Energy (full)
    Word 3-5: unused
    Word 6: Trailer

    Link 1:
    Word 0: Noise Cut MET pieces
    Word 1: RMS MET pieces
    Word 2: NC and RMS Sum ET
    Word 3-5: unused
    Word 6: Trailer

    '''
    #l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr, nc_met_comp, rms_met_comp, alt_met_sums, altmet_trlr
    l1id_1, jwj_mht_comp_1, jwj_met_comp_1, jwj_mst_comp_1, jwj_trlr_1, nc_met_comp_1, rms_met_comp_1, alt_met_sums_1, altmet_trlr_1 = parse_gtobs(lines1)

    l1id_2, jwj_mht_comp_2, jwj_met_comp_2, jwj_mst_comp_2, jwj_trlr_2 = parse_sim_gtobs(lines2)

    jwj_mhtx_1, jwj_mhty_1 = parse_gwords(jwj_mht_comp_1)
    jwj_metx_1, jwj_mety_1 = parse_gwords(jwj_met_comp_1)
    jwj_mstx_1, jwj_msty_1 = parse_gwords(jwj_mst_comp_1)
    

    jwj_mhtx_2, jwj_mhty_2 = parse_gwords(jwj_mht_comp_2)
    jwj_metx_2, jwj_mety_2 = parse_gwords(jwj_met_comp_2)
    jwj_mstx_2, jwj_msty_2 = parse_gwords(jwj_mst_comp_2)

    #TODO: alt MET algo parsing when they are implemented


    #open output file
    out = open("METTOB_dbg/compare_METTOBs_FPGA%s_Run%d.txt" %(FPGA_str,RUN_num),'w+')

    out.write('Mismatches found in Global(MET) TOBs for FPGA-%s run %d \n'%(FPGA_str, RUN_num))
    out.write('Type                             L1ID   Value 1    Value 2\n')
    
    #set of lines to make sure there are no duplicates - useful for when we compare to STF readouts that could have many repeated events in the format
    clines = set(out)

    #set error counters
    l1id_mis = 0
    mhtx_err = 0
    mhty_err = 0
    mstx_err = 0
    msty_err = 0
    metx_err = 0
    mety_err = 0


    #Loop through events by bcid
    for i in range(len(l1id_1)):

        if l1id_1[i] == l1id_2[i]:

            if jwj_mhtx_1[i] != jwj_mhtx_2[i]:
                line = f"JWJ_MHTX      {l1id_1[i]}   {jwj_mhtx_1[i]}    {jwj_mhtx_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    mhtx_err += 1
                    
            if jwj_mhty_1[i] != jwj_mhty_2[i]:
                line = f"JWJ_MHTY      {l1id_1[i]}   {jwj_mhty_1[i]}    {jwj_mhty_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    mhty_err += 1

            if jwj_metx_1[i] != jwj_metx_2[i]:
                line = f"JWJ_METX      {l1id_1[i]}   {jwj_metx_1[i]}    {jwj_metx_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    metx_err += 1

            if jwj_mety_1[i] != jwj_mety_2[i]:
                line = f"JWJ_METY      {l1id_1[i]}   {jwj_mety_1[i]}    {jwj_mety_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    mety_err += 1

            if jwj_mstx_1[i] != jwj_mstx_2[i]:
                line = f"JWJ_MSTX      {l1id_1[i]}   {jwj_mstx_1[i]}    {jwj_mstx_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    mstx_err += 1

            if jwj_msty_1[i] != jwj_msty_2[i]:
                line = f"JWJ_MSTY      {l1id_1[i]}   {jwj_msty_1[i]}    {jwj_msty_2[i]}\n"
                if not line in clines:
                    out.write(line)
                    clines.add(line)
                    msty_err += 1

        else:
            out.write(f"Error: L1ID mismatch: {l1id_1[i]}    {l1id_2[i]}\n")
            l1id_mis += 1

    out.close()

    summ = open(f"METTOB_dbg/Run{RUN_num}_FPGA{FPGA_str}_METTOB_Error_Summary.txt","w+")

    summ.write(f"Events with mismatched L1IDs: {l1id_mis}\n")
    summ.write(f"errors in hard term x: {mhtx_err}\n")
    summ.write(f"errors in hard term y: {mhty_err}\n")
    summ.write(f"errors in soft term x: {mstx_err}\n")
    summ.write(f"errors in soft term y: {msty_err}\n")
    summ.write(f"errors in MET x: {metx_err}\n")
    summ.write(f"errors in MET y: {mety_err}\n")


    summ.close()

    return


'''
Comparison Plot Helper Functions
----------------------------------------------------------------------------------------------------------------------------------------
'''

#helper function to plot measured vs simulated values
#en_or_ang = integer to distinguish between energy, eta, or phi
def scat_plot(meas, sim, en_or_ang, xstr, ystr, plotstr, folder, RUN_num, FPGA_str):
    

    #need different lengths of lines when looking at energy or eta/phi
    n = 0
    if en_or_ang == 0:
        n = 10 * int(np.max(meas))
        if n == 0:
            n = 6000
        if n < 0:
            n = 6000
    else:
        n = 400

    #make a "truth" line of data points that agree
    x = array('d')
    y = array('d')

    for i in range(n):
        x.append(0.1*i)
        y.append(0.1*i)
    
    truth = TGraph(n,x,y)
    truth.SetMinimum(-5)
    truth.SetMaximum(n/10)
    truth.SetLineColor(4)
    truth.SetTitle("Agreement")

    n_dat = int(len(meas))
    dat = TGraph(n_dat, meas, sim)  
    dat.SetMinimum(-5)
    dat.SetMaximum(n/10)  
    dat.SetMarkerStyle(7)     
    dat.SetMarkerColor(2)
    dat.SetTitle("Data")

    c = TCanvas('c', f'{plotstr}', 200, 10, 700, 500)

    mg = TMultiGraph()
    mg.Add(truth, "L")
    mg.Add(dat, "P")

    mg.SetTitle(f'Run {RUN_num} FPGA {FPGA_str} {plotstr}')
    mg.GetXaxis().SetTitle( xstr )
    mg.GetYaxis().SetTitle( ystr)
    
    mg.Draw("A")

    c.BuildLegend(0.1,0.9,0.3,0.8)

    c.Print(f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.pdf")

    return



#helper function for creating eta/phi space plots of difference counts
#d is the energy array being passed in, d phi/eta are the eta and phi arrays of the energy array
#thresh is the threshold for plotting counts, map_type specifies between difference plots (0) or hit maps (1), w is weight type mode, 0 is counts, 1 is value of d[i]
def etaphi_map(d, dphi, deta, thresh, map_type, w, plotstr, folder, RUN_num, FPGA_str):

    
    map_str = ''
    map_str_file = ''
    weight_str = ''
    if map_type == 0:
        if w == 0:
            map_str = 'E_T Difference Counts'
            map_str_file = 'DiffCounts'
            weight_str = 'Count'
        if w == 1:
            map_str = 'E_T Difference Values'
            map_str_file = 'DiffVal'
            weight_str = 'Difference'
    if map_type == 1:
        map_str = 'E_T'
        map_str_file = 'HitMap'
        weight_str = 'Count'    
   
    if thresh < 0:
        c = TCanvas('c','Eta Phi Space',200, 10, 700, 500)
        c.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_LessThan{thresh}.pdf"
        hist = TH2F('hist', f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} < {thresh};eta [bin number];phi [row number]; {weight_str}',36,2.0,38.0,32,0.0,32.0)
        #loop through array of the energy values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
        #loop through columns and rows, match by measured eta/phi
            for irow in range(32):
             for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if(d[i] < thresh):
                            hist.Fill(float(icolumn)+0.5, float(irow)+0.5, count)

        hist.SetStats(0)
        hist.Draw("colz")

        c.Print(name)   

    elif thresh > 0:
        c = TCanvas('c','Eta Phi Space',200, 10, 700, 500)
        c.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_GreaterThan{thresh}.pdf"
        hist = TH2F('hist', f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} > {thresh};eta [bin number];phi [row number]; {weight_str}',36,2.0,38.0,32,0.0,32.0)
        #loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
        #loop through columns and rows, match by measured eta/phi
            for irow in range(32):
             for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if(d[i] > thresh):
                            hist.Fill(float(icolumn)+0.5, float(irow)+0.5, count)

        hist.SetStats(0)
        hist.Draw("colz")

        c.Print(name) 

    #need to do both for 0
    elif thresh == 0:
        ca = TCanvas('ca','Eta Phi Space',200, 10, 700, 500)
        ca.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_LessThan{thresh}.pdf"
        hist1 = TH2F('hist1', f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} < {thresh};eta [bin number];phi [row number]; {weight_str}',36,2.0,38.0,32,0.0,32.0)
        #loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
        #loop through columns and rows, match by measured eta/phi
            for irow in range(32):
             for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if(d[i] < thresh):
                            hist1.Fill(float(icolumn)+0.5, float(irow)+0.5, count)

        hist1.SetStats(0)
        hist1.Draw("colz")

        ca.Print(name)

        cb = TCanvas('cb','Eta Phi Space',200, 10, 700, 500)
        cb.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_GreaterThan{thresh}.pdf"
        hist2 = TH2F('hist2', f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} > {thresh};eta [bin number];phi [row number]; {weight_str}',36,2.0,38.0,32,0.0,32.0)
        #loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
        #loop through columns and rows, match by measured eta/phi
            for irow in range(32):
             for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if(d[i] > thresh):
                            hist2.Fill(float(icolumn)+0.5, float(irow)+0.5, count)

        hist2.SetStats(0)
        hist2.Draw("colz")

        cb.Print(name)     

        cc = TCanvas('cc','Eta Phi Space',200, 10, 700, 500)
        cc.SetRightMargin(0.18)
        name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}_{map_str_file}_Equal{thresh}.pdf"
        hist3 = TH2F('hist3', f'Run {RUN_num} FPGA {FPGA_str} {plotstr} {map_str} = {thresh};eta [bin number];phi [row number]; {weight_str}',36,2.0,38.0,32,0.0,32.0)
        #loop through array of the energy difference values
        for i in range(len(d)):
            count = 0.0
            if w == 0:
                count = 1.0
            if w == 1:
                count = float(d[i])
        #loop through columns and rows, match by measured eta/phi
            for irow in range(32):
             for icolumn in range(40):
                    if (dphi[i] == irow and deta[i] == icolumn):
                        if(d[i] == thresh):
                            hist3.Fill(float(icolumn)+0.5, float(irow)+0.5, count)

        hist3.SetStats(0)
        hist3.Draw("colz")

        cc.Print(name)     

    return

#TProfile function for plotting
def profile_plot(x, y, xstr, ystr, plotstr, folder, RUN_num, FPGA_str):
    c = TCanvas('c', 'Profile Plots', 200, 10, 700, 500)
    c.SetRightMargin(0.18)

    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.pdf"
    prof = TProfile('prof', f"Run{RUN_num} FPGA {FPGA_str} {plotstr}; {xstr}; {ystr}",1000, 0.0,float(max(x)))

    for i in range(len(x)):
        prof.Fill(float(x[i]),float(y[i]))

    prof.Draw("AP")

    c.Print(name)

    return

def hist_1d(arr, xstr, plotstr, b, folder, RUN_num, FPGA_str):
    c = TCanvas('c', 'Histograms', 200, 10, 700, 500)
    c.SetRightMargin(0.18)
    
    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.pdf"
    #add 1 to the bound so the max is included 
    hist = TH1F('hist', f'Run{RUN_num} FPGA {FPGA_str} {plotstr}; {xstr}', 100, -(b+1.0), b+1.0)
    for i in range(len(arr)):
        hist.Fill(arr[i])

    hist.Draw()

    c.Print(name)

    return

'''
Functions to generate comparison plots
----------------------------------------------------------------------------------------------------------------------------------------
'''

#Function to plot energy, phi, eta of jet TOBs for simulation vs actual data in a dumped event data file
#f1 should be dumped TOBs, f2 should be simulation output TOBs
def make_comp_plots(f1, f2, RUN_num, FPGA_num):

    #setup string based on FPGA_num
    FPGA_str = get_fpgastr(FPGA_num)

    #get lines from file
    lines1 = get_lines(f1)
    lines2 = get_lines(f2)    

    #parsing out the different rows
    l1id_1,lead_g_l1,sub_g_l1,lead_j_l1,bc_l1,lead_g_r1,sub_g_r1,lead_j_r1,bc_r1 = parse_jtobs(lines1)

    l1id_2,lead_g_l2,sub_g_l2,lead_j_l2,bc_l2,lead_g_r2,sub_g_r2,lead_j_r2,bc_r2 = parse_jtobs(lines2)

    #if length of l1ids does not match, stop and debug
    if len(l1id_1) != len(l1id_2):
        print("Error: incorrect number of l1ids for comparison, debug and try again\n")
            
    #get phi, eta, energy
    lead_g_l1_phi, lead_g_l1_eta, lead_g_l1_en, lead_g_l1_satbit, sub_g_l1_phi, sub_g_l1_eta, sub_g_l1_en, sub_g_l1_satbit, lead_j_l1_phi, lead_j_l1_eta, lead_j_l1_en, lead_j_l1_satbit, bcid_l1, lead_g_r1_phi, lead_g_r1_eta, lead_g_r1_en, lead_g_r1_satbit, sub_g_r1_phi, sub_g_r1_eta, sub_g_r1_en, sub_g_r1_satbit, lead_j_r1_phi, lead_j_r1_eta, lead_j_r1_en, lead_j_r1_satbit, bcid_r1 = jTOB_full_parse(lines1, 10)

    lead_g_l2_phi, lead_g_l2_eta, lead_g_l2_en, lead_g_l2_satbit, sub_g_l2_phi, sub_g_l2_eta, sub_g_l2_en, sub_g_l2_satbit, lead_j_l2_phi, lead_j_l2_eta, lead_j_l2_en, lead_j_l2_satbit, bcid_l2, lead_g_r2_phi, lead_g_r2_eta, lead_g_r2_en, lead_g_r2_satbit, sub_g_r2_phi, sub_g_r2_eta, sub_g_r2_en, sub_g_r2_satbit, lead_j_r2_phi, lead_j_r2_eta, lead_j_r2_en, lead_j_r2_satbit, bcid_r2 = jTOB_full_parse(lines2, 10)  

    #add the left and right halves for the energy, phi, and eta values for each of the three TOB word types (Jet, Leading gblk, Subleading gblk)

    l1id_1 = [int(s,16) for s in l1id_1]
    l1id_2 = [int(s,16) for s in l1id_2]

    meas_l1id = append_arr(l1id_1, l1id_1)
    sim_l1id = append_arr(l1id_2, l1id_2)


    #Jet
    jet_en_meas = append_arr(lead_j_l1_en, lead_j_r1_en)
    jet_en_sim = append_arr(lead_j_l2_en, lead_j_r2_en)

    jet_phi_meas = append_arr(lead_j_l1_phi, lead_j_r1_phi)
    jet_phi_sim = append_arr(lead_j_l2_phi, lead_j_r2_phi)

    jet_eta_meas = append_arr(lead_j_l1_eta, lead_j_r1_eta)
    jet_eta_sim = append_arr(lead_j_l2_eta, lead_j_r2_eta)

    #Leading GBlock
    lg_en_meas = append_arr(lead_g_l1_en, lead_g_r1_en)
    lg_en_sim = append_arr(lead_g_l2_en, lead_g_r2_en)

    lg_phi_meas = append_arr(lead_g_l1_phi, lead_g_r1_phi)
    lg_phi_sim = append_arr(lead_g_l2_phi, lead_g_r2_phi)

    lg_eta_meas = append_arr(lead_g_l1_eta, lead_g_r1_eta)
    lg_eta_sim = append_arr(lead_g_l2_eta, lead_g_r2_eta)

    #Subleading GBlock
    sg_en_meas = append_arr(sub_g_l1_en, sub_g_r1_en)
    sg_en_sim = append_arr(sub_g_l2_en, sub_g_r2_en)

    sg_phi_meas = append_arr(sub_g_l1_phi, sub_g_r1_phi)
    sg_phi_sim = append_arr(sub_g_l2_phi, sub_g_r2_phi)

    sg_eta_meas = append_arr(sub_g_l1_eta, sub_g_r1_eta)
    sg_eta_sim = append_arr(sub_g_l2_eta, sub_g_r2_eta)

    #comp plot specific strings
    compx_str = "TOBs from gFEX Readout Path"
    compy_str = "Emulated from gFEX Input Readout"

    comp_folder_str = "ComparisonOutputs/TOBComparisonPlots/"

    #For delay testing only
    delay_val = 16
    #delay_str = f"_InterDelay{delay_val}"
    delay_str = ""


    #comp_plot(meas, sim, en_or_ang, xstr, ystr, plotstr, RUN_num, FPGA_str)
    scat_plot(jet_en_meas, jet_en_sim, 0, f"{larger_str} {et_str} {compx_str}", f"{larger_str} {et_str} {compy_str}", f"{larger_str}_{et_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(jet_phi_meas, jet_phi_sim, 1, f"{larger_str} {phi_str} {compx_str}", f"{larger_str} {phi_str} {compy_str}", f"{larger_str}_{phi_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(jet_eta_meas, jet_eta_sim, 2, f"{larger_str} {eta_str} {compx_str}", f"{larger_str} {eta_str} {compy_str}", f"{larger_str}_{eta_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)

    scat_plot(lg_en_meas, lg_en_sim, 0, f"{lsmallr_str} {et_str} {compx_str}", f"{lsmallr_str} {et_str} {compy_str}", f"{lsmallr_str}_{et_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(lg_phi_meas, lg_phi_sim, 1, f"{lsmallr_str} {phi_str} {compx_str}", f"{lsmallr_str} {phi_str} {compy_str}", f"{lsmallr_str}_{phi_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(lg_eta_meas, lg_eta_sim, 2, f"{lsmallr_str} {eta_str} {compx_str}", f"{lsmallr_str} {eta_str} {compy_str}", f"{lsmallr_str}_{eta_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)

    scat_plot(sg_en_meas, sg_en_sim, 0, f"{ssmallr_str} {et_str} {compx_str}", f"{ssmallr_str} {et_str} {compy_str}", f"{ssmallr_str}_{et_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(sg_phi_meas, sg_phi_sim, 1, f"{ssmallr_str} {phi_str} {compx_str}", f"{ssmallr_str} {phi_str} {compy_str}", f"{ssmallr_str}_{phi_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)
    scat_plot(sg_eta_meas, sg_eta_sim, 2, f"{ssmallr_str} {eta_str} {compx_str}", f"{ssmallr_str} {eta_str} {compy_str}", f"{ssmallr_str}_{eta_str}_Comparisons{delay_str}", comp_folder_str, RUN_num, FPGA_str)

    if temp_check == 1 and FPGA_str == 'A':
        fw_large_check = array('d')
        sim_large_check = array('d')

        for i in range(len(jet_en_meas)):
            if int(jet_en_meas[i]) % 8 == 0:
                fw_large_check.append(jet_en_meas[i])
                sim_large_check.append(jet_en_sim[i])

        scat_plot(fw_large_check, sim_large_check, 0, f"{larger_str} {et_str} {compx_str}",
                  f"{larger_str} {et_str} {compy_str}", f"{larger_str}_{et_str}_Comparisons_Mod8", comp_folder_str, RUN_num,
                  FPGA_str)

    



    return

def make_diff_plots(f1, f2, RUN_num, FPGA_num):

    #setup string based on FPGA_num
    FPGA_str = get_fpgastr(FPGA_num)

    #get lines from file
    lines1 = get_lines(f1)
    lines2 = get_lines(f2)    

    #parsing out the different rows
    l1id_1,lead_g_l1,sub_g_l1,lead_j_l1,bc_l1,lead_g_r1,sub_g_r1,lead_j_r1,bc_r1 = parse_jtobs(lines1)

    l1id_2,lead_g_l2,sub_g_l2,lead_j_l2,bc_l2,lead_g_r2,sub_g_r2,lead_j_r2,bc_r2 = parse_jtobs(lines2)

    #if length of l1ids does not match, stop and debug
    if len(l1id_1) != len(l1id_2):
        print("Error: incorrect number of l1ids for comparison, debug and try again\n")
            
    #get phi, eta, energy
    lead_g_l1_phi, lead_g_l1_eta, lead_g_l1_en, lead_g_l1_satbit, sub_g_l1_phi, sub_g_l1_eta, sub_g_l1_en, sub_g_l1_satbit, lead_j_l1_phi, lead_j_l1_eta, lead_j_l1_en, lead_j_l1_satbit, bcid_l1, lead_g_r1_phi, lead_g_r1_eta, lead_g_r1_en, lead_g_r1_satbit, sub_g_r1_phi, sub_g_r1_eta, sub_g_r1_en, sub_g_r1_satbit, lead_j_r1_phi, lead_j_r1_eta, lead_j_r1_en, lead_j_r1_satbit, bcid_r1 = jTOB_full_parse(lines1, 10)

    lead_g_l2_phi, lead_g_l2_eta, lead_g_l2_en, lead_g_l1_satbit, sub_g_l2_phi, sub_g_l2_eta, sub_g_l2_en, sub_g_l2_satbit, lead_j_l2_phi, lead_j_l2_eta, lead_j_l2_en, lead_j_l2_satbit, bcid_l2, lead_g_r2_phi, lead_g_r2_eta, lead_g_r2_en, lead_g_r2_satbit, sub_g_r2_phi, sub_g_r2_eta, sub_g_r2_en, sub_g_r2_satbit, lead_j_r2_phi, lead_j_r2_eta, lead_j_r2_en, lead_j_r2_satbit, bcid_r2 = jTOB_full_parse(lines2, 10)  

    #add the left and right halves for the energy, phi, and eta values for each of the three TOB word types (Jet, Leading gblk, Subleading gblk)

    #Jet
    jet_en_meas = append_arr(lead_j_l1_en, lead_j_r1_en)
    jet_en_sim = append_arr(lead_j_l2_en, lead_j_r2_en)

    jet_phi_meas = append_arr(lead_j_l1_phi, lead_j_r1_phi)
    jet_phi_sim = append_arr(lead_j_l2_phi, lead_j_r2_phi)

    jet_eta_meas = append_arr(lead_j_l1_eta, lead_j_r1_eta)
    jet_eta_sim = append_arr(lead_j_l2_eta, lead_j_r2_eta)

    #Leading GBlock
    lg_en_meas = append_arr(lead_g_l1_en, lead_g_r1_en)
    lg_en_sim = append_arr(lead_g_l2_en, lead_g_r2_en)

    lg_phi_meas = append_arr(lead_g_l1_phi, lead_g_r1_phi)
    lg_phi_sim = append_arr(lead_g_l2_phi, lead_g_r2_phi)

    lg_eta_meas = append_arr(lead_g_l1_eta, lead_g_r1_eta)
    lg_eta_sim = append_arr(lead_g_l2_eta, lead_g_r2_eta)

    #Subleading GBlock
    sg_en_meas = append_arr(sub_g_l1_en, sub_g_r1_en)
    sg_en_sim = append_arr(sub_g_l2_en, sub_g_r2_en)

    sg_phi_meas = append_arr(sub_g_l1_phi, sub_g_r1_phi)
    sg_phi_sim = append_arr(sub_g_l2_phi, sub_g_r2_phi)

    sg_eta_meas = append_arr(sub_g_l1_eta, sub_g_r1_eta)
    sg_eta_sim = append_arr(sub_g_l2_eta, sub_g_r2_eta)  

    #make energy difference histograms for plotting in an eta/phi map
    jet_en_diff = get_diff(jet_en_meas, jet_en_sim)
    lg_en_diff = get_diff(lg_en_meas, lg_en_sim)
    sg_en_diff = get_diff(sg_en_meas, sg_en_sim)


    diffcount_folder_str = "ComparisonOutputs/DiffCountMaps/"

    #etaphi_map(d, dphi, deta, thresh, map_type, w, plotstr, RUN_num, FPGA_str)
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, -10, 0, 0, larger_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, -2,  0, 0, larger_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, 0,  0, 0, larger_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, 2,  0, 0, larger_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, 10, 0, 0, larger_str, diffcount_folder_str, RUN_num, FPGA_str)

    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, -10,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, -2,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, 0,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, 2,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, 10,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    
    #troublshooting
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, 1000,  0, 0, lsmallr_str, diffcount_folder_str, RUN_num, FPGA_str)

    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, -10, 0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, -2,  0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, 0,  0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, 2,  0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, 10,  0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)

    #troubleshooting
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, 1000,  0, 0, ssmallr_str, diffcount_folder_str, RUN_num, FPGA_str)

    diffval_folder_str = "ComparisonOutputs/DiffValMaps/"

    #make eta phi plots of weighted energies 
    etaphi_map(jet_en_diff, jet_phi_meas, jet_eta_meas, 0,  0, 1, larger_str, diffval_folder_str, RUN_num, FPGA_str)
    etaphi_map(lg_en_diff, lg_phi_meas, lg_eta_meas, 0,  0, 1, lsmallr_str, diffval_folder_str, RUN_num, FPGA_str)
    etaphi_map(sg_en_diff, sg_phi_meas, sg_eta_meas, 0,  0, 1, ssmallr_str, diffval_folder_str, RUN_num, FPGA_str)

    diffhist_folder_str = "ComparisonOutputs/DiffHistograms/"
    diffTOBMap_folder_str = "ComparisonOutputs/TOBDiffMaps/"

    #histograms of difference and relative difference
    h_axis1 = "E_T TOB - E_T SIM"
    h_axis2 = "(E_T TOB - E_T SIM) / E_T SIM"  
    h_label1 = "AbsoluteResiduals"
    h_label2 = "RelativeResiduals"

    rel_jet_en_diff = array('f')
    for i in range(len(jet_en_diff)):
        if jet_en_sim[i] != 0:
            rel_jet_en_diff.append(float(jet_en_diff[i]/jet_en_sim[i]))
        else:
            if jet_en_diff[i] == 0:
                rel_jet_en_diff.append(0.0)
            #don't allow this for now
            #else:
                #rel_jet_en_diff.append(float(jet_en_diff[i]))
    
    lim = float(max(jet_en_diff))

    hist_1d(jet_en_diff, h_axis1, f"{larger_str}_{h_label1}", lim, diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(jet_en_diff, h_axis1,f"{larger_str}_{h_label1}_Crop", 200.0, diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(rel_jet_en_diff, h_axis2, f"{larger_str}_{h_label2}", 0.1, diffhist_folder_str, RUN_num, FPGA_str)

    #make mask differences for whether or not an energy difference is 0
    diff_mask = [None] * len(jet_en_diff)

    #try not allowing zeros
    nonzero_jet_en_diff = array('d')
    for i in range(len(jet_en_diff)):
        if jet_en_diff[i] != 0:
            nonzero_jet_en_diff.append(jet_en_diff[i])
            #make diff mask 1 if the difference is non zero
            diff_mask[i] = 1
        else:
            diff_mask[i] = 0
    
    nonzero_rel_jet_en_diff = array('d')
    for i in range(len(rel_jet_en_diff)):
        if rel_jet_en_diff[i] != 0:
            nonzero_rel_jet_en_diff.append(rel_jet_en_diff[i])

    hist_1d(nonzero_jet_en_diff, h_axis1, f"NonZero_{larger_str}_{h_label1}", lim, diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(nonzero_jet_en_diff, h_axis1, f"NonZero_{larger_str}_{h_label1}_Crop", 200.0, diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(nonzero_rel_jet_en_diff, h_axis2, f"NonZero_{larger_str}_{h_label2}", 1.1, diffhist_folder_str, RUN_num, FPGA_str)

    #Absolute Residuals for Small R Jets
    hist_1d(lg_en_diff, h_axis1, f"{lsmallr_str}_{h_label1}", float(max(lg_en_diff)), diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(lg_en_diff, h_axis1,f"{lsmallr_str}_{h_label1}_Crop", 200.0, diffhist_folder_str, RUN_num, FPGA_str)

    hist_1d(sg_en_diff, h_axis1, f"{ssmallr_str}_{h_label1}", float(max(sg_en_diff)), diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(sg_en_diff, h_axis1,f"{ssmallr_str}_{h_label1}_Crop", 200.0, diffhist_folder_str, RUN_num, FPGA_str)

    #make eta/phi maps of two populations: one where the difference is 0, one where it isn't
    nzd_jet_en = array('d')
    nzd_jet_phi = array('d')
    nzd_jet_eta = array('d')

    zd_jet_en = array('d')
    zd_jet_phi = array('d')
    zd_jet_eta = array('d')

    for i in range(len(diff_mask)):
        #if difference is zero
        if diff_mask[i] == 0:
            zd_jet_en.append(jet_en_meas[i])
            zd_jet_phi.append(jet_phi_meas[i])
            zd_jet_eta.append(jet_eta_meas[i])

        #if difference is nonzero
        if diff_mask[i] == 1:
            nzd_jet_en.append(jet_en_meas[i])
            nzd_jet_phi.append(jet_phi_meas[i])
            nzd_jet_eta.append(jet_eta_meas[i])
    
    etaphi_map(zd_jet_en, zd_jet_phi, zd_jet_eta, 0, 1, 0, f"{larger_str}_{et_str}_ZeroDiff", diffTOBMap_folder_str, RUN_num, FPGA_str)
    etaphi_map(nzd_jet_en, nzd_jet_phi, nzd_jet_eta, 0, 1, 0, f"{larger_str}_{et_str}_NonZeroDiff", diffTOBMap_folder_str, RUN_num, FPGA_str)
    
    #make histograms of energies
    zd_bound = 0.0
    if len(zd_jet_en) == 0.0:
        zd_bound = 1.0
    if len(zd_jet_en) != 0.0:
        zd_bound = float(max(zd_jet_en))

    nzd_bound = 0.0
    if len(nzd_jet_en) == 0.0:
        nzd_bound = 1.0
    if len(nzd_jet_en) != 0.0:
        nzd_bound = float(max(nzd_jet_en))

    hist_1d(zd_jet_en, "E_T of Events With No Difference in TOB Comparisons", f"{larger_str}_{et_str}_ZeroDiffHist", zd_bound, diffhist_folder_str, RUN_num, FPGA_str)
    hist_1d(nzd_jet_en, "E_T of Events With Nonzero Difference in TOB Comparisons", f"{larger_str}_{et_str}_NonZeroDiffHist", nzd_bound, diffhist_folder_str, RUN_num, FPGA_str)


    return


def make_TOB_hitmaps(atob,btob,ctob,RUN_num):

    hitmap_str = "ReadoutTOBHitMaps/" 

    alines = get_lines(atob)
    blines = get_lines(btob)
    clines = get_lines(ctob)

    #parsing out the different rows
    l1id_a,lead_g_la,sub_g_la,lead_j_la,bc_la,lead_g_ra,sub_g_ra,lead_j_ra,bc_ra = parse_jtobs(alines)
    l1id_b,lead_g_lb,sub_g_lb,lead_j_lb,bc_lb,lead_g_rb,sub_g_rb,lead_j_rb,bc_rb = parse_jtobs(blines)
    l1id_c,lead_g_lc,sub_g_lc,lead_j_lc,bc_lc,lead_g_rc,sub_g_rc,lead_j_rc,bc_rc = parse_jtobs(clines)


    #get phi, eta, energy
    lead_g_la_phi, lead_g_la_eta, lead_g_la_en, lead_g_la_satbit, sub_g_la_phi, sub_g_la_eta, sub_g_la_en, sub_g_la_satbit, lead_j_la_phi, lead_j_la_eta, lead_j_la_en, lead_g_la_satbit, bcid_la, lead_g_ra_phi, lead_g_ra_eta, lead_g_ra_en, lead_g_ra_satbit, sub_g_ra_phi, sub_g_ra_eta, sub_g_ra_en, sub_g_ra_satbit, lead_j_ra_phi, lead_j_ra_eta, lead_j_ra_en, lead_j_ra_satbit, bcid_ra = jTOB_full_parse(alines, 10)

    lead_g_lb_phi, lead_g_lb_eta, lead_g_lb_en, lead_g_lb_satbit, sub_g_lb_phi, sub_g_lb_eta, sub_g_lb_en, sub_g_lb_satbit, lead_j_lb_phi, lead_j_lb_eta, lead_j_lb_en, lead_j_lb_satbit, bcid_lb, lead_g_rb_phi, lead_g_rb_eta, lead_g_rb_en, lead_g_rb_satbit, sub_g_rb_phi, sub_g_rb_eta, sub_g_rb_en, sub_g_rb_satbit, lead_j_rb_phi, lead_j_rb_eta, lead_j_rb_en, lead_j_rb_satbit, bcid_rb = jTOB_full_parse(blines, 10)

    lead_g_lc_phi, lead_g_lc_eta, lead_g_lc_en, lead_g_lc_satbit, sub_g_lc_phi, sub_g_lc_eta, sub_g_lc_en, sub_g_lc_satbit, lead_j_lc_phi, lead_j_lc_eta, lead_j_lc_en, lead_j_lc_satbit, bcid_lc, lead_g_rc_phi, lead_g_rc_eta, lead_g_rc_en, lead_g_rc_satbit, sub_g_rc_phi, sub_g_rc_eta, sub_g_rc_en, sub_g_rc_satbit, lead_j_rc_phi, lead_j_rc_eta, lead_j_rc_en, lead_j_rc_satbit, bcid_rc = jTOB_full_parse(clines, 10)

    #Jet energy
    a_jet_en = append_arr(lead_j_la_en,lead_j_ra_en)
    b_jet_en = append_arr(lead_j_lb_en,lead_j_rb_en)
    c_jet_en = append_arr(lead_j_lc_en,lead_j_rc_en)   

    #Jet phi  
    a_jet_phi = append_arr(lead_j_la_phi,lead_j_ra_phi)
    b_jet_phi = append_arr(lead_j_lb_phi,lead_j_rb_phi)
    c_jet_phi = append_arr(lead_j_lc_phi,lead_j_rc_phi) 

    #Jet eta
    a_jet_eta = append_arr(lead_j_la_eta,lead_j_ra_eta)
    b_jet_eta = append_arr(lead_j_lb_eta,lead_j_rb_eta)
    c_jet_eta = append_arr(lead_j_lc_eta,lead_j_rc_eta)

    #Leading gBlock energy
    a_lg_en = append_arr(lead_g_la_en,lead_g_ra_en)
    b_lg_en = append_arr(lead_g_lb_en,lead_g_rb_en)
    c_lg_en = append_arr(lead_g_lc_en,lead_g_rc_en)   

    #Leading gBlock phi  
    a_lg_phi = append_arr(lead_g_la_phi,lead_g_ra_phi)
    b_lg_phi = append_arr(lead_g_lb_phi,lead_g_rb_phi)
    c_lg_phi = append_arr(lead_g_lc_phi,lead_g_rc_phi) 

    #Leading gBlock eta
    a_lg_eta = append_arr(lead_g_la_eta,lead_g_ra_eta)
    b_lg_eta = append_arr(lead_g_lb_eta,lead_g_rb_eta)
    c_lg_eta = append_arr(lead_g_lc_eta,lead_g_rc_eta) 
 

    #Subleading gBlock energy
    a_sg_en = append_arr(sub_g_la_en,sub_g_ra_en)
    b_sg_en = append_arr(sub_g_lb_en,sub_g_rb_en)
    c_sg_en = append_arr(sub_g_lc_en,sub_g_rc_en)   

    #Subleading gBlock phi  
    a_sg_phi = append_arr(sub_g_la_phi,sub_g_ra_phi)
    b_sg_phi = append_arr(sub_g_lb_phi,sub_g_rb_phi)
    c_sg_phi = append_arr(sub_g_lc_phi,sub_g_rc_phi) 

    #Subleading gBlock eta
    a_sg_eta = append_arr(sub_g_la_eta,sub_g_ra_eta)
    b_sg_eta = append_arr(sub_g_lb_eta,sub_g_rb_eta)
    c_sg_eta = append_arr(sub_g_lc_eta,sub_g_rc_eta) 


    all_jet_en = a_jet_en + b_jet_en + c_jet_en
    all_jet_phi = a_jet_phi + b_jet_phi + c_jet_phi
    all_jet_eta = a_jet_eta + b_jet_eta + c_jet_eta

    all_lg_en = a_lg_en + b_lg_en + c_lg_en
    all_lg_phi = a_lg_phi + b_lg_phi + c_lg_phi
    all_lg_eta = a_lg_eta + b_lg_eta + c_lg_eta

    all_sg_en = a_sg_en + b_sg_en + c_sg_en
    all_sg_phi = a_sg_phi + b_sg_phi + c_sg_phi
    all_sg_eta = a_sg_eta + b_sg_eta + c_sg_eta

    etaphi_map(all_jet_en, all_jet_phi, all_jet_eta, 0, 1, 0, larger_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_jet_en, all_jet_phi, all_jet_eta, 10,  1, 0, larger_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_jet_en, all_jet_phi, all_jet_eta, 100,  1, 0, larger_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_jet_en, all_jet_phi, all_jet_eta, 1000,  1, 0, larger_str, hitmap_str, RUN_num, 'ABC')

    etaphi_map(all_lg_en, all_lg_phi, all_lg_eta, 0,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_lg_phi, all_lg_eta, 10,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_lg_phi, all_lg_eta, 100,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_lg_phi, all_lg_eta, 1000,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'ABC')

    etaphi_map(all_lg_en, all_sg_phi, all_sg_eta, 0,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_sg_phi, all_sg_eta, 10,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_sg_phi, all_sg_eta, 100,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'ABC')
    etaphi_map(all_lg_en, all_sg_phi, all_sg_eta, 1000,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'ABC')

    ab_jet_en = a_jet_en + b_jet_en 
    ab_jet_phi = a_jet_phi + b_jet_phi 
    ab_jet_eta = a_jet_eta + b_jet_eta 

    ab_lg_en = a_lg_en + b_lg_en 
    ab_lg_phi = a_lg_phi + b_lg_phi 
    ab_lg_eta = a_lg_eta + b_lg_eta 

    ab_sg_en = a_sg_en + b_sg_en 
    ab_sg_phi = a_sg_phi + b_sg_phi 
    ab_sg_eta = a_sg_eta + b_sg_eta

    etaphi_map(ab_jet_en, ab_jet_phi, ab_jet_eta, 0,  1, 0, larger_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_jet_en, ab_jet_phi, ab_jet_eta, 10,  1, 0, larger_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_jet_en, ab_jet_phi, ab_jet_eta, 100,  1, 0, larger_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_jet_en, ab_jet_phi, ab_jet_eta, 1000,  1, 0, larger_str, hitmap_str, RUN_num, 'AB')

    etaphi_map(ab_lg_en, ab_lg_phi, ab_lg_eta, 0,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_lg_phi, ab_lg_eta, 10,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_lg_phi, ab_lg_eta, 100,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_lg_phi, ab_lg_eta, 1000,  1, 0, lsmallr_str, hitmap_str, RUN_num, 'AB')

    etaphi_map(ab_lg_en, ab_sg_phi, ab_sg_eta, 0,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_sg_phi, ab_sg_eta, 10,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_sg_phi, ab_sg_eta, 100,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'AB')
    etaphi_map(ab_lg_en, ab_sg_phi, ab_sg_eta, 1000,  1, 0, ssmallr_str, hitmap_str, RUN_num, 'AB')

    return


#plot energy differences vs BCID to have some comparison for the TBP vs BCID plots, ommiting FPGA C
def en_vs_bcid(atob_hw, atob_sim, btob_hw, btob_sim, RUN_num):

    diffvbcid_str = "ComparisonOutputs/DiffvBCID/"

    ahw_lines = get_lines(atob_hw)
    asim_lines = get_lines(atob_sim)
    bhw_lines = get_lines(btob_hw)
    bsim_lines = get_lines(btob_sim)

    #in case this is needed
    ahw_l1id,ahw_lead_g_l,ahw_sub_g_l,ahw_lead_j_l,ahw_trlr_l,ahw_lead_g_r,ahw_sub_g_r,ahw_lead_j_r,ahw_trlr_r = parse_jtobs(ahw_lines)
    bhw_l1id,bhw_lead_g_l,bhw_sub_g_l,bhw_lead_j_l,bhw_trlr_l,bhw_lead_g_r,bhw_sub_g_r,bhw_lead_j_r,bhw_trlr_r = parse_jtobs(bhw_lines)
    asim_l1id,asim_lead_g_l,asim_sub_g_l,asim_lead_j_l,asim_trlr_l,asim_lead_g_r,asim_sub_g_r,asim_lead_j_r,asim_trlr_r = parse_jtobs(asim_lines)
    bsim_l1id,bsim_lead_g_l,bsim_sub_g_l,bsim_lead_j_l,bsim_trlr_l,bsim_lead_g_r,bsim_sub_g_r,bsim_lead_j_r,bsim_trlr_r = parse_jtobs(bsim_lines)


    ahw_lead_g_l_phi, ahw_lead_g_l_eta, ahw_lead_g_l_en, ahw_lead_g_l_satbit, ahw_sub_g_l_phi, ahw_sub_g_l_eta, ahw_sub_g_l_en, ahw_sub_g_l_satbit, ahw_lead_j_l_phi, ahw_lead_j_l_eta, ahw_lead_j_l_en, ahw_lead_j_l_satbit, ahw_bcid_l, ahw_lead_g_r_phi, ahw_lead_g_r_eta, ahw_lead_g_r_en, ahw_lead_g_r_satbit, ahw_sub_g_r_phi, ahw_sub_g_r_eta, ahw_sub_g_r_en, ahw_sub_g_r_satbit, ahw_lead_j_r_phi, ahw_lead_j_r_eta, ahw_lead_j_r_en, ahw_lead_j_r_satbit, ahw_bcid_r = jTOB_full_parse(ahw_lines, 10)

    asim_lead_g_l_phi, asim_lead_g_l_eta, asim_lead_g_l_en, asim_lead_g_l_satbit, asim_sub_g_l_phi, asim_sub_g_l_eta, asim_sub_g_l_en, asim_sub_g_l_satbit, asim_lead_j_l_phi, asim_lead_j_l_eta, asim_lead_j_l_en, asim_lead_j_l_satbit, asim_bcid_l, asim_lead_g_r_phi, asim_lead_g_r_eta, asim_lead_g_r_en, asim_lead_g_r_satbit, asim_sub_g_r_phi, asim_sub_g_r_eta, asim_sub_g_r_en, asim_sub_g_r_satbit, asim_lead_j_r_phi, asim_lead_j_r_eta, asim_lead_j_r_en, asim_lead_j_r_satbit, asim_bcid_r = jTOB_full_parse(asim_lines, 10)

    bhw_lead_g_l_phi, bhw_lead_g_l_eta, bhw_lead_g_l_en, bhw_lead_g_l_satbit, bhw_sub_g_l_phi, bhw_sub_g_l_eta, bhw_sub_g_l_en, bhw_sub_g_l_satbit, bhw_lead_j_l_phi, bhw_lead_j_l_eta, bhw_lead_j_l_en, bhw_lead_j_l_satbit, bhw_bcid_l, bhw_lead_g_r_phi, bhw_lead_g_r_eta, bhw_lead_g_r_en, bhw_lead_g_r_satbit, bhw_sub_g_r_phi, bhw_sub_g_r_eta, bhw_sub_g_r_en, bhw_sub_g_r_satbit, bhw_lead_j_r_phi, bhw_lead_j_r_eta, bhw_lead_j_r_en, bhw_lead_j_r_satbit, bhw_bcid_r = jTOB_full_parse(bhw_lines, 10)

    bsim_lead_g_l_phi, bsim_lead_g_l_eta, bsim_lead_g_l_en, bsim_lead_g_l_satbit, bsim_sub_g_l_phi, bsim_sub_g_l_eta, bsim_sub_g_l_en, bsim_sub_g_l_satbit, bsim_lead_j_l_phi, bsim_lead_j_l_eta, bsim_lead_j_l_en, bsim_lead_j_l_satbit, bsim_bcid_l, bsim_lead_g_r_phi, bsim_lead_g_r_eta, bsim_lead_g_r_en, bsim_lead_g_r_satbit, bsim_sub_g_r_phi, bsim_sub_g_r_eta, bsim_sub_g_r_en, bsim_sub_g_r_satbit, bsim_lead_j_r_phi, bsim_lead_j_r_eta, bsim_lead_j_r_en, bsim_lead_j_r_satbit, bsim_bcid_r = jTOB_full_parse(bsim_lines, 10)
    
    #FIX ME!!! + is wrong syntax, use np.append(a,b)
    #create combined arrays for simulation and hardware
    fullhw_lead_g_l_phi = ahw_lead_g_l_phi + bhw_lead_g_l_phi
    fullhw_lead_g_l_eta = ahw_lead_g_l_eta + bhw_lead_g_l_eta
    fullhw_lead_g_l_en = ahw_lead_g_l_en + bhw_lead_g_l_en

    fullhw_sub_g_l_phi = ahw_sub_g_l_phi + bhw_sub_g_l_phi
    fullhw_sub_g_l_eta = ahw_sub_g_l_eta + bhw_sub_g_l_eta
    fullhw_sub_g_l_en = ahw_sub_g_l_en + bhw_sub_g_l_en

    fullhw_lead_j_l_phi = ahw_lead_j_l_phi + bhw_lead_j_l_phi
    fullhw_lead_j_l_eta = ahw_lead_j_l_eta + bhw_lead_j_l_eta
    fullhw_lead_j_l_en = ahw_lead_j_l_en + bhw_lead_j_l_en

    fullhw_bcid_l = ahw_bcid_l + bhw_bcid_l

    fullhw_lead_g_r_phi = ahw_lead_g_r_phi + bhw_lead_g_r_phi
    fullhw_lead_g_r_eta = ahw_lead_g_r_eta + bhw_lead_g_r_eta
    fullhw_lead_g_r_en = ahw_lead_g_r_en + bhw_lead_g_r_en

    fullhw_sub_g_r_phi = ahw_sub_g_r_phi + bhw_sub_g_r_phi
    fullhw_sub_g_r_eta = ahw_sub_g_r_eta + bhw_sub_g_r_eta
    fullhw_sub_g_r_en = ahw_sub_g_r_en + bhw_sub_g_r_en

    fullhw_lead_j_r_phi = ahw_lead_j_r_phi + bhw_lead_j_r_phi
    fullhw_lead_j_r_eta = ahw_lead_j_r_eta + bhw_lead_j_r_eta
    fullhw_lead_j_r_en = ahw_lead_j_r_en + bhw_lead_j_r_en

    fullhw_bcid_r = ahw_bcid_r + bhw_bcid_r

    fullsim_lead_g_l_phi = asim_lead_g_l_phi + bsim_lead_g_l_phi
    fullsim_lead_g_l_eta = asim_lead_g_l_eta + bsim_lead_g_l_eta
    fullsim_lead_g_l_en = asim_lead_g_l_en + bsim_lead_g_l_en

    fullsim_sub_g_l_phi = asim_sub_g_l_phi + bsim_sub_g_l_phi
    fullsim_sub_g_l_eta = asim_sub_g_l_eta + bsim_sub_g_l_eta
    fullsim_sub_g_l_en = asim_sub_g_l_en + bsim_sub_g_l_en

    fullsim_lead_j_l_phi = asim_lead_j_l_phi + bsim_lead_j_l_phi
    fullsim_lead_j_l_eta = asim_lead_j_l_eta + bsim_lead_j_l_eta
    fullsim_lead_j_l_en = asim_lead_j_l_en + bsim_lead_j_l_en

    fullsim_bcid_l = asim_bcid_l + bsim_bcid_l

    fullsim_lead_g_r_phi = asim_lead_g_r_phi + bsim_lead_g_r_phi
    fullsim_lead_g_r_eta = asim_lead_g_r_eta + bsim_lead_g_r_eta
    fullsim_lead_g_r_en = asim_lead_g_r_en + bsim_lead_g_r_en

    fullsim_sub_g_r_phi = asim_sub_g_r_phi + bsim_sub_g_r_phi
    fullsim_sub_g_r_eta = asim_sub_g_r_eta + bsim_sub_g_r_eta
    fullsim_sub_g_r_en = asim_sub_g_r_en + bsim_sub_g_r_en

    fullsim_lead_j_r_phi = asim_lead_j_r_phi + bsim_lead_j_r_phi
    fullsim_lead_j_r_eta = asim_lead_j_r_eta + bsim_lead_j_r_eta
    fullsim_lead_j_r_en = asim_lead_j_r_en + bsim_lead_j_r_en

    fullsim_bcid_r = asim_bcid_r + bsim_bcid_r


    #CHANGE THIS TO BE CONSISTENT WITH OTHER DIFF PLOTS
    lead_g_en_diff = array('d')
    sub_g_en_diff = array('d')
    lead_j_en_diff = array('d')
    bcid = array('d')

    for i in range(len(fullhw_lead_j_l_en)):
        lead_g_en_diff.append(fullhw_lead_g_l_en[i] - fullsim_lead_g_l_en[i])
        lead_g_en_diff.append(fullhw_lead_g_r_en[i] - fullsim_lead_g_r_en[i])

        sub_g_en_diff.append(fullhw_sub_g_l_en[i] - fullsim_sub_g_l_en[i])
        sub_g_en_diff.append(fullhw_sub_g_r_en[i] - fullsim_sub_g_r_en[i])

        lead_j_en_diff.append(fullhw_lead_j_l_en[i] - fullsim_lead_j_l_en[i])
        lead_j_en_diff.append(fullhw_lead_j_r_en[i] - fullsim_lead_j_r_en[i])

        bcid.append(fullhw_bcid_l[i])
        bcid.append(fullhw_bcid_r[i])

    #profile_plot(x, y, xstr, ystr, plotstr, RUN_num, FPGA_str)
    profile_plot(bcid, lead_g_en_diff, "BCID", f"{et_str} TOB - {et_str} Sim", f"{lsmallr_str}_{et_str}_DiffvBCID", diffvbcid_str, RUN_num, "AB")
    profile_plot(bcid, sub_g_en_diff, "BCID", f"{et_str} TOB - {et_str} Sim", f"{ssmallr_str}_{et_str}_DiffvBCID", diffvbcid_str, RUN_num, "AB")
    profile_plot(bcid, lead_j_en_diff, "BCID", f"{et_str} TOB - {et_str} Sim", f"{larger_str}_{et_str}_DiffvBCID", diffvbcid_str, RUN_num, "AB")

    return
 

def make_delangle_dist(dumptob, simtob, RUN_num, FPGA_num):

    FPGA_str = get_fpgastr(FPGA_num)

    dump_lines = get_lines(dumptob)
    sim_lines = get_lines(simtob)

    l1id_dump,lead_g_ldump,sub_g_ldump,lead_j_ldump,bc_ldump,lead_g_rdump,sub_g_rdump,lead_j_rdump,bc_rdump = parse_jtobs(dump_lines)
    l1id_sim,lead_g_lsim,sub_g_lsim,lead_j_lsim,bc_lsim,lead_g_rsim,sub_g_rsim,lead_j_rsim,bc_rsim = parse_jtobs(sim_lines)    

    lead_g_ldump_phi, lead_g_ldump_eta, lead_g_ldump_en, lead_g_ldump_satbit, sub_g_ldump_phi, sub_g_ldump_eta, sub_g_ldump_en, sub_g_ldump_satbit, lead_j_ldump_phi, lead_j_ldump_eta, lead_j_ldump_en, lead_g_ldump_satbit, bcid_ldump, lead_g_rdump_phi, lead_g_rdump_eta, lead_g_rdump_en, lead_g_rdump_satbit, sub_g_rdump_phi, sub_g_rdump_eta, sub_g_rdump_en, sub_g_rdump_satbit, lead_j_rdump_phi, lead_j_rdump_eta, lead_j_rdump_en, lead_j_rdump_satbit, bcid_rdump = jTOB_full_parse(dump_lines, 10)
    lead_g_lsim_phi, lead_g_lsim_eta, lead_g_lsim_en, lead_g_lsim_satbit, sub_g_lsim_phi, sub_g_lsim_eta, sub_g_lsim_en, sub_g_lsim_satbit, lead_j_lsim_phi, lead_j_lsim_eta, lead_j_lsim_en, lead_j_lsim_satbit, bcid_lsim, lead_g_rsim_phi, lead_g_rsim_eta, lead_g_rsim_en, lead_g_rsim_satbit, sub_g_rsim_phi, sub_g_rsim_eta, sub_g_rsim_en, sub_g_rsim_satbit, lead_j_rsim_phi, lead_j_rsim_eta, lead_j_rsim_en, lead_j_rsim_satbit, bcid_rsim = jTOB_full_parse(sim_lines, 10)

    #Jet energy
    dump_jet_en = append_arr(lead_j_ldump_en,lead_j_rdump_en)
    sim_jet_en = append_arr(lead_j_lsim_en,lead_j_rsim_en)   

    #Jet phi  
    dump_jet_phi = append_arr(lead_j_ldump_phi,lead_j_rdump_phi)
    sim_jet_phi = append_arr(lead_j_lsim_phi,lead_j_rsim_phi) 

    #Jet eta
    dump_jet_eta = append_arr(lead_j_ldump_eta,lead_j_rdump_eta)
    sim_jet_eta = append_arr(lead_j_lsim_eta,lead_j_rsim_eta)

    #Leading gBlock energy
    dump_lg_en = append_arr(lead_g_ldump_en,lead_g_rdump_en)
    sim_lg_en = append_arr(lead_g_lsim_en,lead_g_rsim_en) 

    #Leading gBlock phi  
    dump_lg_phi = append_arr(lead_g_ldump_phi,lead_g_rdump_phi)
    sim_lg_phi = append_arr(lead_g_lsim_phi,lead_g_rsim_phi)
 
    #Leading gBlock eta
    dump_lg_eta = append_arr(lead_g_ldump_eta,lead_g_rdump_eta)
    sim_lg_eta = append_arr(lead_g_lsim_eta,lead_g_rsim_eta)

    #Subleading gBlock energy
    dump_sg_en = append_arr(sub_g_ldump_en,sub_g_rdump_en)
    sim_sg_en = append_arr(sub_g_lsim_en,sub_g_rsim_en)
   
    #Subleading gBlock phi  
    dump_sg_phi = append_arr(sub_g_ldump_phi,sub_g_rdump_phi)
    sim_sg_phi = append_arr(sub_g_lsim_phi,sub_g_rsim_phi)

    #Subleading gBlock eta
    dump_sg_eta = append_arr(sub_g_ldump_eta,sub_g_rdump_eta)
    sim_sg_eta = append_arr(sub_g_lsim_eta,sub_g_rsim_eta)

    #find the difference between eta and phi for the leading and subleading plots
    dump_delphi = array('i')
    dump_deleta = array('i')
    sim_delphi = array('i')
    sim_deleta = array('i')

    for i in range(len(dump_jet_en)):
        dump_delphi.append(dump_lg_phi[i] - dump_sg_phi[i])
        dump_deleta.append(dump_lg_eta[i] - dump_sg_eta[i])

        sim_delphi.append(sim_lg_phi[i] - sim_sg_phi[i])
        sim_deleta.append(sim_lg_eta[i] - sim_sg_eta[i])


    lsj = "Leading Small Jet"
    ssj = "Subleading Small Jet"
    phi = "Phi"
    eta = "Eta"

    folder = "DeltaAngleDist/"


    hist_1d(dump_delphi, f"{lsj} {phi} - {ssj} {phi}", f"Readout_Delta_{phi}_Distribution", float(max(dump_delphi)), folder, RUN_num, FPGA_str)
    hist_1d(dump_deleta, f"{lsj} {eta} - {ssj} {eta}", f"Readout_Delta_{eta}_Distribution", float(max(dump_deleta)), folder, RUN_num, FPGA_str)

    hist_1d(sim_delphi, f"{lsj} {phi} - {ssj} {phi}", f"Sim_Delta_{phi}_Distribution", float(max(sim_delphi)), folder, RUN_num, FPGA_str)
    hist_1d(sim_deleta, f"{lsj} {eta} - {ssj} {eta}", f"Sim_Delta_{eta}_Distribution", float(max(sim_deleta)), folder, RUN_num, FPGA_str)

    return


def hist_1d_temp(arr, xstr, plotstr, b, folder, RUN_num, FPGA_str):
    c = TCanvas('c', 'Histograms', 200, 10, 700, 500)
    c.SetRightMargin(0.18)

    nbins = int(2.0*b + 3.0)


    name = f"{folder}Run{RUN_num}_FPGA_{FPGA_str}_{plotstr}.pdf"
    # add 1 to the bound so the max is included
    hist = TH1F('hist', f'Run{RUN_num} FPGA {FPGA_str} {plotstr}; {xstr}', nbins, -(b + 1.0), b + 1.0)
    for i in range(len(arr)):
        hist.Fill(arr[i])

    hist.Draw()

    c.Print(name)

    return

#make JWOJ
def JWJ_MET_hist(g_tobs, RUN_num, FPGA_num):
    #set to one for now, just counts
    jwoj_lsb = 1

    folder = "ReadoutMETTOB_Distributions/"

    FPGA_str = get_fpgastr(FPGA_num)

    #check
    check = open(f"{folder}{RUN_num}_FPGA_{FPGA_str}_METTOB_Check.txt", "w+")

    check.write("MET TOB Words\n")
    check.write("JWJ Word          Binary                Signed Integer\n")

    g_lines = get_lines(g_tobs)

    l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr, nc_met_comp, rms_met_comp, alt_met_sums, altmet_trlr = parse_gtobs(g_lines)

    mhtx, mhty = parse_gwords(jwj_mht_comp)
    mstx, msty = parse_gwords(jwj_mst_comp)
    metx, mety = parse_gwords(jwj_met_comp)

    for i in range(len(mhtx)):
        check.write(f"{l1id[i]} MHTX   {mhtx[i]}      {bin_to_signed_int(mhtx[i])}\n")
        check.write(f"{l1id[i]} MHTY   {mhty[i]}      {bin_to_signed_int(mhty[i])}\n")
        check.write(f"{l1id[i]} MSTX   {mstx[i]}      {bin_to_signed_int(mstx[i])}\n")
        check.write(f"{l1id[i]} MSTY   {msty[i]}      {bin_to_signed_int(msty[i])}\n")
        check.write(f"{l1id[i]} METX   {metx[i]}      {bin_to_signed_int(metx[i])}\n")
        check.write(f"{l1id[i]} METY   {mety[i]}      {bin_to_signed_int(mety[i])}\n")
        check.write(f"-------------------------------------------------------------\n")

    check.close()


    mhtx = [bin_to_signed_int(word)  for word in mhtx]
    mhty = [bin_to_signed_int(word)  for word in mhty]
    mstx = [bin_to_signed_int(word)  for word in mstx]
    msty = [bin_to_signed_int(word)  for word in msty]
    metx = [bin_to_signed_int(word)  for word in metx]
    mety = [bin_to_signed_int(word)  for word in mety]

    hist_1d(mhtx, "Readout Missing Hard Term X [Counts]", "mhtx_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(mhty, "Readout Missing Hard Term Y [Counts]", "mhty_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(mstx, "Readout Missing Soft Term X [Counts]", "mstx_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(msty, "Readout Missing Soft Term Y [Counts]", "msty_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(metx, "Readout Missing Energy Term X [Counts]", "metx_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(mety, "Readout Missing Energy Term Y [Counts]", "mety_dist_readout", 32800.0, folder, RUN_num,
            FPGA_str)

    metx_nonzero = array('d')
    for i in range(len(metx)):
        if metx[i] != 0.0:
            metx_nonzero.append(metx[i])
            #print(metx[i])
    hist_1d_temp(metx_nonzero, "Readout Missing Energy Term X [Counts]", "mhtx_dist_nonzer0_readout_rebin", 700.0, folder, RUN_num,
            FPGA_str)

    #temp table to plot 1 ADC Count per bin
    hist_1d_temp(mhtx, "Readout Missing Hard Term X [Counts]", "mhtx_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)
    hist_1d_temp(mhty, "Readout Missing Hard Term Y [Counts]", "mhty_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)
    hist_1d_temp(mstx, "Readout Missing Soft Term X [Counts]", "mstx_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)
    hist_1d_temp(msty, "Readout Missing Soft Term Y [Counts]", "msty_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)
    hist_1d_temp(metx, "Readout Missing Energy Term X [Counts]", "metx_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)
    hist_1d_temp(mety, "Readout Missing Energy Term Y [Counts]", "mety_dist_readout_rebin", 70.0, folder, RUN_num,
            FPGA_str)

    #JWJ values
    a = 1003.0
    b = 409.0

    metx_calc = array('d')
    mety_calc = array('d')

    for i in range(len(mhtx)):
        metx_calc.append(float(a * mhtx[i] + b * mstx[i]) / 1024.0)
        mety_calc.append(float(a * mhty[i] + b * msty[i]) / 1024.0)

    hist_1d(metx_calc, "Missing Energy Term X [Counts]", "metx_dist_calc", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(mety_calc, "Missing Energy Term Y [Counts]", "mety_dist_calc", 32800.0, folder, RUN_num,
            FPGA_str)

    '''
    met_from_met = array('d')
    met_from_mhtmst = array('d')
    for i in range(len(metx)):
        met_from_met.append(np.sqrt((float(metx[i]))**2+(float(mety[i]))**2))
        met_from_mhtmst.append(np.sqrt((float(metx_calc[i]))**2+(float(mety_calc[i]))**2))

    print(met_from_mhtmst)
    hist_1d(met_from_met, "MET Caluclated from METx METy [Counts]", "met_from_met", 32800.0, folder, RUN_num,
            FPGA_str)
    hist_1d(met_from_mhtmst, "MET Caluclated from MHT and MST [Counts]", "met_from_mhtmst", 32800.0, folder, RUN_num,
            FPGA_str)
    '''

    return

def make_met_comp(readout, sim, RUN_num, FPGA_num):
    #set to one for now, just counts
    jwoj_lsb = 1.0

    FPGA_str = get_fpgastr(FPGA_num)

    ro_lines = get_lines(readout)
    sim_lines = get_lines(sim)

    ro_l1id, ro_jwj_mht_comp, ro_jwj_met_comp, ro_jwj_mst_comp, ro_jwj_trlr, ro_nc_met_comp, ro_rms_met_comp, ro_alt_met_sums, ro_altmet_trlr = parse_gtobs(ro_lines)

    ro_mhtx, ro_mhty = parse_gwords(ro_jwj_mht_comp)
    ro_mstx, ro_msty = parse_gwords(ro_jwj_mst_comp)
    ro_metx, ro_mety = parse_gwords(ro_jwj_met_comp)


    ro_mhtx = [bin_to_signed_int(word) * jwoj_lsb for word in ro_mhtx]
    ro_mhty = [bin_to_signed_int(word) * jwoj_lsb for word in ro_mhty]
    ro_mstx = [bin_to_signed_int(word) * jwoj_lsb for word in ro_mstx]
    ro_msty = [bin_to_signed_int(word) * jwoj_lsb for word in ro_msty]
    ro_metx = [bin_to_signed_int(word) * jwoj_lsb for word in ro_metx]
    ro_mety = [bin_to_signed_int(word) * jwoj_lsb for word in ro_mety]

    ro_mhtx = array('d',ro_mhtx)
    ro_mhty = array('d',ro_mhty)
    ro_mstx = array('d',ro_mstx)
    ro_msty = array('d',ro_msty)
    ro_metx = array('d',ro_metx)
    ro_mety = array('d',ro_mety)

    # working on sim part
    sim_l1id, sim_jwj_mht_comp, sim_jwj_met_comp, sim_jwj_mst_comp, sim_jwj_trlr = parse_sim_gtobs(sim_lines)

    sim_mhtx, sim_mhty = parse_gwords(sim_jwj_mht_comp)
    sim_mstx, sim_msty = parse_gwords(sim_jwj_mst_comp)
    sim_metx, sim_mety = parse_gwords(sim_jwj_met_comp)

    sim_mhtx = [bin_to_signed_int(word) * jwoj_lsb for word in sim_mhtx]
    sim_mhty = [bin_to_signed_int(word) * jwoj_lsb for word in sim_mhty]
    sim_mstx = [bin_to_signed_int(word) * jwoj_lsb for word in sim_mstx]
    sim_msty = [bin_to_signed_int(word) * jwoj_lsb for word in sim_msty]
    sim_metx = [bin_to_signed_int(word) * jwoj_lsb for word in sim_metx]
    sim_mety = [bin_to_signed_int(word) * jwoj_lsb for word in sim_mety]

    sim_mhtx = array('d',sim_mhtx)
    sim_mhty = array('d',sim_mhty)
    sim_mstx = array('d',sim_mstx)
    sim_msty = array('d',sim_msty)
    sim_metx = array('d',sim_metx)
    sim_mety = array('d',sim_mety)
    #check l1ids
    for i in range(len(ro_l1id)):
        if(ro_l1id[i] != sim_l1id[i]):
            print(f"L1ID Does not match at index {i}")
            print(f"Readout L1ID: {ro_l1id[i]}")
            print(f"Simulation L1ID: {sim_l1id[i]}")

    xstr = "Value from Readout Path TOBs"
    ystr = "Value from C Sim Emulated TOBs"

    folder = "METTOB_Comparisons/"
    folder2 = "SimMETTOB_Distributions/"

    #scat_plot(meas, sim, en_or_ang, xstr, ystr, plotstr, folder, RUN_num, FPGA_str)


    scat_plot(ro_mhtx, sim_mhtx, 0, f"MHT X {xstr}", f"MHT X {ystr}", f"MHTX_Comparisons", folder, RUN_num, FPGA_str)
    scat_plot(ro_mhty, sim_mhty, 0, f"MHT Y {xstr}", f"MHT Y {ystr}", f"MHTY_Comparisons", folder, RUN_num, FPGA_str)
    scat_plot(ro_mstx, sim_mstx, 0, f"MST X {xstr}", f"MST X {ystr}", f"MSTX_Comparisons", folder, RUN_num, FPGA_str)
    scat_plot(ro_msty, sim_msty, 0, f"MST Y {xstr}", f"MST Y {ystr}", f"MSTY_Comparisons", folder, RUN_num, FPGA_str)
    scat_plot(ro_metx, sim_metx, 0, f"MET X {xstr}", f"MET X {ystr}", f"METX_Comparisons", folder, RUN_num, FPGA_str)
    scat_plot(ro_mety, sim_mety, 0, f"MET Y {xstr}", f"MET Y {ystr}", f"METY_Comparisons", folder, RUN_num, FPGA_str)

    #temp table to plot 1 ADC Count per bin
    hist_1d_temp(sim_mhtx, "Sim Missing Hard Term X [Counts]", "mhtx_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_mhty, "Sim Missing Hard Term Y [Counts]", "mhty_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_mstx, "Sim Missing Soft Term X [Counts]", "mstx_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_msty, "Sim Missing Soft Term Y [Counts]", "msty_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_metx, "Sim Missing Energy Term X [Counts]", "metx_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_mety, "Sim Missing Energy Term Y [Counts]", "mety_dist_sim_rebin", 700.0, folder2, RUN_num,
            FPGA_str)


    hist_1d(sim_mhtx, "Sim Missing Hard Term X [Counts]", "mhtx_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)
    hist_1d(sim_mhty, "Sim Missing Hard Term Y [Counts]", "mhty_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)
    hist_1d(sim_mstx, "Sim Missing Soft Term X [Counts]", "mstx_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)
    hist_1d(sim_msty, "Sim Missing Soft Term Y [Counts]", "msty_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)
    hist_1d(sim_metx, "Sim Missing Energy Term X [Counts]", "metx_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)
    hist_1d(sim_mety, "Sim Missing Energy Term Y [Counts]", "mety_dist_sim", 32800.0, folder2, RUN_num,
            FPGA_str)

    sim_metx_nonzero = array('d')
    for i in range(len(sim_metx)):
        if sim_metx[i] != 0.0:
            sim_metx_nonzero.append(sim_metx[i])
            #print(sim_metx[i])
    hist_1d_temp(sim_metx_nonzero, "Sim Missing Energy Term X [Counts]", "mhtx_dist_nonzer0_sim_rebin1", 700.0, folder2, RUN_num,
            FPGA_str)
    hist_1d_temp(sim_metx_nonzero, "Sim Missing Energy Term X [Counts]", "mhtx_dist_nonzer0_sim_rebin2", 70.0, folder2, RUN_num,
            FPGA_str)
    return

def comp_half_fpga(f1, f2, RUN_num, FPGA_num):
    #setup string based on FPGA_num
    FPGA_str = get_fpgastr(FPGA_num)


    #get lines from file
    fw = get_lines(f1)
    sim = get_lines(f2)

    #parsing out the different rows
    l1id_1,lead_g_l1,sub_g_l1,lead_j_l1,bc_l1,lead_g_r1,sub_g_r1,lead_j_r1,bc_r1 = parse_jtobs(fw)

    l1id_2,lead_g_l2,sub_g_l2,lead_j_l2,bc_l2,lead_g_r2,sub_g_r2,lead_j_r2,bc_r2 = parse_jtobs(sim)

    # if length of l1ids does not match, stop and debug
    if len(l1id_1) != len(l1id_2):
        print("Error: incorrect number of l1ids for comparison, debug and try again\n")

    # get phi, eta, energy
    lead_g_l1_phi, lead_g_l1_eta, lead_g_l1_en, lead_g_l1_satbit, sub_g_l1_phi, sub_g_l1_eta, sub_g_l1_en, sub_g_l1_satbit, lead_j_l1_phi, lead_j_l1_eta, lead_j_l1_en, lead_j_l1_satbit, bcid_l1, lead_g_r1_phi, lead_g_r1_eta, lead_g_r1_en, lead_g_r1_satbit, sub_g_r1_phi, sub_g_r1_eta, sub_g_r1_en, sub_g_r1_satbit, lead_j_r1_phi, lead_j_r1_eta, lead_j_r1_en, lead_j_r1_satbit, bcid_r1 = jTOB_full_parse(
        fw, 10)

    lead_g_l2_phi, lead_g_l2_eta, lead_g_l2_en, lead_g_l2_satbit, sub_g_l2_phi, sub_g_l2_eta, sub_g_l2_en, sub_g_l2_satbit, lead_j_l2_phi, lead_j_l2_eta, lead_j_l2_en, lead_j_l2_satbit, bcid_l2, lead_g_r2_phi, lead_g_r2_eta, lead_g_r2_en, lead_g_r2_satbit, sub_g_r2_phi, sub_g_r2_eta, sub_g_r2_en, sub_g_r2_satbit, lead_j_r2_phi, lead_j_r2_eta, lead_j_r2_en, lead_j_r2_satbit, bcid_r2 = jTOB_full_parse(
        sim, 10)

    # add the left and right halves for the energy, phi, and eta values for each of the three TOB word types (Jet, Leading gblk, Subleading gblk)

    l1id_1 = [int(s, 16) for s in l1id_1]
    l1id_2 = [int(s, 16) for s in l1id_2]


    #comp plot specific strings
    compx_str = "TOBs from gFEX Readout Path"
    compy_str = "Emulated from gFEX Input Readout"

    comp_folder_str = "ComparisonOutputs/TOBComparisonPlots/"


    left_half = FPGA_str + "1"
    right_half = FPGA_str + "2"

    fw_left_jet_en = array('d')
    sim_left_jet_en = array('d')
    fw_right_jet_en = array('d')
    sim_right_jet_en = array('d')

    fw_left_ls_jet_en = array('d')
    sim_left_ls_jet_en = array('d')
    fw_right_ls_jet_en = array('d')
    sim_right_ls_jet_en = array('d')

    fw_left_ss_jet_en = array('d')
    sim_left_ss_jet_en = array('d')
    fw_right_ss_jet_en = array('d')
    sim_right_ss_jet_en = array('d')

    for i in range(len(l1id_1)):
        fw_left_jet_en.append(lead_j_l1_en[i])
        sim_left_jet_en.append(lead_j_l2_en[i])
        fw_right_jet_en.append(lead_j_r1_en[i])
        sim_right_jet_en.append(lead_j_r2_en[i])

        fw_left_ls_jet_en.append(lead_g_l1_en[i])
        sim_left_ls_jet_en.append(lead_g_l2_en[i])
        fw_right_ls_jet_en.append(lead_g_r1_en[i])
        sim_right_ls_jet_en.append(lead_g_r2_en[i])

        fw_left_ss_jet_en.append(sub_g_l1_en[i])
        sim_left_ss_jet_en.append(sub_g_l2_en[i])
        fw_right_ss_jet_en.append(sub_g_r1_en[i])
        sim_right_ss_jet_en.append(sub_g_r2_en[i])


    #For delay testing only
    delay_val = 14
    #delay_str = f"_InterDelay{delay_val}"
    delay_str = ""


    scat_plot(fw_left_jet_en, sim_left_jet_en, 0, f"{larger_str} {et_str} {compx_str}", f"{larger_str} {et_str} {compy_str}",
              f"{larger_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, left_half)

    scat_plot(fw_right_jet_en, sim_right_jet_en, 0, f"{larger_str} {et_str} {compx_str}", f"{larger_str} {et_str} {compy_str}",
              f"{larger_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, right_half)

    scat_plot(fw_left_ls_jet_en, sim_left_ls_jet_en, 0, f"{lsmallr_str} {et_str} {compx_str}", f"{lsmallr_str} {et_str} {compy_str}",
              f"{lsmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, left_half)

    scat_plot(fw_right_ls_jet_en, sim_right_ls_jet_en, 0, f"{lsmallr_str} {et_str} {compx_str}", f"{lsmallr_str} {et_str} {compy_str}",
              f"{lsmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, right_half)

    scat_plot(fw_left_ss_jet_en, sim_left_ss_jet_en, 0, f"{ssmallr_str} {et_str} {compx_str}", f"{ssmallr_str} {et_str} {compy_str}",
              f"{ssmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, left_half)

    scat_plot(fw_right_ss_jet_en, sim_right_ss_jet_en, 0, f"{ssmallr_str} {et_str} {compx_str}", f"{ssmallr_str} {et_str} {compy_str}",
              f"{ssmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}", comp_folder_str, RUN_num, right_half)

    '''
    l_zero_count = 0
    r_zero_count = 0
    for i in range(len(l1id_1)):
        if fw_left_jet_en[i] == 0.0 and sim_left_jet_en[i] == 0.0:
            l_zero_count += 1
        if fw_right_jet_en[i] == 0.0 and sim_right_jet_en[i] == 0.0:
            r_zero_count += 1
    print(f"FPGA {left_half} has {l_zero_count} 0 energy events out of {len(l1id_1)}")
    print(f"FPGA {right_half} has {r_zero_count} 0 energy events out of {len(l1id_1)}")
    '''

    return

def fpga_en_comps(a1, b1, a2, b2, RUN_num, FPGA_num):

    return

def delay_diff_rms(fw, sim):

    return
