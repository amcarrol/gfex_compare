#module for functions to create various distribution plots of TOBs from FW and emulated from simulation
import helper_functions as hf
import plotting_functions as pf
import numpy as np
from array import *

#global strings
#define strings for plot names/axes
larger_str = "Leading_LargeR_Jet"
lsmallr_str = "Leading_SmallR_Jet"
ssmallr_str = "Subleading_SmallR_Jet"

en_str = "Energy"
eta_str = "Eta"
phi_str = "Phi"

#global variables
jet_lsb = 0.2  # GeV

#src_str should be "Readout" or "Sim" depending on what dat is being used fro distributions
def distributions(fpga_dat, src_str,  RUN_num):


    FPGA_str = fpga_dat[0].fpga_id

    l = len(fpga_dat)

    #dist plot specific strings
    xstr_base = f"{src_str} Distribution"
    plotstr_base = f"{src_str}_Distribution"

    jet_folder_str = f"TOB_Distributions/{src_str}TOBs/JetTOBs/"
    met_folder_str = f"TOB_Distributions/{src_str}TOBs/METTOBs/"


    left_half = FPGA_str + "1"
    right_half = FPGA_str + "2"

    #Energies
    left_ll_jet_en = array('d')
    right_ll_jet_en = array('d')

    left_ls_jet_en = array('d')
    right_ls_jet_en = array('d')

    left_ss_jet_en = array('d')
    right_ss_jet_en = array('d')

    #Phi
    left_ll_jet_phi = array('d')
    right_ll_jet_phi = array('d')

    left_ls_jet_phi = array('d')
    right_ls_jet_phi = array('d')

    left_ss_jet_phi = array('d')
    right_ss_jet_phi = array('d')

    #Eta
    left_ll_jet_eta = array('d')
    right_ll_jet_eta = array('d')

    left_ls_jet_eta = array('d')
    right_ls_jet_eta = array('d')

    left_ss_jet_eta = array('d')
    right_ss_jet_eta = array('d')

    #Saturation Bits
    left_ll_jet_satbit =  array('d')
    left_ls_jet_satbit = array('d')
    left_ss_jet_satbit = array('d')
    right_ll_jet_satbit =  array('d')
    right_ls_jet_satbit = array('d')
    right_ss_jet_satbit = array('d')

    #JWJ
    mhtx  = array('d')
    mhty  = array('d')
    metx  = array('d')
    mety  = array('d')
    mstx  = array('d')
    msty  = array('d')

    #JWJ sumET
    jwj_tot_sumEt = array('d')
    jwj_hard_sumEt = array('d')
    jwj_soft_sumEt = array('d')

    #Alt MET
    nc_metx  = array('d')
    nc_mety  = array('d')
    rms_metx  = array('d')
    rms_mety  = array('d')
    nc_sumEt  = array('d')
    rms_sumEt  = array('d')

    for i in range(l):
        #Energy
        left_ll_jet_en.append(fpga_dat[i].TOB1_lgLJ_et)
        right_ll_jet_en.append(fpga_dat[i].TOB2_lgLJ_et)

        left_ls_jet_en.append(fpga_dat[i].TOB1_lgJ_et)
        right_ls_jet_en.append(fpga_dat[i].TOB2_lgJ_et)

        left_ss_jet_en.append(fpga_dat[i].TOB1_sgJ_et)
        right_ss_jet_en.append(fpga_dat[i].TOB2_sgJ_et)

        #Phi
        left_ll_jet_phi.append(fpga_dat[i].TOB1_lgLJ_phi)
        right_ll_jet_phi.append(fpga_dat[i].TOB2_lgLJ_phi)

        left_ls_jet_phi.append(fpga_dat[i].TOB1_lgJ_phi)
        right_ls_jet_phi.append(fpga_dat[i].TOB2_lgJ_phi)

        left_ss_jet_phi.append(fpga_dat[i].TOB1_sgJ_phi)
        right_ss_jet_phi.append(fpga_dat[i].TOB2_sgJ_phi)

        #Eta
        left_ll_jet_eta.append(fpga_dat[i].TOB1_lgLJ_eta)
        right_ll_jet_eta.append(fpga_dat[i].TOB2_lgLJ_eta)

        left_ls_jet_eta.append(fpga_dat[i].TOB1_lgJ_eta)
        right_ls_jet_eta.append(fpga_dat[i].TOB2_lgJ_eta)

        left_ss_jet_eta.append(fpga_dat[i].TOB1_sgJ_eta)
        right_ss_jet_eta.append(fpga_dat[i].TOB2_sgJ_eta)

        #Saturation Bits
        left_ll_jet_satbit.append(fpga_dat[i].TOB1_lgLJ_satbit)
        left_ls_jet_satbit.append(fpga_dat[i].TOB1_lgJ_satbit)
        left_ss_jet_satbit.append(fpga_dat[i].TOB1_sgJ_satbit)
        right_ll_jet_satbit.append(fpga_dat[i].TOB2_lgLJ_satbit)
        right_ls_jet_satbit.append(fpga_dat[i].TOB2_lgJ_satbit)
        right_ss_jet_satbit.append(fpga_dat[i].TOB2_sgJ_satbit)


        #JWJ
        mhtx.append(fpga_dat[i].jwj_mhtx)
        mhty.append(fpga_dat[i].jwj_mhty)
        metx.append(fpga_dat[i].jwj_metx)
        mety.append(fpga_dat[i].jwj_mety)
        mstx.append(fpga_dat[i].jwj_mstx)
        msty.append(fpga_dat[i].jwj_msty)

        #JWJ sumET
        jwj_tot_sumEt.append(fpga_dat[i].jwj_tot_sumEt)
        jwj_hard_sumEt.append(fpga_dat[i].jwj_hard_sumEt)
        jwj_soft_sumEt.append(fpga_dat[i].jwj_soft_sumEt)

        #Alt Met
        nc_metx.append(fpga_dat[i].nc_metx)
        nc_mety.append(fpga_dat[i].nc_mety)
        rms_metx.append(fpga_dat[i].rms_metx)
        rms_mety.append(fpga_dat[i].rms_mety)
        nc_sumEt.append(fpga_dat[i].nc_sumEt)
        rms_sumEt.append(fpga_dat[i].rms_sumEt)

    #Energy
    pf.hist_1d(left_ll_jet_en,f"{left_half} {larger_str} {en_str} {xstr_base}",
               f"{larger_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ll_jet_en,f"{right_half} {larger_str} {en_str} {xstr_base}",
               f"{larger_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ls_jet_en,f"{left_half} {lsmallr_str} {en_str} {xstr_base}",
               f"{lsmallr_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ls_jet_en,f"{right_half} {lsmallr_str} {en_str} {xstr_base}",
               f"{lsmallr_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ss_jet_en,f"{left_half} {ssmallr_str} {en_str} {xstr_base}",
               f"{ssmallr_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ss_jet_en,f"{right_half} {ssmallr_str} {en_str} {xstr_base}",
               f"{ssmallr_str}_{en_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    #Phi
    pf.hist_1d(left_ll_jet_phi, f"{left_half} {larger_str} {phi_str} {xstr_base}",
               f"{larger_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ll_jet_phi, f"{right_half} {larger_str} {phi_str} {xstr_base}",
               f"{larger_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ls_jet_phi, f"{left_half} {lsmallr_str} {phi_str} {xstr_base}",
               f"{lsmallr_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ls_jet_phi, f"{right_half} {lsmallr_str} {phi_str} {xstr_base}",
               f"{lsmallr_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ss_jet_phi, f"{left_half} {ssmallr_str} {phi_str} {xstr_base}",
               f"{ssmallr_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ss_jet_phi, f"{right_half} {ssmallr_str} {phi_str} {xstr_base}",
               f"{ssmallr_str}_{phi_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    #Eta
    pf.hist_1d(left_ll_jet_eta, f"{left_half} {larger_str} {eta_str} {xstr_base}",
               f"{larger_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ll_jet_eta, f"{right_half} {larger_str} {eta_str} {xstr_base}",
               f"{larger_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ls_jet_eta, f"{left_half} {lsmallr_str} {eta_str} {xstr_base}",
               f"{lsmallr_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ls_jet_eta, f"{right_half} {lsmallr_str} {eta_str} {xstr_base}",
               f"{lsmallr_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    pf.hist_1d(left_ss_jet_eta, f"{left_half} {ssmallr_str} {eta_str} {xstr_base}",
               f"{ssmallr_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ss_jet_eta, f"{right_half} {ssmallr_str} {eta_str} {xstr_base}",
               f"{ssmallr_str}_{eta_str}_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    #Saturation Bits
    pf.hist_1d(left_ll_jet_satbit, f"{left_half} {larger_str} Saturation Bit {xstr_base}",
               f"{larger_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(left_ls_jet_satbit, f"{left_half} {lsmallr_str} Saturation Bit {xstr_base}",
               f"{lsmallr_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(left_ss_jet_satbit, f"{left_half} {ssmallr_str} Saturation Bit {xstr_base}",
               f"{ssmallr_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, left_half)
    pf.hist_1d(right_ll_jet_satbit, f"{right_half} {larger_str} Saturation Bit {xstr_base}",
               f"{larger_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, right_half)
    pf.hist_1d(right_ls_jet_satbit, f"{right_half} {lsmallr_str} Saturation Bit {xstr_base}",
               f"{lsmallr_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, right_half)
    pf.hist_1d(right_ss_jet_satbit, f"{right_half} {ssmallr_str} Saturation Bit {xstr_base}",
               f"{ssmallr_str}_SatBit_{plotstr_base}", jet_folder_str, RUN_num, right_half)

    #JWJ
    pf.hist_1d(mhtx, f"{FPGA_str} JWJ MHTX {xstr_base}", f"JWJ_MHTX_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(mhty, f"{FPGA_str} JWJ MHTY {xstr_base}", f"JWJ_MHTY_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(mstx, f"{FPGA_str} JWJ MSTX {xstr_base}", f"JWJ_MSTX_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(msty, f"{FPGA_str} JWJ MSTY {xstr_base}", f"JWJ_MSTY_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(metx, f"{FPGA_str} JWJ METX {xstr_base}", f"JWJ_METX_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(mety, f"{FPGA_str} JWJ METY {xstr_base}", f"JWJ_METY_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)

    #JWJ sumEt
    pf.hist_1d(jwj_tot_sumEt, f"{FPGA_str} JWJ Total sumET {xstr_base}", f"JWJ_SUMET_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(jwj_hard_sumEt, f"{FPGA_str} JWJ Hard sumET {xstr_base}", f"JWJ_SUMHT_{plotstr_base}", met_folder_str, RUN_num,
               FPGA_str)
    pf.hist_1d(jwj_soft_sumEt, f"{FPGA_str} JWJ Soft sumET {xstr_base}", f"JWJ_SUMST_{plotstr_base}", met_folder_str, RUN_num,
               FPGA_str)

    #Alt MET
    pf.hist_1d(nc_metx, f"{FPGA_str} NC METX {xstr_base}", f"NC_METX_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(nc_mety, f"{FPGA_str} NC METY {xstr_base}", f"NC_METY_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(rms_metx, f"{FPGA_str} RMS METX {xstr_base}", f"RMS_METX_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(rms_mety, f"{FPGA_str} RMS METY {xstr_base}", f"RMS_METY_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(nc_sumEt, f"{FPGA_str} NC SUMET {xstr_base}", f"NC_SUMET_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    pf.hist_1d(rms_sumEt, f"{FPGA_str} RMS SUMET {xstr_base}", f"RMS_SUMET_{plotstr_base}", met_folder_str, RUN_num, FPGA_str)
    
    return


def fpga_comp_hist(sim_dat, fw_dat, RUN_num):

    # DQ checks for variable assignments

    # FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    # Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")


    jet_comp_folder_str = "Comparison_Histograms/JetTOBs/"
    met_comp_folder_str = "Comparison_Histograms/METTOBs/"

    left_half = FPGA_str + "1"
    right_half = FPGA_str + "2"

    # Energies
    fw_left_ll_jet_en = array('d')
    sim_left_ll_jet_en = array('d')
    fw_right_ll_jet_en = array('d')
    sim_right_ll_jet_en = array('d')

    fw_left_ls_jet_en = array('d')
    sim_left_ls_jet_en = array('d')
    fw_right_ls_jet_en = array('d')
    sim_right_ls_jet_en = array('d')

    fw_left_ss_jet_en = array('d')
    sim_left_ss_jet_en = array('d')
    fw_right_ss_jet_en = array('d')
    sim_right_ss_jet_en = array('d')

    # Phi
    fw_left_ll_jet_phi = array('d')
    sim_left_ll_jet_phi = array('d')
    fw_right_ll_jet_phi = array('d')
    sim_right_ll_jet_phi = array('d')

    fw_left_ls_jet_phi = array('d')
    sim_left_ls_jet_phi = array('d')
    fw_right_ls_jet_phi = array('d')
    sim_right_ls_jet_phi = array('d')

    fw_left_ss_jet_phi = array('d')
    sim_left_ss_jet_phi = array('d')
    fw_right_ss_jet_phi = array('d')
    sim_right_ss_jet_phi = array('d')

    # Eta
    fw_left_ll_jet_eta = array('d')
    sim_left_ll_jet_eta = array('d')
    fw_right_ll_jet_eta = array('d')
    sim_right_ll_jet_eta = array('d')

    fw_left_ls_jet_eta = array('d')
    sim_left_ls_jet_eta = array('d')
    fw_right_ls_jet_eta = array('d')
    sim_right_ls_jet_eta = array('d')

    fw_left_ss_jet_eta = array('d')
    sim_left_ss_jet_eta = array('d')
    fw_right_ss_jet_eta = array('d')
    sim_right_ss_jet_eta = array('d')

    #PUC
    fw_puc = array('d')
    sim_puc = array('d')

    # JWJ
    fw_mhtx = array('d')
    sim_mhtx = array('d')
    fw_mhty = array('d')
    sim_mhty = array('d')
    fw_metx = array('d')
    sim_metx = array('d')
    fw_mety = array('d')
    sim_mety = array('d')
    fw_mstx = array('d')
    sim_mstx = array('d')
    fw_msty = array('d')
    sim_msty = array('d')

    # JWJ sumET
    fw_jwj_tot_sumEt = array('d')
    sim_jwj_tot_sumEt = array('d')
    fw_jwj_hard_sumEt = array('d')
    sim_jwj_hard_sumEt = array('d')
    fw_jwj_soft_sumEt = array('d')
    sim_jwj_soft_sumEt = array('d')

    # Alt MET
    fw_nc_metx = array('d')
    sim_nc_metx = array('d')
    fw_nc_mety = array('d')
    sim_nc_mety = array('d')
    fw_rms_metx = array('d')
    sim_rms_metx = array('d')
    fw_rms_mety = array('d')
    sim_rms_mety = array('d')
    fw_nc_sumEt = array('d')
    sim_nc_sumEt = array('d')
    fw_rms_sumEt = array('d')
    sim_rms_sumEt = array('d')


    for i in range(l):
        # Energy
        fw_left_ll_jet_en.append(fw_dat[i].TOB1_lgLJ_et)
        sim_left_ll_jet_en.append(sim_dat[i].TOB1_lgLJ_et)
        fw_right_ll_jet_en.append(fw_dat[i].TOB2_lgLJ_et)
        sim_right_ll_jet_en.append(sim_dat[i].TOB2_lgLJ_et)

        fw_left_ls_jet_en.append(fw_dat[i].TOB1_lgJ_et)
        sim_left_ls_jet_en.append(sim_dat[i].TOB1_lgJ_et)
        fw_right_ls_jet_en.append(fw_dat[i].TOB2_lgJ_et)
        sim_right_ls_jet_en.append(sim_dat[i].TOB2_lgJ_et)

        fw_left_ss_jet_en.append(fw_dat[i].TOB1_sgJ_et)
        sim_left_ss_jet_en.append(sim_dat[i].TOB1_sgJ_et)
        fw_right_ss_jet_en.append(fw_dat[i].TOB2_sgJ_et)
        sim_right_ss_jet_en.append(sim_dat[i].TOB2_sgJ_et)

        # Phi
        fw_left_ll_jet_phi.append(fw_dat[i].TOB1_lgLJ_phi)
        sim_left_ll_jet_phi.append(sim_dat[i].TOB1_lgLJ_phi)
        fw_right_ll_jet_phi.append(fw_dat[i].TOB2_lgLJ_phi)
        sim_right_ll_jet_phi.append(sim_dat[i].TOB2_lgLJ_phi)

        fw_left_ls_jet_phi.append(fw_dat[i].TOB1_lgJ_phi)
        sim_left_ls_jet_phi.append(sim_dat[i].TOB1_lgJ_phi)
        fw_right_ls_jet_phi.append(fw_dat[i].TOB2_lgJ_phi)
        sim_right_ls_jet_phi.append(sim_dat[i].TOB2_lgJ_phi)

        fw_left_ss_jet_phi.append(fw_dat[i].TOB1_sgJ_phi)
        sim_left_ss_jet_phi.append(sim_dat[i].TOB1_sgJ_phi)
        fw_right_ss_jet_phi.append(fw_dat[i].TOB2_sgJ_phi)
        sim_right_ss_jet_phi.append(sim_dat[i].TOB2_sgJ_phi)

        # Eta
        fw_left_ll_jet_eta.append(fw_dat[i].TOB1_lgLJ_eta)
        sim_left_ll_jet_eta.append(sim_dat[i].TOB1_lgLJ_eta)
        fw_right_ll_jet_eta.append(fw_dat[i].TOB2_lgLJ_eta)
        sim_right_ll_jet_eta.append(sim_dat[i].TOB2_lgLJ_eta)

        fw_left_ls_jet_eta.append(fw_dat[i].TOB1_lgJ_eta)
        sim_left_ls_jet_eta.append(sim_dat[i].TOB1_lgJ_eta)
        fw_right_ls_jet_eta.append(fw_dat[i].TOB2_lgJ_eta)
        sim_right_ls_jet_eta.append(sim_dat[i].TOB2_lgJ_eta)

        fw_left_ss_jet_eta.append(fw_dat[i].TOB1_sgJ_eta)
        sim_left_ss_jet_eta.append(sim_dat[i].TOB1_sgJ_eta)
        fw_right_ss_jet_eta.append(fw_dat[i].TOB2_sgJ_eta)
        sim_right_ss_jet_eta.append(sim_dat[i].TOB2_sgJ_eta)

        #PUC
        fw_puc.append(fw_dat[i].puc)
        sim_puc.append(sim_dat[i].puc)

        # JWJ
        fw_mhtx.append(fw_dat[i].jwj_mhtx)
        sim_mhtx.append(sim_dat[i].jwj_mhtx)
        fw_mhty.append(fw_dat[i].jwj_mhty)
        sim_mhty.append(sim_dat[i].jwj_mhty)
        fw_metx.append(fw_dat[i].jwj_metx)
        sim_metx.append(sim_dat[i].jwj_metx)
        fw_mety.append(fw_dat[i].jwj_mety)
        sim_mety.append(sim_dat[i].jwj_mety)
        fw_mstx.append(fw_dat[i].jwj_mstx)
        sim_mstx.append(sim_dat[i].jwj_mstx)
        fw_msty.append(fw_dat[i].jwj_msty)
        sim_msty.append(sim_dat[i].jwj_msty)

        #JWJ sumET
        fw_jwj_tot_sumEt.append(fw_dat[i].jwj_tot_sumEt)
        sim_jwj_tot_sumEt.append(sim_dat[i].jwj_tot_sumEt)
        fw_jwj_hard_sumEt.append(fw_dat[i].jwj_hard_sumEt)
        sim_jwj_hard_sumEt.append(sim_dat[i].jwj_hard_sumEt)
        fw_jwj_soft_sumEt.append(fw_dat[i].jwj_soft_sumEt)
        sim_jwj_soft_sumEt.append(sim_dat[i].jwj_soft_sumEt)

        # Alt MET
        fw_nc_metx.append(fw_dat[i].nc_metx)
        sim_nc_metx.append(sim_dat[i].nc_metx)
        fw_nc_mety.append(fw_dat[i].nc_mety)
        sim_nc_mety.append(sim_dat[i].nc_mety)
        fw_rms_metx.append(fw_dat[i].rms_metx)
        sim_rms_metx.append(sim_dat[i].rms_metx)
        fw_rms_mety.append(fw_dat[i].rms_mety)
        sim_rms_mety.append(sim_dat[i].rms_mety)
        fw_nc_sumEt.append(fw_dat[i].nc_sumEt)
        sim_nc_sumEt.append(sim_dat[i].nc_sumEt)
        fw_rms_sumEt.append(fw_dat[i].rms_sumEt)
        sim_rms_sumEt.append(sim_dat[i].rms_sumEt)


    #call function from plotting functions file
    flab = f"Comparison_Histogram"
    plab_base = f"gFEX"

    en_xlab = f"ADC Counts (x 0.2 GeV)"
    ang_xlab = f"Bin Number"

    #jet ET
    pf.comp_hist(sim_left_ll_jet_en, fw_left_ll_jet_en, f"{plab_base} Leading Large Jet ET",
                 f"Leading_Large_R_Jet_ET_{flab}", en_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ll_jet_en, fw_right_ll_jet_en, f"{plab_base} Leading Large Jet ET",
                 f"Leading_Large_R_Jet_ET_{flab}", en_xlab, jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ls_jet_en, fw_left_ls_jet_en, f"{plab_base} Leading Small Jet ET",
                 f"Leading_Small_R_Jet_ET_{flab}", en_xlab,  jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ls_jet_en, fw_right_ls_jet_en, f"{plab_base} Leading Small Jet ET",
                 f"Leading_Small_R_Jet_ET_{flab}", en_xlab,  jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ss_jet_en, fw_left_ss_jet_en, f"{plab_base} Subleading Small Jet ET",
                 f"Subleading_Small_R_Jet_ET_{flab}", en_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ss_jet_en, fw_right_ss_jet_en, f"{plab_base} Subleading Small Jet ET",
                 f"Subleading_Small_R_Jet_ET_{flab}", en_xlab, jet_comp_folder_str, RUN_num, right_half)

    #Jet phi
    pf.comp_hist(sim_left_ll_jet_phi, fw_left_ll_jet_phi, f"{plab_base} Leading Large Jet Phi",
                 f"Leading_Large_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ll_jet_phi, fw_right_ll_jet_phi, f"{plab_base} Leading Large Jet Phi",
                 f"Leading_Large_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ls_jet_phi, fw_left_ls_jet_phi, f"{plab_base} Leading Small Jet Phi",
                 f"Leading_Small_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ls_jet_phi, fw_right_ls_jet_phi, f"{plab_base} Leading Small Jet Phi",
                 f"Leading_Small_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ss_jet_phi, fw_left_ss_jet_phi, f"{plab_base} Subleading Small Jet Phi",
                 f"Subleading_Small_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ss_jet_phi, fw_right_ss_jet_phi, f"{plab_base} Subleading Small Jet Phi",
                 f"Subleading_Small_R_Jet_Phi_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)

    #Jet eta
    pf.comp_hist(sim_left_ll_jet_eta, fw_left_ll_jet_eta, f"{plab_base} Leading Large Jet Eta",
                 f"Leading_Large_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ll_jet_eta, fw_right_ll_jet_eta, f"{plab_base} Leading Large Jet Eta",
                 f"Leading_Large_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ls_jet_eta, fw_left_ls_jet_eta, f"{plab_base} Leading Small Jet Eta",
                 f"Leading_Small_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ls_jet_eta, fw_right_ls_jet_eta, f"{plab_base} Leading Small Jet Eta",
                 f"Leading_Small_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)
    pf.comp_hist(sim_left_ss_jet_eta, fw_left_ss_jet_eta, f"{plab_base} Subleading Small Jet Eta",
                 f"Subleading_Small_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, left_half)
    pf.comp_hist(sim_right_ss_jet_eta, fw_right_ss_jet_eta, f"{plab_base} Subleading Small Jet Eta",
                 f"Subleading_Small_R_Jet_Eta_{flab}", ang_xlab, jet_comp_folder_str, RUN_num, right_half)

    #PUC
    pf.comp_hist(sim_puc, fw_puc, f"{plab_base} PUC",
                 f"PUC_{flab}", en_xlab, jet_comp_folder_str, RUN_num, FPGA_str)

    #JWJ
    pf.comp_hist(sim_metx, fw_metx, f"{plab_base} JWJ METX", f"JWJ_METX_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_mety, fw_mety, f"{plab_base} JWJ METY", f"JWJ_METY_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_mhtx, fw_mhtx, f"{plab_base} JWJ MHTX", f"JWJ_MHTX_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_mhty, fw_mhty, f"{plab_base} JWJ MHTY", f"JWJ_MHTY_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_mstx, fw_mstx, f"{plab_base} JWJ MSTX", f"JWJ_MSTX_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_msty, fw_msty, f"{plab_base} JWJ MSTY", f"JWJ_MSTY_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)

    #Alt MET
    pf.comp_hist(sim_nc_metx, fw_nc_metx, f"{plab_base} NC METX", f"NC_METX_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_nc_mety, fw_nc_mety, f"{plab_base} NC METY", f"NC_METY_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_rms_metx, fw_rms_metx, f"{plab_base} Rho+RMS METX", f"RMS_METX_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_rms_mety, fw_rms_mety, f"{plab_base} Rho+RMS METY", f"RMS_METY_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_nc_sumEt, fw_nc_sumEt, f"{plab_base} NC Sum ET", f"NC_SUMET_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)
    pf.comp_hist(sim_rms_sumEt, fw_rms_sumEt, f"{plab_base} Rho+RMS Sum ET", f"RMS_SUMET_{flab}", en_xlab, met_comp_folder_str,
                 RUN_num, FPGA_str)



    pf.comp_hist(sim_jwj_tot_sumEt, fw_jwj_tot_sumEt, f"JWJ Total sumET", f"JWJ_SUMET",
                     en_xlab, met_comp_folder_str, RUN_num, FPGA_str)
    pf.comp_hist(sim_jwj_hard_sumEt, fw_jwj_hard_sumEt, f"JWJ Hard sumET", f"JWJ_SUMHT",
                     en_xlab, met_comp_folder_str, RUN_num, FPGA_str)
    pf.comp_hist(sim_jwj_soft_sumEt, fw_jwj_soft_sumEt, f"JWJ Soft sumET", f"JWJ_SUMST",
                     en_xlab, met_comp_folder_str, RUN_num, FPGA_str)

    return

#plotting infrastructure for any one-off or specialty distributions
def misc_distributions(sim_dat, fw_dat, RUN_num):

    #Bool flags for custom debugging
    deleta = True

    # DQ checks for variable assignments

    # FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    # Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    jet_comp_folder_str = "Comparison_Histograms/JetTOBs/"
    met_comp_folder_str = "Comparison_Histograms/METTOBs/"

    # Energies
    fw_left_ll_jet_en = array('d')
    sim_left_ll_jet_en = array('d')
    fw_right_ll_jet_en = array('d')
    sim_right_ll_jet_en = array('d')

    fw_left_ls_jet_en = array('d')
    sim_left_ls_jet_en = array('d')
    fw_right_ls_jet_en = array('d')
    sim_right_ls_jet_en = array('d')

    fw_left_ss_jet_en = array('d')
    sim_left_ss_jet_en = array('d')
    fw_right_ss_jet_en = array('d')
    sim_right_ss_jet_en = array('d')

    # Phi
    fw_left_ll_jet_phi = array('d')
    sim_left_ll_jet_phi = array('d')
    fw_right_ll_jet_phi = array('d')
    sim_right_ll_jet_phi = array('d')

    fw_left_ls_jet_phi = array('d')
    sim_left_ls_jet_phi = array('d')
    fw_right_ls_jet_phi = array('d')
    sim_right_ls_jet_phi = array('d')

    fw_left_ss_jet_phi = array('d')
    sim_left_ss_jet_phi = array('d')
    fw_right_ss_jet_phi = array('d')
    sim_right_ss_jet_phi = array('d')

    # Eta
    fw_left_ll_jet_eta = array('d')
    sim_left_ll_jet_eta = array('d')
    fw_right_ll_jet_eta = array('d')
    sim_right_ll_jet_eta = array('d')

    fw_left_ls_jet_eta = array('d')
    sim_left_ls_jet_eta = array('d')
    fw_right_ls_jet_eta = array('d')
    sim_right_ls_jet_eta = array('d')

    fw_left_ss_jet_eta = array('d')
    sim_left_ss_jet_eta = array('d')
    fw_right_ss_jet_eta = array('d')
    sim_right_ss_jet_eta = array('d')

    # PUC
    fw_puc = array('d')
    sim_puc = array('d')

    # JWJ
    fw_mhtx = array('d')
    sim_mhtx = array('d')
    fw_mhty = array('d')
    sim_mhty = array('d')
    fw_metx = array('d')
    sim_metx = array('d')
    fw_mety = array('d')
    sim_mety = array('d')
    fw_mstx = array('d')
    sim_mstx = array('d')
    fw_msty = array('d')
    sim_msty = array('d')

    # JWJ sumET
    fw_jwj_tot_sumEt = array('d')
    sim_jwj_tot_sumEt = array('d')
    fw_jwj_hard_sumEt = array('d')
    sim_jwj_hard_sumEt = array('d')
    fw_jwj_soft_sumEt = array('d')
    sim_jwj_soft_sumEt = array('d')

    # Alt MET
    fw_nc_metx = array('d')
    sim_nc_metx = array('d')
    fw_nc_mety = array('d')
    sim_nc_mety = array('d')
    fw_rms_metx = array('d')
    sim_rms_metx = array('d')
    fw_rms_mety = array('d')
    sim_rms_mety = array('d')
    fw_nc_sumEt = array('d')
    sim_nc_sumEt = array('d')
    fw_rms_sumEt = array('d')
    sim_rms_sumEt = array('d')

    for i in range(l):
        # Energy
        fw_left_ll_jet_en.append(fw_dat[i].TOB1_lgLJ_et)
        sim_left_ll_jet_en.append(sim_dat[i].TOB1_lgLJ_et)
        fw_right_ll_jet_en.append(fw_dat[i].TOB2_lgLJ_et)
        sim_right_ll_jet_en.append(sim_dat[i].TOB2_lgLJ_et)

        fw_left_ls_jet_en.append(fw_dat[i].TOB1_lgJ_et)
        sim_left_ls_jet_en.append(sim_dat[i].TOB1_lgJ_et)
        fw_right_ls_jet_en.append(fw_dat[i].TOB2_lgJ_et)
        sim_right_ls_jet_en.append(sim_dat[i].TOB2_lgJ_et)

        fw_left_ss_jet_en.append(fw_dat[i].TOB1_sgJ_et)
        sim_left_ss_jet_en.append(sim_dat[i].TOB1_sgJ_et)
        fw_right_ss_jet_en.append(fw_dat[i].TOB2_sgJ_et)
        sim_right_ss_jet_en.append(sim_dat[i].TOB2_sgJ_et)

        # Phi
        fw_left_ll_jet_phi.append(fw_dat[i].TOB1_lgLJ_phi)
        sim_left_ll_jet_phi.append(sim_dat[i].TOB1_lgLJ_phi)
        fw_right_ll_jet_phi.append(fw_dat[i].TOB2_lgLJ_phi)
        sim_right_ll_jet_phi.append(sim_dat[i].TOB2_lgLJ_phi)

        fw_left_ls_jet_phi.append(fw_dat[i].TOB1_lgJ_phi)
        sim_left_ls_jet_phi.append(sim_dat[i].TOB1_lgJ_phi)
        fw_right_ls_jet_phi.append(fw_dat[i].TOB2_lgJ_phi)
        sim_right_ls_jet_phi.append(sim_dat[i].TOB2_lgJ_phi)

        fw_left_ss_jet_phi.append(fw_dat[i].TOB1_sgJ_phi)
        sim_left_ss_jet_phi.append(sim_dat[i].TOB1_sgJ_phi)
        fw_right_ss_jet_phi.append(fw_dat[i].TOB2_sgJ_phi)
        sim_right_ss_jet_phi.append(sim_dat[i].TOB2_sgJ_phi)

        # Eta
        fw_left_ll_jet_eta.append(fw_dat[i].TOB1_lgLJ_eta)
        sim_left_ll_jet_eta.append(sim_dat[i].TOB1_lgLJ_eta)
        fw_right_ll_jet_eta.append(fw_dat[i].TOB2_lgLJ_eta)
        sim_right_ll_jet_eta.append(sim_dat[i].TOB2_lgLJ_eta)

        fw_left_ls_jet_eta.append(fw_dat[i].TOB1_lgJ_eta)
        sim_left_ls_jet_eta.append(sim_dat[i].TOB1_lgJ_eta)
        fw_right_ls_jet_eta.append(fw_dat[i].TOB2_lgJ_eta)
        sim_right_ls_jet_eta.append(sim_dat[i].TOB2_lgJ_eta)

        fw_left_ss_jet_eta.append(fw_dat[i].TOB1_sgJ_eta)
        sim_left_ss_jet_eta.append(sim_dat[i].TOB1_sgJ_eta)
        fw_right_ss_jet_eta.append(fw_dat[i].TOB2_sgJ_eta)
        sim_right_ss_jet_eta.append(sim_dat[i].TOB2_sgJ_eta)

        #PUC
        fw_puc.append(fw_dat[i].puc)
        sim_puc.append(sim_dat[i].puc)

        # JWJ
        fw_mhtx.append(fw_dat[i].jwj_mhtx)
        sim_mhtx.append(sim_dat[i].jwj_mhtx)
        fw_mhty.append(fw_dat[i].jwj_mhty)
        sim_mhty.append(sim_dat[i].jwj_mhty)
        fw_metx.append(fw_dat[i].jwj_metx)
        sim_metx.append(sim_dat[i].jwj_metx)
        fw_mety.append(fw_dat[i].jwj_mety)
        sim_mety.append(sim_dat[i].jwj_mety)
        fw_mstx.append(fw_dat[i].jwj_mstx)
        sim_mstx.append(sim_dat[i].jwj_mstx)
        fw_msty.append(fw_dat[i].jwj_msty)
        sim_msty.append(sim_dat[i].jwj_msty)

        #JWJ sumET
        fw_jwj_tot_sumEt.append(fw_dat[i].jwj_tot_sumEt)
        sim_jwj_tot_sumEt.append(sim_dat[i].jwj_tot_sumEt)
        fw_jwj_hard_sumEt.append(fw_dat[i].jwj_hard_sumEt)
        sim_jwj_hard_sumEt.append(sim_dat[i].jwj_hard_sumEt)
        fw_jwj_soft_sumEt.append(fw_dat[i].jwj_soft_sumEt)
        sim_jwj_soft_sumEt.append(sim_dat[i].jwj_soft_sumEt)

        # Alt MET
        fw_nc_metx.append(fw_dat[i].nc_metx)
        sim_nc_metx.append(sim_dat[i].nc_metx)
        fw_nc_mety.append(fw_dat[i].nc_mety)
        sim_nc_mety.append(sim_dat[i].nc_mety)
        fw_rms_metx.append(fw_dat[i].rms_metx)
        sim_rms_metx.append(sim_dat[i].rms_metx)
        fw_rms_mety.append(fw_dat[i].rms_mety)
        sim_rms_mety.append(sim_dat[i].rms_mety)
        fw_nc_sumEt.append(fw_dat[i].nc_sumEt)
        sim_nc_sumEt.append(sim_dat[i].nc_sumEt)
        fw_rms_sumEt.append(fw_dat[i].rms_sumEt)
        sim_rms_sumEt.append(sim_dat[i].rms_sumEt)

    if(deleta):
        #delta eta plots
        LR_deleta = array('d')
        SR_deleta = array('d')

        for i in range(l):
            if fw_left_ll_jet_en[i] != sim_left_ll_jet_en[i]:
                LR_deleta.append(fw_left_ll_jet_eta[i] - sim_left_ll_jet_eta[i])
            if fw_right_ll_jet_en[i] != sim_right_ll_jet_en[i]:
                LR_deleta.append(fw_right_ll_jet_eta[i] - sim_right_ll_jet_eta[i])
            if fw_left_ls_jet_en[i] != sim_left_ls_jet_en[i]:
                SR_deleta.append(fw_left_ls_jet_eta[i] - sim_left_ls_jet_eta[i])
            if fw_right_ls_jet_en[i] != sim_right_ls_jet_en[i]:
                SR_deleta.append(fw_right_ls_jet_eta[i] - sim_right_ls_jet_eta[i])
                
        if len(LR_deleta) != 0:
            pf.hist_1d(LR_deleta, f"FW Eta - CSim Eta Large R Jets",
               f"LR_deleta", jet_comp_folder_str, RUN_num, FPGA_str)
        if len(SR_deleta) != 0:
            pf.hist_1d(SR_deleta, f"FW Eta - CSim Eta Leading Small R Jets",
               f"SR_deleta", jet_comp_folder_str, RUN_num, FPGA_str)

    return

def vec_sum_plots(fw_a_dat, fw_b_dat, fw_c_dat, sim_a_dat, sim_b_dat, sim_c_dat, RUN_num):
    met_comp_folder_str = f"Comparison_Histograms/METTOBs/"
    flab = f"Comparison_Histogram"
    plab_base = f"gFEX"
    en_xlab = f"ADC Counts (x 0.2 GeV)"

    #setup arrays
    fw_a_metx = array('d')
    sim_a_metx = array('d')
    fw_a_mety = array('d')
    sim_a_mety = array('d')
    fw_a_jwj_tot_sumEt = array('d')
    sim_a_jwj_tot_sumEt = array('d')

    fw_b_metx = array('d')
    sim_b_metx = array('d')
    fw_b_mety = array('d')
    sim_b_mety = array('d')
    fw_b_jwj_tot_sumEt = array('d')
    sim_b_jwj_tot_sumEt = array('d')

    fw_c_metx = array('d')
    sim_c_metx = array('d')
    fw_c_mety = array('d')
    sim_c_mety = array('d')
    fw_c_jwj_tot_sumEt = array('d')
    sim_c_jwj_tot_sumEt = array('d')

    for i in range(len(fw_a_dat)):
        fw_a_metx.append(fw_a_dat[i].jwj_metx)
        fw_a_mety.append(fw_a_dat[i].jwj_mety)
        fw_b_metx.append(fw_b_dat[i].jwj_metx)
        fw_b_mety.append(fw_b_dat[i].jwj_mety)
        fw_c_metx.append(fw_c_dat[i].jwj_metx)
        fw_c_mety.append(fw_c_dat[i].jwj_mety)

        sim_a_metx.append(sim_a_dat[i].jwj_metx)
        sim_a_mety.append(sim_a_dat[i].jwj_mety)
        sim_b_metx.append(sim_b_dat[i].jwj_metx)
        sim_b_mety.append(sim_b_dat[i].jwj_mety)
        sim_c_metx.append(sim_c_dat[i].jwj_metx)
        sim_c_mety.append(sim_c_dat[i].jwj_mety)

        fw_a_jwj_tot_sumEt.append(fw_a_dat[i].jwj_tot_sumEt)
        fw_b_jwj_tot_sumEt.append(fw_b_dat[i].jwj_tot_sumEt)
        fw_c_jwj_tot_sumEt.append(fw_c_dat[i].jwj_tot_sumEt)

        sim_a_jwj_tot_sumEt.append(sim_a_dat[i].jwj_tot_sumEt)
        sim_b_jwj_tot_sumEt.append(sim_b_dat[i].jwj_tot_sumEt)
        sim_c_jwj_tot_sumEt.append(sim_c_dat[i].jwj_tot_sumEt)

    fw_all_metx = fw_a_metx + fw_b_metx + fw_c_metx
    fw_all_mety = fw_a_mety + fw_b_mety + fw_c_mety

    sim_all_metx = sim_a_metx + sim_b_metx + sim_c_metx
    sim_all_mety = sim_a_mety + sim_b_mety + sim_c_mety

    fw_all_tot_sumET = fw_a_jwj_tot_sumEt + fw_b_jwj_tot_sumEt + fw_c_jwj_tot_sumEt
    sim_all_tot_sumET = sim_a_jwj_tot_sumEt + sim_b_jwj_tot_sumEt + sim_c_jwj_tot_sumEt

    fw_te_highest = array('d')
    sim_te_highest = array('d')

    for i in range(len(fw_a_jwj_tot_sumEt)):
        fw_max = fw_a_jwj_tot_sumEt[i]
        if fw_b_jwj_tot_sumEt[i] > fw_max:
            fw_max = fw_b_jwj_tot_sumEt[i]
        if fw_c_jwj_tot_sumEt[i] > fw_max:
            fw_max = fw_c_jwj_tot_sumEt[i]

        sim_max = sim_a_jwj_tot_sumEt[i]
        if sim_b_jwj_tot_sumEt[i] > sim_max:
            sim_max = sim_b_jwj_tot_sumEt[i]
        if sim_c_jwj_tot_sumEt[i] > sim_max:
            sim_max = sim_c_jwj_tot_sumEt[i]

        fw_te_highest.append(fw_max)
        sim_te_highest.append(sim_max)
    
    
    fw_all_met = array('d')
    sim_all_met = array('d')

    for i in range(len(fw_all_metx)):
        fw_all_met.append(hf.calc_vec_mag(fw_all_metx[i], fw_all_mety[i]))
        sim_all_met.append(hf.calc_vec_mag(sim_all_metx[i], sim_all_mety[i]))

    pf.comp_hist(sim_all_met, fw_all_met, f"{plab_base} Calculated MET", f"Combined_MET_{flab}", en_xlab,
                 met_comp_folder_str, RUN_num, "All")
    pf.comp_hist(sim_all_tot_sumET, fw_all_tot_sumET, f"{plab_base} Total SumET", f"Tot_SumET_{flab}", en_xlab,
                 met_comp_folder_str, RUN_num, "All")
    pf.comp_hist(sim_te_highest, fw_te_highest, f"{plab_base} Total SumET Highest Val", f"Tot_SumET_Highest_{flab}", en_xlab,
                 met_comp_folder_str, RUN_num, "All")
    return

def make_distribution_plots(ev_list, src_str, RUN_num):

    fpga_a_list = []
    fpga_b_list = []
    fpga_c_list = []

    l = len(ev_list)

    for i in range(0, l):
        fpga_a_list.append(ev_list[i].FPGA_A)
        fpga_b_list.append(ev_list[i].FPGA_B)
        fpga_c_list.append(ev_list[i].FPGA_C)

    distributions(fpga_a_list, src_str, RUN_num)
    distributions(fpga_b_list, src_str, RUN_num)
    distributions(fpga_c_list, src_str, RUN_num)

    return

def make_comp_histograms(fw_ev_list, sim_ev_list, RUN_num):

    #organize events into FPGAs
    fw_fpga_a_list = []
    fw_fpga_b_list = []
    fw_fpga_c_list = []

    sim_fpga_a_list = []
    sim_fpga_b_list = []
    sim_fpga_c_list = []

    l = len(fw_ev_list)

    for i in range(0,l):
        fw_fpga_a_list.append(fw_ev_list[i].FPGA_A)
        fw_fpga_b_list.append(fw_ev_list[i].FPGA_B)
        fw_fpga_c_list.append(fw_ev_list[i].FPGA_C)
        sim_fpga_a_list.append(sim_ev_list[i].FPGA_A)
        sim_fpga_b_list.append(sim_ev_list[i].FPGA_B)
        sim_fpga_c_list.append(sim_ev_list[i].FPGA_C)


    fpga_comp_hist(sim_fpga_a_list, fw_fpga_a_list, RUN_num)
    fpga_comp_hist(sim_fpga_b_list, fw_fpga_b_list, RUN_num)
    fpga_comp_hist(sim_fpga_c_list, fw_fpga_c_list, RUN_num)


    return

def make_misc_histograms(fw_ev_list, sim_ev_list, RUN_num):

    #organize events into FPGAs
    fw_fpga_a_list = []
    fw_fpga_b_list = []
    fw_fpga_c_list = []

    sim_fpga_a_list = []
    sim_fpga_b_list = []
    sim_fpga_c_list = []

    l = len(fw_ev_list)

    for i in range(0,l):
        fw_fpga_a_list.append(fw_ev_list[i].FPGA_A)
        fw_fpga_b_list.append(fw_ev_list[i].FPGA_B)
        fw_fpga_c_list.append(fw_ev_list[i].FPGA_C)
        sim_fpga_a_list.append(sim_ev_list[i].FPGA_A)
        sim_fpga_b_list.append(sim_ev_list[i].FPGA_B)
        sim_fpga_c_list.append(sim_ev_list[i].FPGA_C)

    misc_distributions(sim_fpga_a_list, fw_fpga_a_list, RUN_num)
    misc_distributions(sim_fpga_b_list, fw_fpga_b_list, RUN_num)
    misc_distributions(sim_fpga_c_list, fw_fpga_c_list, RUN_num)

    vec_sum_plots(fw_fpga_a_list, fw_fpga_b_list, fw_fpga_c_list, sim_fpga_a_list, sim_fpga_b_list, sim_fpga_c_list, RUN_num)

    return
