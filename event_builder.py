#file to create a list of event objects to be referenced for comparisons

#import classes and other needed functions
from classes import fpga_ev
from classes import event
import helper_functions as hf
import jet_tob_parsing as jtp
import global_tob_parsing as gtp

# function takes in all necessary info and creates fpga object
def fpga_builder(fpga_id, j_l1id, TOB1_lgJ_eta, TOB1_lgJ_phi, TOB1_lgJ_et, TOB1_lgJ_satbit,
                 TOB1_sgJ_eta, TOB1_sgJ_phi, TOB1_sgJ_et, TOB1_sgJ_satbit, TOB1_lgLJ_eta,
                 TOB1_lgLJ_phi, TOB1_lgLJ_et, TOB1_lgLJ_satbit, puc, TOB1_bcid,  TOB2_lgJ_eta, TOB2_lgJ_phi,
                 TOB2_lgJ_et, TOB2_lgJ_satbit, TOB2_sgJ_eta, TOB2_sgJ_phi, TOB2_sgJ_et, TOB2_sgJ_satbit,
                 TOB2_lgLJ_eta, TOB2_lgLJ_phi, TOB2_lgLJ_et, TOB2_lgLJ_satbit, TOB2_bcid, g_l1id, jwj_mstx, jwj_msty,
                 jwj_mhtx, jwj_mhty, jwj_metx, jwj_mety, nc_metx, nc_mety, rms_metx, rms_mety, nc_sumEt, rms_sumEt,
                 tot_sumEt, hard_sumEt, soft_sumEt):

    fpga = fpga_ev(fpga_id)

    fpga.j_l1id             = j_l1id
    fpga.TOB1_lgJ_eta     = TOB1_lgJ_eta
    fpga.TOB1_lgJ_phi     = TOB1_lgJ_phi
    fpga.TOB1_lgJ_et      = TOB1_lgJ_et
    fpga.TOB1_lgJ_satbit  = TOB1_lgJ_satbit
    fpga.TOB1_sgJ_eta     = TOB1_sgJ_eta
    fpga.TOB1_sgJ_phi     = TOB1_sgJ_phi
    fpga.TOB1_sgJ_et      = TOB1_sgJ_et
    fpga.TOB1_sgJ_satbit  = TOB1_sgJ_satbit
    fpga.TOB1_lgLJ_eta    = TOB1_lgLJ_eta
    fpga.TOB1_lgLJ_phi    = TOB1_lgLJ_phi
    fpga.TOB1_lgLJ_et     = TOB1_lgLJ_et
    fpga.TOB1_lgLJ_satbit = TOB1_lgLJ_satbit
    fpga.puc              = puc
    fpga.TOB1_bcid        = TOB1_bcid
    fpga.TOB2_lgJ_eta     = TOB2_lgJ_eta
    fpga.TOB2_lgJ_phi     = TOB2_lgJ_phi
    fpga.TOB2_lgJ_et      = TOB2_lgJ_et
    fpga.TOB2_lgJ_satbit  = TOB2_lgJ_satbit
    fpga.TOB2_sgJ_eta     = TOB2_sgJ_eta
    fpga.TOB2_sgJ_phi     = TOB2_sgJ_phi
    fpga.TOB2_sgJ_et      = TOB2_sgJ_et
    fpga.TOB2_sgJ_satbit  = TOB2_sgJ_satbit
    fpga.TOB2_lgLJ_eta    = TOB2_lgLJ_eta
    fpga.TOB2_lgLJ_phi    = TOB2_lgLJ_phi
    fpga.TOB2_lgLJ_et     = TOB2_lgLJ_et
    fpga.TOB2_lgLJ_satbit = TOB2_lgLJ_satbit
    fpga.TOB2_bcid        = TOB2_bcid
    fpga.g_l1id           = g_l1id
    fpga.jwj_mstx             = jwj_mstx
    fpga.jwj_msty             = jwj_msty
    fpga.jwj_mhtx             = jwj_mhtx
    fpga.jwj_mhty             = jwj_mhty
    fpga.jwj_metx             = jwj_metx
    fpga.jwj_mety             = jwj_mety
    fpga.nc_metx           = nc_metx
    fpga.nc_mety           = nc_mety
    fpga.rms_metx          = rms_metx
    fpga.rms_mety          = rms_mety
    fpga.nc_sumEt          = nc_sumEt
    fpga.rms_sumEt         = rms_sumEt
    fpga.jwj_tot_sumEt      = tot_sumEt
    fpga.jwj_hard_sumEt     = hard_sumEt
    fpga.jwj_soft_sumEt    = soft_sumEt
    

    return fpga



#builds and returns the event object
def event_builder(l1id, fpga_a, fpga_b, fpga_c):

    return event(l1id, fpga_a, fpga_b, fpga_c)


# builds the events based on the files read in
# inputs are for files corresponding to each fpga (a,b,c) and jet or global tobs (j or g)
def event_list_builder(aj, ag, bj, bg, cj, cg):

    #initialize empty list of events
    ev_list = []

    aj = hf.get_lines(aj)
    ag = hf.get_lines(ag)
    bj = hf.get_lines(bj)
    bg = hf.get_lines(bg)
    cj = hf.get_lines(cj)
    cg = hf.get_lines(cg)

    aj_l1id, a_lead_gJ_left_phi, a_lead_gJ_left_eta, a_lead_gJ_left_en, a_lead_gJ_left_satbit, a_sub_gJ_left_phi, \
    a_sub_gJ_left_eta, a_sub_gJ_left_en, a_sub_gJ_left_satbit, a_lead_gLJ_left_phi, a_lead_gLJ_left_eta, \
    a_lead_gLJ_left_en, a_lead_gLJ_left_satbit, a_puc, a_bcid_left, a_lead_gJ_right_phi, a_lead_gJ_right_eta, \
    a_lead_gJ_right_en, a_lead_gJ_right_satbit, a_sub_gJ_right_phi, a_sub_gJ_right_eta, a_sub_gJ_right_en, \
    a_sub_gJ_right_satbit, a_lead_gLJ_right_phi, a_lead_gLJ_right_eta, a_lead_gLJ_right_en, a_lead_gLJ_right_satbit, \
    a_bcid_right = jtp.jetTOB_full_parse(aj, 10)

    bj_l1id, b_lead_gJ_left_phi, b_lead_gJ_left_eta, b_lead_gJ_left_en, b_lead_gJ_left_satbit, b_sub_gJ_left_phi, \
    b_sub_gJ_left_eta, b_sub_gJ_left_en, b_sub_gJ_left_satbit, b_lead_gLJ_left_phi, b_lead_gLJ_left_eta, \
    b_lead_gLJ_left_en, b_lead_gLJ_left_satbit, b_puc, b_bcid_left, b_lead_gJ_right_phi, b_lead_gJ_right_eta, \
    b_lead_gJ_right_en, b_lead_gJ_right_satbit, b_sub_gJ_right_phi, b_sub_gJ_right_eta, b_sub_gJ_right_en, \
    b_sub_gJ_right_satbit, b_lead_gLJ_right_phi, b_lead_gLJ_right_eta, b_lead_gLJ_right_en, b_lead_gLJ_right_satbit, \
    b_bcid_right = jtp.jetTOB_full_parse(bj, 10)

    cj_l1id, c_lead_gJ_left_phi, c_lead_gJ_left_eta, c_lead_gJ_left_en, c_lead_gJ_left_satbit, c_sub_gJ_left_phi, \
    c_sub_gJ_left_eta, c_sub_gJ_left_en, c_sub_gJ_left_satbit, c_lead_gLJ_left_phi, c_lead_gLJ_left_eta, \
    c_lead_gLJ_left_en, c_lead_gLJ_left_satbit, c_puc, c_bcid_left, c_lead_gJ_right_phi, c_lead_gJ_right_eta, \
    c_lead_gJ_right_en, c_lead_gJ_right_satbit, c_sub_gJ_right_phi, c_sub_gJ_right_eta, c_sub_gJ_right_en, \
    c_sub_gJ_right_satbit, c_lead_gLJ_right_phi, c_lead_gLJ_right_eta, c_lead_gLJ_right_en, c_lead_gLJ_right_satbit, \
    c_bcid_right = jtp.jetTOB_full_parse(cj, 10)

    ag_l1id, a_jwj_mhtx, a_jwj_mhty, a_jwj_mstx, a_jwj_msty, a_jwj_metx, a_jwj_mety, a_nc_metx, a_nc_mety, a_rms_metx, \
    a_rms_mety, a_ncsumEt, a_rmssumEt, a_tot_sumEt, a_hard_sumEt, a_soft_sumEt = gtp.globalTOB_full_parse(ag, 10)

    bg_l1id, b_jwj_mhtx, b_jwj_mhty, b_jwj_mstx, b_jwj_msty, b_jwj_metx, b_jwj_mety, b_nc_metx, b_nc_mety, b_rms_metx, \
    b_rms_mety, b_ncsumEt, b_rmssumEt, b_tot_sumEt, b_hard_sumEt, b_soft_sumEt = gtp.globalTOB_full_parse(bg, 10)
    
    cg_l1id, c_jwj_mhtx, c_jwj_mhty, c_jwj_mstx, c_jwj_msty, c_jwj_metx, c_jwj_mety, c_nc_metx, c_nc_mety, c_rms_metx, \
    c_rms_mety, c_ncsumEt, c_rmssumEt, c_tot_sumEt, c_hard_sumEt, c_soft_sumEt  = gtp.globalTOB_full_parse(cg, 10)

    

    if((len(aj_l1id) == len(bj_l1id)) and (len(aj_l1id) == len(cj_l1id)) and (len(aj_l1id) == len(ag_l1id))
    and (len(aj_l1id) == len(bg_l1id)) and (len(aj_l1id) == len(cg_l1id))):
        l = len(aj_l1id)
    else:
        print('Error: L1ID arrays are not all identical, please check and try again, '
              'length of event array has been set to 0')

    #loop through all the arrays to start filling events
    for i in range(0,l):
        #build fpgas for the given event
        fpga_a = fpga_builder('A', aj_l1id[i], a_lead_gJ_left_eta[i], a_lead_gJ_left_phi[i], a_lead_gJ_left_en[i],
            a_lead_gJ_left_satbit[i], a_sub_gJ_left_eta[i], a_sub_gJ_left_phi[i], a_sub_gJ_left_en[i],
            a_sub_gJ_left_satbit[i], a_lead_gLJ_left_eta[i], a_lead_gLJ_left_phi[i], a_lead_gLJ_left_en[i],
            a_lead_gLJ_left_satbit[i], a_puc[i], a_bcid_left[i], a_lead_gJ_right_eta[i], a_lead_gJ_right_phi[i],
            a_lead_gJ_right_en[i], a_lead_gJ_right_satbit[i], a_sub_gJ_right_eta[i], a_sub_gJ_right_phi[i],
            a_sub_gJ_right_en[i], a_sub_gJ_right_satbit[i], a_lead_gLJ_right_eta[i], a_lead_gLJ_right_phi[i],
            a_lead_gLJ_right_en[i], a_lead_gLJ_right_satbit[i], a_bcid_right[i], ag_l1id[i], a_jwj_mstx[i],
            a_jwj_msty[i], a_jwj_mhtx[i], a_jwj_mhty[i], a_jwj_metx[i], a_jwj_mety[i], a_nc_metx[i], a_nc_mety[i],
            a_rms_metx[i], a_rms_mety[i], a_ncsumEt[i], a_rmssumEt[i], a_tot_sumEt[i], a_hard_sumEt[i], a_soft_sumEt[i])

        fpga_b = fpga_builder('B', bj_l1id[i], b_lead_gJ_left_eta[i], b_lead_gJ_left_phi[i], b_lead_gJ_left_en[i],
            b_lead_gJ_left_satbit[i], b_sub_gJ_left_eta[i], b_sub_gJ_left_phi[i], b_sub_gJ_left_en[i],
            b_sub_gJ_left_satbit[i], b_lead_gLJ_left_eta[i], b_lead_gLJ_left_phi[i], b_lead_gLJ_left_en[i],
            b_lead_gLJ_left_satbit[i], b_puc[i], b_bcid_left[i], b_lead_gJ_right_eta[i], b_lead_gJ_right_phi[i],
            b_lead_gJ_right_en[i], b_lead_gJ_right_satbit[i], b_sub_gJ_right_eta[i], b_sub_gJ_right_phi[i],
            b_sub_gJ_right_en[i], b_sub_gJ_right_satbit[i], b_lead_gLJ_right_eta[i], b_lead_gLJ_right_phi[i],
            b_lead_gLJ_right_en[i], b_lead_gLJ_right_satbit[i], b_bcid_right[i], bg_l1id[i], b_jwj_mstx[i],
            b_jwj_msty[i], b_jwj_mhtx[i], b_jwj_mhty[i], b_jwj_metx[i], b_jwj_mety[i], b_nc_metx[i], b_nc_mety[i],
            b_rms_metx[i], b_rms_mety[i], b_ncsumEt[i], b_rmssumEt[i], b_tot_sumEt[i], b_hard_sumEt[i], b_soft_sumEt[i])

        fpga_c = fpga_builder('C', cj_l1id[i], c_lead_gJ_left_eta[i], c_lead_gJ_left_phi[i], c_lead_gJ_left_en[i],
            c_lead_gJ_left_satbit[i], c_sub_gJ_left_eta[i], c_sub_gJ_left_phi[i], c_sub_gJ_left_en[i],
            c_sub_gJ_left_satbit[i], c_lead_gLJ_left_eta[i], c_lead_gLJ_left_phi[i], c_lead_gLJ_left_en[i],
            c_lead_gLJ_left_satbit[i], c_puc[i], c_bcid_left[i], c_lead_gJ_right_eta[i], c_lead_gJ_right_phi[i],
            c_lead_gJ_right_en[i], c_lead_gJ_right_satbit[i], c_sub_gJ_right_eta[i], c_sub_gJ_right_phi[i],
            c_sub_gJ_right_en[i], c_sub_gJ_right_satbit[i], c_lead_gLJ_right_eta[i], c_lead_gLJ_right_phi[i],
            c_lead_gLJ_right_en[i], c_lead_gLJ_right_satbit[i], c_bcid_right[i], cg_l1id[i], c_jwj_mstx[i],
            c_jwj_msty[i], c_jwj_mhtx[i], c_jwj_mhty[i], c_jwj_metx[i], c_jwj_mety[i], c_nc_metx[i], c_nc_mety[i],
            c_rms_metx[i], c_rms_mety[i], c_ncsumEt[i], c_rmssumEt[i], c_tot_sumEt[i], c_hard_sumEt[i], c_soft_sumEt[i])

        #determine l1id
        if ((aj_l1id[i] == bj_l1id[i]) and (aj_l1id[i] == cj_l1id[i]) and (aj_l1id[i] == ag_l1id[i])
                and (aj_l1id[i] == bg_l1id[i]) and (aj_l1id[i] == cg_l1id[i])):
            l1id = aj_l1id[i]
        #if they are not all equal, print statement and use FPGA A Jet L1ID
        else:
            print("Could not find agreement L1ID, using FPGA A Jet L1ID")
            l1id = aj_l1id[i]

        #build event
        ev = event_builder(l1id, fpga_a, fpga_b, fpga_c)

        #add to list
        ev_list.append(ev)

    #return list
    return ev_list
