#module for defining the classes fpga_ev and event for storing all parsed information for one event.
#each event will have 3 FPGA objects, which will have all the relevant parsed data
#all data will be accesible in the code as a list of these event objects

#class which will have all the relevant parsing for an FPGA for an event
class fpga_ev:
    #key:
    #     A, B, C        -- FPGA A B or C
    #     TOB1, TOB2     -- left or right half of FPGA
    #     l, s           -- leading or sub leading
    #     gJ, gLJ        -- small or large jet
    #     eta, phi, et   -- eta, phi or transverse energy
    #     satbit         -- the saturation bit for the hex words
    #     bcid           -- bcid parsed from the jet trailer
    #     mst, mht, met  -- missing soft, hard, or full energy
    #     x, y           -- x or y comp of met terms
    #     nc             -- Noise Cut alternative MET algorithm
    #     rms            --  Rho + RMS alternative MET algorithm

    def __init__(self, fpga_id):
        self.fpga_id          = fpga_id
        self.j_l1id             = 0
        self.TOB1_lgJ_eta     = 0.0
        self.TOB1_lgJ_phi     = 0.0
        self.TOB1_lgJ_et      = 0.0
        self.TOB1_lgJ_satbit  = 0
        self.TOB1_sgJ_eta     = 0.0
        self.TOB1_sgJ_phi     = 0.0
        self.TOB1_sgJ_et      = 0.0
        self.TOB1_sgJ_satbit  = 0
        self.TOB1_lgLJ_eta    = 0.0
        self.TOB1_lgLJ_phi    = 0.0
        self.TOB1_lgLJ_et     = 0.0
        self.TOB1_lgLJ_satbit = 0
        self.TOB1_bcid        = 0.0
        self.TOB2_lgJ_eta     = 0.0
        self.TOB2_lgJ_phi     = 0.0
        self.TOB2_lgJ_et      = 0.0
        self.TOB2_lgJ_satbit  = 0
        self.TOB2_sgJ_eta     = 0.0
        self.TOB2_sgJ_phi     = 0.0
        self.TOB2_sgJ_et      = 0.0
        self.TOB2_sgJ_satbit  = 0
        self.TOB2_lgLJ_eta    = 0.0
        self.TOB2_lgLJ_phi    = 0.0
        self.TOB2_lgLJ_et     = 0.0
        self.TOB2_lgLJ_satbit = 0
        self.TOB2_bcid        = 0.0
        self.puc              = 0
        self.g_l1id           = 0
        self.jwj_mstx             = 0.0
        self.jwj_msty             = 0.0
        self.jwj_mhtx             = 0.0
        self.jwj_mhty             = 0.0
        self.jwj_metx             = 0.0
        self.jwj_mety             = 0.0
        self.nc_metx           = 0.0
        self.nc_mety           = 0.0
        self.rms_metx          = 0.0
        self.rms_mety          = 0.0
        self.nc_sumEt          = 0.0
        self.rms_sumEt         = 0.0
        self.jwj_tot_sumEt      = 0.0
        self.jwj_hard_sumEt     = 0.0
        self.jwj_soft_sumEt     = 0.0

    #debugging function to check values
    def print_fpga_values(self):
        print("Printing all values of FPGA Object:")
        print(f"FPGA: {self.fpga_id}")
        print(f"j_L1ID: {self.j_l1id}")
        print(f"TOB1_lgJ_eta: {self.TOB1_lgJ_eta}")
        print(f"TOB1_lgJ_phi: {self.TOB1_lgJ_phi}")
        print(f"TOB1_lgJ_et: {self.TOB1_lgJ_et}")
        print(f"TOB1_lgJ_satbit: {self.TOB1_lgJ_satbit}")
        print(f"TOB1_sgJ_eta: {self.TOB1_sgJ_eta}")
        print(f"TOB1_sgJ_phi: {self.TOB1_sgJ_phi}")
        print(f"TOB1_sgJ_et: {self.TOB1_sgJ_et}")
        print(f"TOB1_sgJ_satbit: {self.TOB1_sgJ_satbit}")
        print(f"TOB1_lgLJ_eta: {self.TOB1_lgLJ_eta}")
        print(f"TOB1_lgLJ_phi: {self.TOB1_lgLJ_phi}")
        print(f"TOB1_lgLJ_et: {self.TOB1_lgLJ_et}")
        print(f"TOB1_lgLJ_satbit: {self.TOB1_lgLJ_satbit}")
        print(f"TOB1_BCID: {self.TOB1_bcid}")
        print(f"TOB2_lgJ_eta: {self.TOB2_lgJ_eta}")
        print(f"TOB2_lgJ_phi: {self.TOB2_lgJ_phi}")
        print(f"TOB2_lgJ_et: {self.TOB2_lgJ_et}")
        print(f"TOB2_lgJ_satbit: {self.TOB2_lgJ_satbit}")
        print(f"TOB2_sgJ_eta: {self.TOB2_sgJ_eta}")
        print(f"TOB2_sgJ_phi: {self.TOB2_sgJ_phi}")
        print(f"TOB2_sgJ_et: {self.TOB2_sgJ_et}")
        print(f"TOB2_sgJ_satbit: {self.TOB2_sgJ_satbit}")
        print(f"TOB2_lgLJ_eta: {self.TOB2_lgLJ_eta}")
        print(f"TOB2_lgLJ_phi: {self.TOB2_lgLJ_phi}")
        print(f"TOB2_lgLJ_et: {self.TOB2_lgLJ_et}")
        print(f"TOB2_lgLJ_satbit: {self.TOB2_lgLJ_satbit}")
        print(f"TOB2_BCID: {self.TOB2_bcid}")
        print(f"puc: {self.puc}")
        print(f"g_L1ID: {self.g_l1id}")
        print(f"jwj_mstx: {self.jwj_mstx}")
        print(f"jwj_msty: {self.jwj_msty}")
        print(f"jwj_mhtx: {self.jwj_mhtx}")
        print(f"jwj_mhty: {self.jwj_mhty}")
        print(f"jwj_metx: {self.jwj_metx}")
        print(f"jwj_mety: {self.jwj_mety}")
        print(f"nc_metx: {self.nc_metx}")
        print(f"nc_mety: {self.nc_mety}")
        print(f"rms_metx: {self.rms_metx}")
        print(f"rms_mety: {self.rms_mety}")
        print(f"nc_sumEt: {self.nc_sumEt}")
        print(f"rms_sumEt: {self.rms_sumEt}")
        print(f"jwj_tot_sumEt: {self.jwj_tot_sumEt}")
        print(f"jwj_hard_sumEt: {self.jwj_hard_sumEt}")
        print(f"jwj_soft_sumEt: {self.jwj_soft_sumEt}")


#create a class called event
class event:

    # TO DO: Add Unix timestamp

    def __init__(self, l1id, FPGA_A, FPGA_B, FPGA_C):

        self.l1id    = l1id
        self.FPGA_A  = FPGA_A
        self.FPGA_B  = FPGA_B
        self.FPGA_C  = FPGA_C

    #debugging function to check values of all the fpgas
    def print_event_values(self):
        print(f"L1ID: {self.l1id}")
        self.FPGA_A.print_fpga_values()
        self.FPGA_B.print_fpga_values()
        self.FPGA_C.print_fpga_values()



