# Overview

Suite of python functions desgined to compare outputs from gFEX software simulations, hardware simulations, and data dumped from runs in ATLAS.

# How to Run these scripts

## Prerequisites

Please read through this presentation, which serves as a tutorial for running these scripts: https://docs.google.com/presentation/d/1dh7HSzDlqCCdmMdnl1Ewmo4f_7lueok5Kg8DGfqa_ZI/edit?usp=sharing



## Parsing Dumped Data File  

Run the parse_event_dump.py script to generate an input file for the  C simulation as well as formatted jet and global TOB output files to use for later comparisons.  

```
python3 parse_event_dump.py -f "filename" -t <number_tobslices>
```
## Move simulation input file and run simulation  

Copy or move the generated file(Run Number).shared_quads.txt to the data folder of your C simulation  

```
cp SimTOBfiles/file(RUN_number).shared_quads.txt /path/to/csim/data/
```

To run the sim:
```
setupATLAS  
lsetup root  (this will not work unless you have a certain line in your bashrc, copy and paste the version it suggests as a response)  

root  
```
In root environment:  
```
.L gJet.C  
gJet(RUN_number, 2)  
```

## Move files back

Once the sim is done, copy or move the output TOB files back to where your comparison scripts are located.  
The files you will need:  
file(Run number)_atobs.txt  
file(Run number)_btobs.txt  
file(Run number)_ctobs.txt  
file(Run number)_agtobs.txt  
file(Run number)_bgtobs.txt  
file(Run number)_cgtobs.txt  

But, you can also simply run the command   
```
mv data/file(RUN_number)_* /path/back/to/compare/repo/SimTOBfiles
```

## Run Comparison Script

Now, you can change which comparison functions you want to call by manipulating run_compare.py script. When you are ready to run:  

```
python3 run_compare.py -r <run_number>
```

## Shell Scripts  

There are two scripts you can use to move around the data:  
move_outputs.sh  
clear_folders.sh  

running move_outputs allows you to copy all the outputs to your CERNBox (IMPORTANT NOTE: you need to manually change the directory in the script to direct to your desired location rather than my CERNBox)  

```  

./move_outputs.sh "folder_name"
```

running clear folders allows you to clear all the folders of their contents before running a different run (be sure to save any outputs that you want to before running this!) It takes the option of run number and an integer flag. The integer flag should be set to 1 to clear SimTOBfiles and ReadoutTOBfiles folders, any other number will leave them untouched  

```
./clear_folders.sh <run_number> <flag>
```
