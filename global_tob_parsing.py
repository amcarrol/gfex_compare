#Helper functions for organizing/parsing Global TOB data read in from file

import helper_functions as hf

# helper function to parse global tob words from tob output file
def parse_global_tobs(lines):
    #get the header for each event
    l1id = lines[1::17]
    #reverse list to make it easier to index
    l1id = [l[::-1] for l in l1id]
    #take the first element of the split list (separates at space)
    l1id = [l.split()[0] for l in l1id]
    #flip back
    l1id = [l[::-1] for l in l1id]

    jwj_mht_comp = lines[3::17]
    jwj_mst_comp = lines[4::17]
    jwj_met_comp = lines[5::17]
    jwj_trlr = lines[9::17]

    #strip off first two characters (word number and space)
    jwj_mht_comp = [l[2:] for l in jwj_mht_comp]
    jwj_met_comp = [l[2:] for l in jwj_met_comp]
    jwj_mst_comp = [l[2:] for l in jwj_mst_comp]
    jwj_trlr = [l[2:] for l in jwj_trlr]

    #altmet trlr doesn't change position regardless of options below
    altmet_trlr = lines[17::17]
    altmet_trlr = [l[2:] for l in altmet_trlr]



    nc_met_comp = lines[11::17]
    rms_met_comp = lines[12::17]
    alt_met_sums = lines[13::17]

    nc_met_comp = [l[2:] for l in nc_met_comp]
    rms_met_comp = [l[2:] for l in rms_met_comp]
    alt_met_sums = [l[2:] for l in alt_met_sums]

    tot_sumEt = lines[6::17]
    hard_sumEt = lines[7::17]
    soft_sumEt = lines[8::17]

    tot_sumEt = [l[2:] for l in tot_sumEt]
    hard_sumEt = [l[2:] for l in hard_sumEt]
    soft_sumEt = [l[2:] for l in soft_sumEt]



    return(l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr, nc_met_comp, rms_met_comp, alt_met_sums,
           altmet_trlr, tot_sumEt, hard_sumEt, soft_sumEt)

#helper function to parse relevant TOB items from list of parsed TOB words
def parse_global_words(words):

    words = [hf.hex_to_binary(i,32) for i in words]
    #parse out the x,y, and bcid terms
    word1 = [l[0:16] for l in words]
    word2 = [l[16:] for l in words]
    return(word1, word2)

#function to fully parse global tobs from lines in input file to components of words (analog to jetTOB_full_parse)
def globalTOB_full_parse(lines, base):

    l1id, jwj_mht_comp, jwj_met_comp, jwj_mst_comp, jwj_trlr, nc_met_comp, rms_met_comp, alt_met_sums, altmet_trlr,\
    tot_sumEt, hard_sumEt, soft_sumEt = parse_global_tobs(lines)

    jwj_mhtx, jwj_mhty = parse_global_words(jwj_mht_comp)
    jwj_mstx, jwj_msty = parse_global_words(jwj_mst_comp)
    jwj_metx, jwj_mety = parse_global_words(jwj_met_comp)

    nc_metx, nc_mety = parse_global_words(nc_met_comp)
    rms_metx, rms_mety = parse_global_words(rms_met_comp)
    ncsumEt, rmssumEt = parse_global_words(alt_met_sums)

    zero1, tot_sumEt = parse_global_words(tot_sumEt)
    zero2, hard_sumEt = parse_global_words(hard_sumEt)
    zero3, soft_sumEt = parse_global_words(soft_sumEt)

    if base == 10:

        jwj_mhtx = [hf.bin_to_signed_int(s) for s in jwj_mhtx]
        jwj_mhty = [hf.bin_to_signed_int(s) for s in jwj_mhty]
        jwj_mstx = [hf.bin_to_signed_int(s) for s in jwj_mstx]
        jwj_msty = [hf.bin_to_signed_int(s) for s in jwj_msty]
        jwj_metx = [hf.bin_to_signed_int(s) for s in jwj_metx]
        jwj_mety = [hf.bin_to_signed_int(s) for s in jwj_mety]
        nc_metx = [hf.bin_to_signed_int(s) for s in nc_metx]
        nc_mety = [hf.bin_to_signed_int(s) for s in nc_mety]
        rms_metx = [hf.bin_to_signed_int(s) for s in rms_metx]
        rms_mety = [hf.bin_to_signed_int(s) for s in rms_mety]
        ncsumEt = [int(s,2) for s in ncsumEt]  #unsigned int
        rmssumEt = [int(s,2) for s in rmssumEt] #unsigned int
        tot_sumEt = [int(s,2) for s in tot_sumEt] #unsigned int
        hard_sumEt = [hf.bin_to_signed_int(s) for s in hard_sumEt]
        soft_sumEt = [hf.bin_to_signed_int(s) for s in soft_sumEt]

        return (l1id, jwj_mhtx, jwj_mhty, jwj_mstx, jwj_msty, jwj_metx, jwj_mety, nc_metx, nc_mety, rms_metx, rms_mety,
                ncsumEt, rmssumEt, tot_sumEt, hard_sumEt, soft_sumEt)

    #if not base 10, return binary
    else:
        return (l1id, jwj_mhtx, jwj_mhty, jwj_mstx, jwj_msty, jwj_metx, jwj_mety, nc_metx, nc_mety, rms_metx, rms_mety,
                ncsumEt, rmssumEt, tot_sumEt, hard_sumEt, soft_sumEt)


'''
end
'''
