#module for functions to create the various comparison plots between readout and emulated TOBs
import helper_functions as hf
import plotting_functions as pf
import numpy as np
from array import *

#strings for file naming
#define strings for plot names/axes
larger_str = "Leading_LargeR_Jet"
lsmallr_str = "Leading_SmallR_Jet"
ssmallr_str = "Subleading_SmallR_Jet"

et_str = "E_T"
eta_str = "Eta"
phi_str = "Phi"

units = ["(ADC Counts)", "(GeV)"]

def comp_plots(fw_dat, sim_dat, RUN_num):

    #DQ checks for variable assignments

    #FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    #Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    #comp plot specific strings
    compx_str = "TOBs from gFEX Readout Path"
    compy_str = "Emulated from gFEX Input Readout"

    jet_comp_folder_str = "Comparison_Plots/JetTOB_Comparisons/"
    met_comp_folder_str = "Comparison_Plots/METTOB_Comparisons/"


    left_half = FPGA_str + "1"
    right_half = FPGA_str + "2"

    #Energies
    fw_left_ll_jet_en = array('d')
    sim_left_ll_jet_en = array('d')
    fw_right_ll_jet_en = array('d')
    sim_right_ll_jet_en = array('d')

    fw_left_ls_jet_en = array('d')
    sim_left_ls_jet_en = array('d')
    fw_right_ls_jet_en = array('d')
    sim_right_ls_jet_en = array('d')

    fw_left_ss_jet_en = array('d')
    sim_left_ss_jet_en = array('d')
    fw_right_ss_jet_en = array('d')
    sim_right_ss_jet_en = array('d')

    #Phi
    fw_left_ll_jet_phi = array('d')
    sim_left_ll_jet_phi = array('d')
    fw_right_ll_jet_phi = array('d')
    sim_right_ll_jet_phi = array('d')

    fw_left_ls_jet_phi = array('d')
    sim_left_ls_jet_phi = array('d')
    fw_right_ls_jet_phi = array('d')
    sim_right_ls_jet_phi = array('d')

    fw_left_ss_jet_phi = array('d')
    sim_left_ss_jet_phi = array('d')
    fw_right_ss_jet_phi = array('d')
    sim_right_ss_jet_phi = array('d')

    #Eta
    fw_left_ll_jet_eta = array('d')
    sim_left_ll_jet_eta = array('d')
    fw_right_ll_jet_eta = array('d')
    sim_right_ll_jet_eta = array('d')

    fw_left_ls_jet_eta = array('d')
    sim_left_ls_jet_eta = array('d')
    fw_right_ls_jet_eta = array('d')
    sim_right_ls_jet_eta = array('d')

    fw_left_ss_jet_eta = array('d')
    sim_left_ss_jet_eta = array('d')
    fw_right_ss_jet_eta = array('d')
    sim_right_ss_jet_eta = array('d')

    #JWJ
    fw_mhtx  = array('d')
    sim_mhtx = array('d')
    fw_mhty  = array('d')
    sim_mhty = array('d')
    fw_metx  = array('d')
    sim_metx = array('d')
    fw_mety  = array('d')
    sim_mety = array('d')
    fw_mstx  = array('d')
    sim_mstx = array('d')
    fw_msty  = array('d')
    sim_msty = array('d')
    #JWJ sumET
    fw_tot_sumET = array('d')
    sim_tot_sumET = array('d')
    fw_hard_sumET = array('d')
    sim_hard_sumET = array('d')
    fw_soft_sumET = array('d')
    sim_soft_sumET = array('d')

    #Alt MET
    fw_nc_metx = array('d')
    sim_nc_metx = array('d')
    fw_nc_mety = array('d')
    sim_nc_mety = array('d')
    fw_rms_metx =  array('d')
    sim_rms_metx = array('d')
    fw_rms_mety =  array('d')
    sim_rms_mety = array('d')
    fw_nc_sumEt = array('d')
    sim_nc_sumEt = array('d')
    fw_rms_sumEt = array('d')
    sim_rms_sumEt = array('d')

    for i in range(l):
        #Energy
        fw_left_ll_jet_en.append(fw_dat[i].TOB1_lgLJ_et)
        sim_left_ll_jet_en.append(sim_dat[i].TOB1_lgLJ_et)
        fw_right_ll_jet_en.append(fw_dat[i].TOB2_lgLJ_et)
        sim_right_ll_jet_en.append(sim_dat[i].TOB2_lgLJ_et)

        fw_left_ls_jet_en.append(fw_dat[i].TOB1_lgJ_et)
        sim_left_ls_jet_en.append(sim_dat[i].TOB1_lgJ_et)
        fw_right_ls_jet_en.append(fw_dat[i].TOB2_lgJ_et)
        sim_right_ls_jet_en.append(sim_dat[i].TOB2_lgJ_et)

        fw_left_ss_jet_en.append(fw_dat[i].TOB1_sgJ_et)
        sim_left_ss_jet_en.append(sim_dat[i].TOB1_sgJ_et)
        fw_right_ss_jet_en.append(fw_dat[i].TOB2_sgJ_et)
        sim_right_ss_jet_en.append(sim_dat[i].TOB2_sgJ_et)

        #Phi
        fw_left_ll_jet_phi.append(fw_dat[i].TOB1_lgLJ_phi)
        sim_left_ll_jet_phi.append(sim_dat[i].TOB1_lgLJ_phi)
        fw_right_ll_jet_phi.append(fw_dat[i].TOB2_lgLJ_phi)
        sim_right_ll_jet_phi.append(sim_dat[i].TOB2_lgLJ_phi)

        fw_left_ls_jet_phi.append(fw_dat[i].TOB1_lgJ_phi)
        sim_left_ls_jet_phi.append(sim_dat[i].TOB1_lgJ_phi)
        fw_right_ls_jet_phi.append(fw_dat[i].TOB2_lgJ_phi)
        sim_right_ls_jet_phi.append(sim_dat[i].TOB2_lgJ_phi)

        fw_left_ss_jet_phi.append(fw_dat[i].TOB1_sgJ_phi)
        sim_left_ss_jet_phi.append(sim_dat[i].TOB1_sgJ_phi)
        fw_right_ss_jet_phi.append(fw_dat[i].TOB2_sgJ_phi)
        sim_right_ss_jet_phi.append(sim_dat[i].TOB2_sgJ_phi)

        #Eta
        fw_left_ll_jet_eta.append(fw_dat[i].TOB1_lgLJ_eta)
        sim_left_ll_jet_eta.append(sim_dat[i].TOB1_lgLJ_eta)
        fw_right_ll_jet_eta.append(fw_dat[i].TOB2_lgLJ_eta)
        sim_right_ll_jet_eta.append(sim_dat[i].TOB2_lgLJ_eta)

        fw_left_ls_jet_eta.append(fw_dat[i].TOB1_lgJ_eta)
        sim_left_ls_jet_eta.append(sim_dat[i].TOB1_lgJ_eta)
        fw_right_ls_jet_eta.append(fw_dat[i].TOB2_lgJ_eta)
        sim_right_ls_jet_eta.append(sim_dat[i].TOB2_lgJ_eta)

        fw_left_ss_jet_eta.append(fw_dat[i].TOB1_sgJ_eta)
        sim_left_ss_jet_eta.append(sim_dat[i].TOB1_sgJ_eta)
        fw_right_ss_jet_eta.append(fw_dat[i].TOB2_sgJ_eta)
        sim_right_ss_jet_eta.append(sim_dat[i].TOB2_sgJ_eta)

        #JWJ
        fw_mhtx.append(fw_dat[i].jwj_mhtx)
        sim_mhtx.append(sim_dat[i].jwj_mhtx)
        fw_mhty.append(fw_dat[i].jwj_mhty)
        sim_mhty.append(sim_dat[i].jwj_mhty)
        fw_metx.append(fw_dat[i].jwj_metx)
        sim_metx.append(sim_dat[i].jwj_metx)
        fw_mety.append(fw_dat[i].jwj_mety)
        sim_mety.append(sim_dat[i].jwj_mety)
        fw_mstx.append(fw_dat[i].jwj_mstx)
        sim_mstx.append(sim_dat[i].jwj_mstx)
        fw_msty.append(fw_dat[i].jwj_msty)
        sim_msty.append(sim_dat[i].jwj_msty)

        #JWJ sumET
        fw_tot_sumET.append(fw_dat[i].jwj_tot_sumEt)
        sim_tot_sumET.append(sim_dat[i].jwj_tot_sumEt)
        fw_hard_sumET.append(fw_dat[i].jwj_hard_sumEt)
        sim_hard_sumET.append(sim_dat[i].jwj_hard_sumEt)
        fw_soft_sumET.append(fw_dat[i].jwj_soft_sumEt)
        sim_soft_sumET.append(sim_dat[i].jwj_soft_sumEt)

        #Alt MET
        fw_nc_metx.append(fw_dat[i].nc_metx)
        sim_nc_metx.append(sim_dat[i].nc_metx)
        fw_nc_mety.append(fw_dat[i].nc_mety)
        sim_nc_mety.append(sim_dat[i].nc_mety)
        fw_rms_metx.append(fw_dat[i].rms_metx)
        sim_rms_metx.append(sim_dat[i].rms_metx)
        fw_rms_mety.append(fw_dat[i].rms_mety)
        sim_rms_mety.append(sim_dat[i].rms_mety)
        fw_nc_sumEt.append(fw_dat[i].nc_sumEt)
        sim_nc_sumEt.append(sim_dat[i].nc_sumEt)
        fw_rms_sumEt.append(fw_dat[i].rms_sumEt)
        sim_rms_sumEt.append(sim_dat[i].rms_sumEt)

    #For delay testing only
    delay_val = 0
    #delay_str = f"_InterDelay{delay_val}"
    delay_str = ""

    #Energy
    pf.scat_plot(fw_left_ll_jet_en, sim_left_ll_jet_en,f"{larger_str} {et_str} {compx_str} {units[0]}",
                 f"{larger_str} {et_str} {compy_str} {units[0]}",f"{larger_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ls_jet_en, sim_left_ls_jet_en,f"{lsmallr_str} {et_str} {compx_str} {units[0]}",
                 f"{lsmallr_str} {et_str} {compy_str} {units[0]}",f"{lsmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ss_jet_en, sim_left_ss_jet_en,f"{ssmallr_str} {et_str} {compx_str} {units[0]}",
                 f"{ssmallr_str} {et_str} {compy_str} {units[0]}",f"{ssmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_right_ll_jet_en, sim_right_ll_jet_en, f"{larger_str} {et_str} {compx_str} {units[0]}",
                 f"{larger_str} {et_str} {compy_str} {units[0]}", f"{larger_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ls_jet_en, sim_right_ls_jet_en, f"{lsmallr_str} {et_str} {compx_str} {units[0]}",
                 f"{lsmallr_str} {et_str} {compy_str} {units[0]}", f"{lsmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ss_jet_en, sim_right_ss_jet_en, f"{ssmallr_str} {et_str} {compx_str} {units[0]}",
                 f"{ssmallr_str} {et_str} {compy_str} {units[0]}", f"{ssmallr_str}_{et_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    #Phi
    pf.scat_plot(fw_left_ll_jet_phi, sim_left_ll_jet_phi,f"{larger_str} {phi_str} {compx_str}",
                 f"{larger_str} {phi_str} {compy_str}", f"{larger_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ls_jet_phi, sim_left_ls_jet_phi,f"{lsmallr_str} {phi_str} {compx_str}",
                 f"{lsmallr_str} {phi_str} {compy_str}",f"{lsmallr_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ss_jet_phi, sim_left_ss_jet_phi,f"{ssmallr_str} {phi_str} {compx_str}",
                 f"{ssmallr_str} {phi_str} {compy_str}",f"{ssmallr_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_right_ll_jet_phi, sim_right_ll_jet_phi, f"{larger_str} {phi_str} {compx_str}",
                 f"{larger_str} {phi_str} {compy_str}", f"{larger_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ls_jet_phi, sim_right_ls_jet_phi, f"{lsmallr_str} {et_str} {compx_str}",
                 f"{lsmallr_str} {phi_str} {compy_str}", f"{lsmallr_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ss_jet_phi, sim_right_ss_jet_phi, f"{ssmallr_str} {phi_str} {compx_str}",
                 f"{ssmallr_str} {phi_str} {compy_str}", f"{ssmallr_str}_{phi_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    #Eta
    pf.scat_plot(fw_left_ll_jet_eta, sim_left_ll_jet_eta, f"{larger_str} {eta_str} {compx_str}",
                 f"{larger_str} {eta_str} {compy_str}",f"{larger_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ls_jet_eta, sim_left_ls_jet_eta, f"{lsmallr_str} {eta_str} {compx_str}",
                 f"{lsmallr_str} {eta_str} {compy_str}",f"{lsmallr_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_left_ss_jet_eta, sim_left_ss_jet_eta,f"{ssmallr_str} {eta_str} {compx_str}",
                 f"{ssmallr_str} {eta_str} {compy_str}",f"{ssmallr_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, left_half)

    pf.scat_plot(fw_right_ll_jet_eta, sim_right_ll_jet_eta, f"{larger_str} {eta_str} {compx_str}",
                 f"{larger_str} {eta_str} {compy_str}", f"{larger_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ls_jet_eta, sim_right_ls_jet_eta, f"{lsmallr_str} {eta_str} {compx_str}",
                 f"{lsmallr_str} {eta_str} {compy_str}", f"{lsmallr_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    pf.scat_plot(fw_right_ss_jet_eta, sim_right_ss_jet_eta, f"{ssmallr_str} {eta_str} {compx_str}",
                 f"{ssmallr_str} {eta_str} {compy_str}", f"{ssmallr_str}_{eta_str}_Half_FPGA_Comparisons{delay_str}",
                 jet_comp_folder_str, RUN_num, right_half)

    #JWJ
    pf.scat_plot(fw_mhtx, sim_mhtx, f"JWJ MHT X {compx_str} {units[0]}", f"JWJ MHT X {compy_str} {units[0]}", f"JWJ_MHTX_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    pf.scat_plot(fw_mhty, sim_mhty, f"JWJ MHT Y {compx_str} {units[0]}", f" JWJ MHT Y {compy_str} {units[0]}", f"JWJ_MHTY_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    pf.scat_plot(fw_metx, sim_metx, f"JWJ MET X {compx_str} {units[0]}", f"JWJ MET X {compy_str} {units[0]}", f"JWJ_METX_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    pf.scat_plot(fw_mety, sim_mety, f"JWJ MET Y {compx_str} {units[0]}", f"JWJ MET Y {compy_str} {units[0]}", f"JWJ_METY_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    pf.scat_plot(fw_mstx, sim_mstx, f"JWJ MST X {compx_str} {units[0]}", f"JWJ MST X {compy_str} {units[0]}", f"JWJ_MSTX_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    pf.scat_plot(fw_msty, sim_msty, f"JWJ MST Y {compx_str} {units[0]}", f"JWJ MST Y {compy_str} {units[0]}", f"JWJ_MSTY_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    #JWJ sumET
    pf.scat_plot(fw_tot_sumET, sim_tot_sumET, f"JWJ Total sumET {compx_str} {units[0]}", f"JWJ Total SumET {compy_str} {units[0]}", f"JWJ_SUMET_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_hard_sumET, sim_hard_sumET, f"JWJ Hard sumET {compx_str} {units[0]}", f"JWJ Hard SumET {compy_str} {units[0]}", f"JWJ_SUMHT_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_soft_sumET, sim_soft_sumET, f"JWJ Soft sumET {compx_str} {units[0]}", f"JWJ Soft SumET {compy_str} {units[0]}", f"JWJ_SUMST_Comparisons",
                 met_comp_folder_str, RUN_num, FPGA_str)

    #Alt MET
    pf.scat_plot(fw_nc_metx, sim_nc_metx, f"NC MET X {compx_str} {units[0]}", f"NC MET X {compy_str} {units[0]}",
                 f"NC_METX_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_nc_mety, sim_nc_mety, f"NC MET Y {compx_str} {units[0]}", f"NC MET Y {compy_str} {units[0]}",
                 f"NC_METY_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_rms_metx, sim_rms_metx, f"Rho+RMS MET X {compx_str} {units[0]}", f"Rho+RMS MET X {compy_str} {units[0]}",
                 f"RMS_METX_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_rms_mety, sim_rms_mety, f"Rho+RMS MET Y {compx_str} {units[0]}", f"Rho+RMS MET Y {compy_str} {units[0]}",
                 f"RMS_METY_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_nc_sumEt, sim_nc_sumEt, f"NC Sum Et {compx_str} {units[0]}", f"NC Sum Et {compy_str} {units[0]}",
                 f"NC_SUMET_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)
    pf.scat_plot(fw_rms_sumEt, sim_rms_sumEt, f"Rho+RMS Sum Et {compx_str} {units[0]}", f"Rho+RMS Sum Et {compy_str} {units[0]}",
                 f"RMS_SUMET_Comparisons", met_comp_folder_str, RUN_num, FPGA_str)

    return

def mismatch_line_num_plots_ctob(fw_dat, sim_dat, RUN_num):

    #DQ checks for variable assignments

    #FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    #Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    eta = array('d')
    phi = array('d')

    for i in range(l):
        #l1id check
        if fw_dat[i].j_l1id == sim_dat[i].j_l1id:

            if fw_dat[i].TOB2_lgLJ_et != sim_dat[i].TOB2_lgLJ_et:
                #for differences, stipulate that the difference is at least 10% to hide issues not corresponding to interFPGA comm
                if (float(fw_dat[i].TOB2_lgLJ_et) < 0.8*sim_dat[i].TOB2_lgLJ_et) or (float(fw_dat[i].TOB2_lgLJ_et) > 1.2*sim_dat[i].TOB2_lgLJ_et):
                    if fw_dat[i].TOB2_lgLJ_eta >= 28:
                        eta.append(fw_dat[i].TOB2_lgLJ_eta)
                        phi.append(fw_dat[i].TOB2_lgLJ_phi)

    ln = array('d')
    cc = array('d')
    
    for i in range(len(eta)):
        for j in range(-3,4):
            lni, cci = hf.calc_ctob_line_num(eta[i], phi[i], j)
            ln.append(lni)
            cc.append(cci)

    if len(ln) == 0:
        ln.append(0)
    if len(cc) == 0:
        cc.append(0)

    folder = f"Comparison_Plots/JetTOB_Comparisons/"
    plotstr_base = f"Mismatching_Events_"

    pf.hist_1d(ln, "Line Number", f"{plotstr_base}LineNumber", folder, RUN_num, f"{FPGA_str}2")
    pf.hist_1d(cc, "Clock Cycle", f"{plotstr_base}ClockCycle", folder, RUN_num, f"{FPGA_str}2")



    return

def match_line_num_plots_ctob(fw_a, fw_b, fw_c, sim_a, sim_b, sim_c, RUN_num):
    # Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_a) == len(sim_a):
        l = len(fw_a)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    eta = array('d')
    phi = array('d')

    for i in range(l):
        # l1id check
        if fw_b[i].j_l1id == sim_b[i].j_l1id:

            if fw_b[i].TOB2_lgLJ_et == sim_b[i].TOB2_lgLJ_et:
                # for differences, stipulate that the difference is at least 10% to hide issues not corresponding to interFPGA comm
                if ((sim_c[i].TOB2_lgJ_eta - sim_b[i].TOB2_lgLJ_eta <= 2) and (abs(sim_c[i].TOB2_lgJ_phi - sim_b[i].TOB2_lgLJ_phi) <= 2)):
                    if fw_b[i].TOB2_lgLJ_eta >= 28:
                        eta.append(fw_b[i].TOB2_lgLJ_eta)
                        phi.append(fw_b[i].TOB2_lgLJ_phi)

    ln = array('d')
    cc = array('d')

    for i in range(len(eta)):
        for j in range(-3,4):
            lni, cci = hf.calc_ctob_line_num(eta[i], phi[i], j)
            ln.append(lni)
            cc.append(cci)

    if len(ln) == 0:
        ln.append(0)
    if len(cc) == 0:
        cc.append(0)

    folder = f"Comparison_Plots/JetTOB_Comparisons/"
    plotstr_base = f"Matching_Events_"

    pf.hist_1d(ln, "Line Number", f"{plotstr_base}LineNumber", folder, RUN_num, f"B2")
    pf.hist_1d(cc, "Clock Cycle", f"{plotstr_base}ClockCycle", folder, RUN_num, f"B2")



    return


def mismatch_line_num_plots_atob(fw_dat, sim_dat, RUN_num):
    # DQ checks for variable assignments

    # FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    # Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    eta = array('d')
    phi = array('d')

    for i in range(l):
        # l1id check
        if fw_dat[i].j_l1id == sim_dat[i].j_l1id:

            if fw_dat[i].TOB1_lgLJ_et != sim_dat[i].TOB1_lgLJ_et:
                # for differences, stipulate that the difference is at least 10% to hide issues not corresponding to interFPGA comm
                if (float(fw_dat[i].TOB1_lgLJ_et) < 0.8 * sim_dat[i].TOB1_lgLJ_et) or (
                        float(fw_dat[i].TOB1_lgLJ_et) > 1.2 * sim_dat[i].TOB1_lgLJ_et):
                    if fw_dat[i].TOB1_lgLJ_eta >= 20:
                        eta.append(fw_dat[i].TOB1_lgLJ_eta)
                        phi.append(fw_dat[i].TOB1_lgLJ_phi)

    ln = array('d')
    cc = array('d')

    for i in range(len(eta)):
        for j in range(0, 9):
            lni, cci = hf.calc_atob_line_num(eta[i], phi[i], j)
            ln.append(lni)
            cc.append(cci)

    if len(ln) == 0:
        ln.append(0)
    if len(cc) == 0:
        cc.append(0)

    folder = f"Comparison_Plots/JetTOB_Comparisons/"
    plotstr_base = f"Mismatching_Events_"

    pf.hist_1d(ln, "Line Number", f"{plotstr_base}LineNumber", folder, RUN_num, f"{FPGA_str}1")
    pf.hist_1d(cc, "Clock Cycle", f"{plotstr_base}ClockCycle", folder, RUN_num, f"{FPGA_str}1")

    return

def match_line_num_plots_atob(fw_a, fw_b, fw_c, sim_a, sim_b, sim_c, RUN_num):
    # Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_a) == len(sim_a):
        l = len(fw_a)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    eta = array('d')
    phi = array('d')

    for i in range(l):
        # l1id check
        if fw_b[i].j_l1id == sim_b[i].j_l1id:

            if fw_b[i].TOB1_lgLJ_et == sim_b[i].TOB1_lgLJ_et:
                # for differences, stipulate that the difference is at least 10% to hide issues not corresponding to interFPGA comm
                if fw_b[i].TOB1_lgLJ_eta >= 20:
                    eta.append(fw_b[i].TOB1_lgLJ_eta)
                    phi.append(fw_b[i].TOB1_lgLJ_phi)

    ln = array('d')
    cc = array('d')

    for i in range(len(eta)):
        for j in range(0,9):
            lni, cci = hf.calc_atob_line_num(eta[i], phi[i], j)
            ln.append(lni)
            cc.append(cci)

    if len(ln) == 0:
        ln.append(0)
    if len(cc) == 0:
        cc.append(0)

    folder = f"Comparison_Plots/JetTOB_Comparisons/"
    plotstr_base = f"Matching_Events_"

    pf.hist_1d(ln, "Line Number", f"{plotstr_base}LineNumber", folder, RUN_num, f"B1")
    pf.hist_1d(cc, "Clock Cycle", f"{plotstr_base}ClockCycle", folder, RUN_num, f"B1")



    return

def make_puc_comp_plot(fw_dat, sim_dat, RUN_num):
    #DQ checks for variable assignments

    #FPGA string : ensure that only like FPGAs are compared
    FPGA_str = ''
    if fw_dat[0].fpga_id == sim_dat[0].fpga_id:
        FPGA_str = fw_dat[0].fpga_id
    else:
        print("Error: Please only compare FPGAs of the same identifier, FPGA_str set to empty string")

    #Length : ensure that lists are of the same length for comparisons
    l = 0
    if len(fw_dat) == len(sim_dat):
        l = len(fw_dat)
    else:
        print("Error: length of FW and Sim FPGA lists do not match, l set to 0")

    fw_puc = array('d')
    sim_puc = array('d')

    for i in range(l):
        fw_puc.append(fw_dat[i].puc)
        sim_puc.append(sim_dat[i].puc)

    puc_str = "PUC Value"
    compx_str = "from gFEX Readout Path"
    compy_str = "Emulated from gFEX Input Readout"
    jet_comp_folder_str = "Comparison_Plots/JetTOB_Comparisons/"

    pf.scat_plot(fw_puc, sim_puc, f"{puc_str} {compx_str} {units[0]}", f"{puc_str} {compy_str} {units[0]}", f"PUC_Comp", jet_comp_folder_str, RUN_num, FPGA_str)


    return

def make_comp_plots(fw_ev_list, sim_ev_list, RUN_num):

    #organize events into FPGAs
    fw_fpga_a_list = []
    fw_fpga_b_list = []
    fw_fpga_c_list = []

    sim_fpga_a_list = []
    sim_fpga_b_list = []
    sim_fpga_c_list = []

    l = len(fw_ev_list)

    for i in range(0,l):
        fw_fpga_a_list.append(fw_ev_list[i].FPGA_A)
        fw_fpga_b_list.append(fw_ev_list[i].FPGA_B)
        fw_fpga_c_list.append(fw_ev_list[i].FPGA_C)
        sim_fpga_a_list.append(sim_ev_list[i].FPGA_A)
        sim_fpga_b_list.append(sim_ev_list[i].FPGA_B)
        sim_fpga_c_list.append(sim_ev_list[i].FPGA_C)

    comp_plots(fw_fpga_a_list, sim_fpga_a_list, RUN_num)
    comp_plots(fw_fpga_b_list, sim_fpga_b_list, RUN_num)
    comp_plots(fw_fpga_c_list, sim_fpga_c_list, RUN_num)

    mismatch_line_num_plots_ctob(fw_fpga_b_list, sim_fpga_b_list, RUN_num)
    match_line_num_plots_ctob(fw_fpga_a_list, fw_fpga_b_list, fw_fpga_c_list, sim_fpga_a_list, sim_fpga_b_list, sim_fpga_c_list, RUN_num)

    mismatch_line_num_plots_atob(fw_fpga_b_list, sim_fpga_b_list, RUN_num)
    match_line_num_plots_atob(fw_fpga_a_list, fw_fpga_b_list, fw_fpga_c_list, sim_fpga_a_list, sim_fpga_b_list, sim_fpga_c_list, RUN_num)

    make_puc_comp_plot(fw_fpga_a_list, sim_fpga_a_list, RUN_num)
    make_puc_comp_plot(fw_fpga_b_list, sim_fpga_b_list, RUN_num)
    make_puc_comp_plot(fw_fpga_c_list, sim_fpga_c_list, RUN_num)

    return

