#!/bin/bash

# Script to easily move all output/input files to my CERNBox folders, folder name input must already exist
dir_str="/eos/user/a/amcarrol/gFEXFirstData/"
full_path=$dir_str$1

cp -r ReadoutTOBfiles/ $full_path
cp -r SimTOBfiles/ $full_path
cp -r Comparison_dbg/ $full_path
cp -r Comparison_Plots/ $full_path
cp -r TOB_Distributions/ $full_path
cp -r Comparison_Histograms/ $full_path
