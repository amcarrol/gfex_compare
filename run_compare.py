# File to run all the comparison functions in a separate file from gfex_compare for easier editing


# import the other scripts and libraries
import event_builder as eb
import make_compare_files as mcf
import make_compare_plots as mcp
import make_distribution_plots as mdp
import check_met_calc as cmc
import argparse
import ROOT as r
import dbg

# set batch mode for plots to prevent all plots opening
r.gROOT.SetBatch(r.kTRUE)

ap = argparse.ArgumentParser()
ap.add_argument("-r", "--run", required=True, default=None, type=int, help="Run Number of TOB files to be compared")
ap.add_argument("-d", "--detailed", required=False, default=False, type=bool, help="If true, generates many more plots")


args = vars(ap.parse_args())


RUN_num = args["run"]
detail = args["detailed"]


#Report of inputs
print(f"Run Number set to {RUN_num}")
print(f"detail bool set to {detail}")


'''
------------------------------------------------------------------------------------------------
Running Flags: Change to 0 to omit comparisons from that fpga
a,b,c FPGAs
j,g   jet or global (MET)
------------------------------------------------------------------------------------------------
'''
aj_flag = 1
bj_flag = 1
cj_flag = 1
ag_flag = 1
bg_flag = 1
cg_flag = 1



#print statement
print("Reading in Files")

# files to be called
dump_ajtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_ajtobs.txt"
dump_bjtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_bjtobs.txt"
dump_cjtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_cjtobs.txt"

dump_agtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_agtobs.txt"
dump_bgtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_bgtobs.txt"
dump_cgtobs = f"ReadoutTOBfiles/run{RUN_num}_dump_cgtobs.txt"


sim_ajtobs = f"SimTOBfiles/file{RUN_num}_atobs.txt"
sim_bjtobs = f"SimTOBfiles/file{RUN_num}_btobs.txt"
sim_cjtobs = f"SimTOBfiles/file{RUN_num}_ctobs.txt"

sim_agtobs = f"SimTOBfiles/file{RUN_num}_agtobs.txt"
sim_bgtobs = f"SimTOBfiles/file{RUN_num}_bgtobs.txt"
sim_cgtobs = f"SimTOBfiles/file{RUN_num}_cgtobs.txt"

'''
-------------------------------------------------------------------------------------------------
Event Builder
-------------------------------------------------------------------------------------------------
'''


#print statement
print("Building List of Events")

#gives lists of event objects to be passed into other functions
fw_event_list = eb.event_list_builder(dump_ajtobs, dump_agtobs, dump_bjtobs, dump_bgtobs, dump_cjtobs, dump_cgtobs)
sim_event_list = eb.event_list_builder(sim_ajtobs, sim_agtobs, sim_bjtobs, sim_bgtobs, sim_cjtobs, sim_cgtobs)


'''
------------------------------------------------------------------------------------------------
Running Text Comparisons between Readout Path and C Simulation
------------------------------------------------------------------------------------------------
'''

#print statement
print("generating comparison output files for Jet TOBs and MET TOBs")

#Text Based TOB Comparisons
mcf.make_compare_files(fw_event_list, sim_event_list, RUN_num)


'''
--------------------------------------------------------------------------------------------------
Comparison Plots
--------------------------------------------------------------------------------------------------
'''

if detail:
    #print statement
    print("generating comparison plots")

    #Comparison plots
    mcp.make_comp_plots(fw_event_list, sim_event_list, RUN_num)

    #print statement
    print("generating TOB distribution plots")

    #Distributions plots
    mdp.make_distribution_plots(fw_event_list, "Readout", RUN_num)
    mdp.make_distribution_plots(sim_event_list, "Sim", RUN_num)

    #print statement
    print("generating eta phi plots of mismatches")

    #Debug Script Items
    dbg.mismatch_etaphi_plots(fw_event_list, sim_event_list, RUN_num)

    #Large R Jet Eta - Leading Small R Jet Eta
    mdp.make_misc_histograms(fw_event_list, sim_event_list, RUN_num)


print("generating comparison histograms")
#Comparison Histograms
mdp.make_comp_histograms(fw_event_list, sim_event_list, RUN_num)

#MET Check
print("checking MET TOB values to calculated MET values")
cmc.make_check_met_calc(fw_event_list, RUN_num, "Readout")

#Special Actions
#Look for events with: Large R Jet in A2 close to A/B border AND Small R Jet in A2 close to Large R Jet AND Small R Jet in B1 at similar phi and close to A/B border



'''
END OF RELEVANT THINGS 
'''


'''
#print statement
print("generating Jet TOB hitmaps")

#hit maps
comp.make_TOB_hitmaps(dump_ajtobs, dump_bjtobs, dump_cjtobs, RUN_num)
'''



'''
#print statement
print("generating angle distributions for small jets")

comp.make_delangle_dist(dump_ajtobs, sim_ajtobs, RUN_num, 0)
comp.make_delangle_dist(dump_bjtobs, sim_bjtobs, RUN_num, 1)
comp.make_delangle_dist(dump_cjtobs, sim_cjtobs, RUN_num, 2)
'''

'''
#print statement
print('generating MET TOB Distributions')

comp.JWJ_MET_hist(dump_agtobs, RUN_num, 0)
comp.JWJ_MET_hist(dump_bgtobs, RUN_num, 1)
comp.JWJ_MET_hist(dump_cgtobs, RUN_num, 2)




#print statement
print('Generating MET TOB Comparison Plots')

comp.make_met_comp(dump_agtobs, sim_agtobs, RUN_num, 0)
comp.make_met_comp(dump_bgtobs, sim_bgtobs, RUN_num, 1)
comp.make_met_comp(dump_cgtobs, sim_cgtobs, RUN_num, 2)
'''

#print statement
print("done!")
