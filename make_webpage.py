#generates an html webpage to display plots from CERNBox of a given comparison run

#imports
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-r", "--run", required=True, default=None, type=int, help="Run Number of TOB files to be compared")
ap.add_argument("-f", "--folder", required=True, default=None, type=str,
                help="Folder in /eos/user/a/amcarrol/gFEXFirstData/ directory referencing plots to be made")

args = vars(ap.parse_args())


RUN_num = args["run"]
folder = args["folder"]

direc = "/eos/user/a/amcarrol/gFEXFirstData/"

f = open(f"{direc}{folder}Plots.html", "w")

html = f"<DOCTYPE! html>\n " \
       f"<html>\n" \
       f"   <head>\n" \
       f"       <title>Plots for Run{RUN_num} </title>\n" \
       f"   </head>\n" \
       f"   <body>\n" \
       f"       <h1> Folders </h1>\n" \
       f"       <p>List </p>\n" \
       f"   </body>\n" \
       f"</html>\n"

f.write(html)

f.close()