#!/bin/bash

# Script for clearing out old run inputs and outputs from folders

# Only Clear the TOB files if moving to a new run, not rerunning the same files
if [ $2 -eq 1 ]
then
echo "Clearing Input and Output Folders for Run $1"
rm ReadoutTOBfiles/*$1*
rm SimTOBfiles/*$1*
else
echo "Clearing Output Folders for Run $1"
fi


rm Comparison_dbg/dbg_Jet_Comp/*$1*
rm Comparison_dbg/dbg_MET_Comp/*$1*
rm Comparison_dbg/dbg_hitmaps/*$1*
rm Comparison_Plots/JetTOB_Comparisons/*$1*
rm Comparison_Plots/METTOB_Comparisons/*$1*
rm Comparison_Histograms/JetTOBs/*$1*
rm Comparison_Histograms/METTOBs/*$1*
rm TOB_Distributions/ReadoutTOBs/JetTOBs/*$1*
rm TOB_Distributions/ReadoutTOBs/METTOBs/*$1*
rm TOB_Distributions/SimTOBs/JetTOBs/*$1*
rm TOB_Distributions/SimTOBs/METTOBs/*$1*
