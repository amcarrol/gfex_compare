#Helper Functions for working on parsing and comparisons for gFEX TOBs
import math

'''
Base Changing Helper Functions
------------------------------------------------------------------------------------------------------------------------------------
'''


# function that will take in the 8 digit hexadecimal string and return the 32 digit binary number for parsing
# hstr is the string of 8 characters in hexadecimal
# n is the integer length of the binary string
# returns a string of the binary characters
def hex_to_binary(hstr, n):
    return (str(bin(int(hstr, 16))))[2:].zfill(n)


# returns hexadecimal given binary string
def binary_to_hex(bstr, n):
    return (str(hex(int(bstr, 2))))[2:].zfill(n)


def bin_to_signed_int(bstr):
    num = 0
    if bstr[0] == '1':
        #to calculate signed integer from binary, subtract 1, [2:] cuts off the '0b' from bin()
        calc = bin(int(bstr,2) - 1)[2:]
        #flip bits so that 0->1 and 1->0
        calc_list = list(calc)
        for i in range(len(calc_list)):
            if calc_list[i] == '0':
                calc_list[i] = '1'
            else:
                calc_list[i] = '0'
        calc = ''.join(calc_list)
        num = -1* int(calc,2)
    if bstr[0] == '0':
        num = int(bstr,2)

    return(num)

#takes in a signed number n and returns a nicely formated binary string of length l
def signed_int_to_bin(n, l):

    res = ''

    if n < 0:
        bstr = bin(n)[3:].zfill(l)
        for i in range(l):
            if bstr[i] == '0':
                res = res + '1'
            if bstr[i] == '1':
                res = res + '0'

        res = bin(int(res,2)+1)[2:].zfill(l)
    else:
        res = bin(n)[2:].zfill(l)

    return res

'''
Parsing Helper Functions
-----------------------------------------------------------------------------------------------------------------------------------
'''

#given file, returns nicely parsed lines
def get_lines(f):
    dat = open(f, 'r+')
    lines = dat.readlines()
    lines = [s.strip() for s in lines]
    dat.close()
    return lines

#assigns FPGA_string for file writing based on FPGA_num
def get_fpgastr(fpga_num):
    fpga_str = ''
    if fpga_num == 0:
        fpga_str = 'A'
    if fpga_num == 1:
        fpga_str = 'B'
    if fpga_num== 2:
        fpga_str = 'C'
    return fpga_str



'''
Array Manipulation Helper Functions
'''


# helper function to get difference of values between two arrays
def get_diff(arr1, arr2):
    diff = array('d')

    for i in range(len(arr1)):
        diff.append(arr1[i] - arr2[i])

    return diff


# helper function to append left and right arrays of FPGAs together, if arrays are not same size, returns an empty array
def append_arr(arr1, arr2):
    combined = array()
    if len(arr1) == len(arr2):
        for i in range(len(arr1)):
            combined.append(arr1[i])
            combined.append(arr2[i])

    return combined

#calculate metx or mety value for
def calc_met_val(mht, mst, a, b):

    return((mht*a+mst*b)/1024)

def calc_vec_mag(x,y):

    return(math.sqrt(x**2+y**2))

#right now specific to FPGA B2, calculate line number for mismatching events
#eta and phi are eta and phi positions of the readout jet for mismatched events
def calc_ctob_line_num(eta, phi, jbit):
    #total bits - need to check this math
    tb = ((eta-28)*32+phi)*7+jbit

    #clock cycle - should be integer 0-6
    cc = int(tb/128)

    #line number - should be integer 0-34?
    ln = 2 + int((tb%128)/4)
    if(tb%128 < 3):
        ln = 1

    return (ln, cc)

def calc_atob_line_num(eta, phi, jbit):

    ln = 3 + int((((jbit + 9*((eta-20)*32 + phi))%165)/4))
    cc = int((jbit+9*((eta-20)*32+phi))/165)

    return (ln, cc)