#Helper functions for organizing/parsing Jet TOB data read in from file

import helper_functions as hf

'''
for jet TOBs, format is
Link 0: Left half of pFPGA
Word 0: unused
Word 1: Leading gBlock (small R jet)
Word 2: Subleading gBlock
Word 3: Leading gJet
Word 4-5: unused
Word 6: Trailer

Link 1: Right half of pFPGA
Word 0: unused (PUC in energy field)
Word 1: Leading gBlock (small R jet)
Word 2: Subleading gBlock
Word 3: Leading gJet
Word 4-5: unused
Word 6: Trailer

Word Structure (32 bits):
31: Energy Saturation
30-26: Phi
25-20: Eta
19-8: energy
7: Status
6-5: Reserved
4-0: TOB ID
'''



#helper function to parse data for jet tobs
#takes an array of lines from a jet tob data file and parses it into leading/subleading gblock, gJet, and respective eta, phi, energy
def parse_jet_tobs(lines):

    #get the header for each event
    l1id = lines[1::17]
    #reverse list to make it easier to index
    l1id = [l[::-1] for l in l1id]
    #take the first element of the split list (separates at space)
    l1id = [l.split()[0] for l in l1id]
    #flip back
    l1id = [l[::-1] for l in l1id]


    #left is TOB_1, right is TOB_2
    lead_gJ_left = lines[4::17]
    sub_gJ_left = lines[5::17]
    lead_gLJ_left = lines[6::17]
    trlr_left = lines[9::17]
    puc = lines[11::17]
    lead_gJ_right = lines[12::17]
    sub_gJ_right = lines[13::17]
    lead_gLJ_right = lines[14::17]
    trlr_right = lines[17::17]


    #get rid of the first two characters in each row (just word numbers and a space)
    lead_gJ_left = [l[2:] for l in lead_gJ_left]
    sub_gJ_left = [l[2:] for l in sub_gJ_left]
    lead_gLJ_left = [l[2:] for l in lead_gLJ_left]
    trlr_left = [l[2:] for l in trlr_left]
    lead_gJ_right = [l[2:] for l in lead_gJ_right]
    sub_gJ_right = [l[2:] for l in sub_gJ_right]
    lead_gLJ_right = [l[2:] for l in  lead_gLJ_right]
    trlr_right = [l[2:] for l in trlr_right]

    #puc is in the energy field of word 0
    puc = [l[5:8] for l in puc]

    return(l1id, lead_gJ_left, sub_gJ_left,lead_gLJ_left, puc, trlr_left,lead_gJ_right,sub_gJ_right,lead_gLJ_right,trlr_right)

#parses array of words from a jet TOB into phi, eta, and energy
#assumes 8 bit hexadecimal words have been converted into full 32 bit binary word
def parse_jet_words(words):

    phi = [word[1:6] for word in words]
    eta = [word[6:12] for word in words]
    energy = [word[12:24] for word in words]
    #single bit can already be set to an integer
    sat_bit = [int(word[0]) for word in words]

    return(phi, eta, energy, sat_bit)
'''
TOB Trailer format
0-8   : CRC
9-17  : gFEX Trailer
18-19 : FEX ID
20-23 : Bunch Crossing Number (BCN)
23-31 : K28.5 -- don't know what this is
'''

#right now just takes the 4 bcid bits, can add other fields of the trailer later for testing if needed
def parse_jet_trlr(trlr):
    bcn = [word[20:24] for word in trlr]
    return bcn


# runs the above helper functions to prep the data file
# base is what base you want the eta,phi, and energy information returned in, only binary (2) or decimal (10) allowed right now
def jetTOB_full_parse(lines, base):
    l1id, lead_gJ_left,  sub_gJ_left, lead_gLJ_left, puc, trlr_left, lead_gJ_right, sub_gJ_right, lead_gLJ_right, trlr_right = parse_jet_tobs(lines)

    lead_gJ_left = [hf.hex_to_binary(l, 32) for l in lead_gJ_left]
    sub_gJ_left = [hf.hex_to_binary(l, 32) for l in sub_gJ_left]
    lead_gLJ_left = [hf.hex_to_binary(l, 32) for l in lead_gLJ_left]
    trlr_left = [hf.hex_to_binary(l, 32) for l in trlr_left]
    lead_gJ_right = [hf.hex_to_binary(l, 32) for l in  lead_gJ_right]
    sub_gJ_right = [hf.hex_to_binary(l, 32) for l in sub_gJ_right]
    lead_gLJ_right = [hf.hex_to_binary(l, 32) for l in lead_gLJ_right]
    trlr_right = [hf.hex_to_binary(l, 32) for l in trlr_right]

    lead_gJ_left_phi, lead_gJ_left_eta, lead_gJ_left_en, lead_gJ_left_satbit = parse_jet_words(lead_gJ_left)
    sub_gJ_left_phi, sub_gJ_left_eta, sub_gJ_left_en, sub_gJ_left_satbit = parse_jet_words(sub_gJ_left)
    lead_gLJ_left_phi, lead_gLJ_left_eta, lead_gLJ_left_en, lead_gLJ_left_satbit = parse_jet_words(lead_gLJ_left)
    lead_gJ_right_phi, lead_gJ_right_eta, lead_gJ_right_en, lead_gJ_right_satbit = parse_jet_words(lead_gJ_right)
    sub_gJ_right_phi, sub_gJ_right_eta, sub_gJ_right_en, sub_gJ_right_satbit = parse_jet_words(sub_gJ_right)
    lead_gLJ_right_phi, lead_gLJ_right_eta, lead_gLJ_right_en, lead_gLJ_right_satbit = parse_jet_words(lead_gLJ_right)

    bcid_left = parse_jet_trlr(trlr_left)
    bcid_right = parse_jet_trlr(trlr_right)

    puc = [hf.hex_to_binary(l,12) for l in puc]
    

    '''
    #check
    for i in range(len(puc)):
        if puc[i] != "000000000000":
            print(puc[i])
    '''

    if base == 10:
        lead_gJ_left_phi = [int(l, 2) for l in lead_gJ_left_phi]
        lead_gJ_left_eta = [int(l, 2) for l in lead_gJ_left_eta]
        lead_gJ_left_en = [int(l, 2) for l in lead_gJ_left_en]
        sub_gJ_left_phi = [int(l, 2) for l in sub_gJ_left_phi]
        sub_gJ_left_eta = [int(l, 2) for l in sub_gJ_left_eta]
        sub_gJ_left_en = [int(l, 2) for l in sub_gJ_left_en]
        lead_gLJ_left_phi = [int(l, 2) for l in lead_gLJ_left_phi]
        lead_gLJ_left_eta = [int(l, 2) for l in lead_gLJ_left_eta]
        lead_gLJ_left_en = [int(l, 2) for l in lead_gLJ_left_en]
        puc = [hf.bin_to_signed_int(l) for l in puc]
        bcid_left = [int(l, 2) for l in bcid_left]

        lead_gJ_right_phi = [int(l, 2) for l in lead_gJ_right_phi]
        lead_gJ_right_eta = [int(l, 2) for l in lead_gJ_right_eta]
        lead_gJ_right_en = [int(l, 2) for l in lead_gJ_right_en]
        sub_gJ_right_phi = [int(l, 2) for l in sub_gJ_right_phi]
        sub_gJ_right_eta = [int(l, 2) for l in sub_gJ_right_eta]
        sub_gJ_right_en = [int(l, 2) for l in sub_gJ_right_en]
        lead_gLJ_right_phi = [int(l, 2) for l in lead_gLJ_right_phi]
        lead_gLJ_right_eta = [int(l, 2) for l in lead_gLJ_right_eta]
        lead_gLJ_right_en = [int(l, 2) for l in lead_gLJ_right_en]
        bcid_right = [int(l, 2) for l in bcid_right]

        return (
            l1id, lead_gJ_left_phi,  lead_gJ_left_eta,  lead_gJ_left_en,  lead_gJ_left_satbit, sub_gJ_left_phi, sub_gJ_left_eta, sub_gJ_left_en, sub_gJ_left_satbit,
            lead_gLJ_left_phi,  lead_gLJ_left_eta,  lead_gLJ_left_en,  lead_gLJ_left_satbit, puc, bcid_left, lead_gJ_right_phi, lead_gJ_right_eta, lead_gJ_right_en,
        lead_gJ_right_satbit, sub_gJ_right_phi, sub_gJ_right_eta,sub_gJ_right_en, sub_gJ_right_satbit, lead_gLJ_right_phi, lead_gLJ_right_eta, lead_gLJ_right_en,
        lead_gLJ_right_satbit, bcid_right)

    # default is binary
    else:
        return (
            l1id, lead_gJ_left_phi,  lead_gJ_left_eta,  lead_gJ_left_en,  lead_gJ_left_satbit, sub_gJ_left_phi, sub_gJ_left_eta, sub_gJ_left_en, sub_gJ_left_satbit,
            lead_gLJ_left_phi,  lead_gLJ_left_eta,  lead_gLJ_left_en,  lead_gLJ_left_satbit, puc, bcid_left, lead_gJ_right_phi,lead_gJ_right_eta, lead_gJ_right_en,
        lead_gJ_right_satbit,  sub_gJ_right_phi,  sub_gJ_right_eta,  sub_gJ_right_en, sub_gJ_right_satbit, lead_gLJ_right_phi, lead_gLJ_right_eta, lead_gLJ_right_en,
        lead_gLJ_right_satbit, bcid_right)
